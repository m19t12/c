#ifndef ORDER_H
#define ORDER_H
#include "Shoppingcart.h"

class Order : public ShoppingCart {

public:

	Order();

	Order(int id, double cost, std::string status);

	Order(std::string strOr);

	void Calculation(double cost);

	int getId();

	void setId(int id);

	double getCost();

	void setCost(double cost);
	
	void setStatus(std::string status);

	std::string getStatus();

	void save(std::ofstream &outfile);

	std::string toString();

private:
	int id;
	double cost;
	std::string status;
	
};

#endif // !ORDER_H

