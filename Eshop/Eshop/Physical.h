#ifndef PHYSICAL_H
#define PHYSICAL_H
#include "Customer.h"

class Physical : public Customer {

public:
	Physical();

	Physical(string name, string surename, string am, string security);

	Physical(string strPh);

	string getName();

	void setName(string name);

	string getSurename();

	void setSurename(string surename);

	string getAm();

	void setAm(string am);

	string getSecurity();

	void setSecurity(string security);

	void save(ofstream &outfile);

	string toStringPh();

private:
	string name, surename, am, security;
};

#endif // !PHYSICAL_H

