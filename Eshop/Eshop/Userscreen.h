#ifndef USERSCREEN_H
#define USERSCREEN_H
#include <vector>
#include <iostream>
#include "Physical.h"
#include "Company.h"
#include "Order.h"

class UserScreen {

public:
	UserScreen(Physical physical, Company company, string type, string username);

	void ShowItems();
	void AddItemToCart();
	void ShowCart();
	void RemoveItemFromCart();
	void OrderStatus();
	void OrderItems();
	void Save();

private:
	Physical physical;
	Company company;
	char action;
	int code = 1, temp = 0, id;
	string str, manufacturer, type, username;
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;

};
#endif // !USERSCREEN_H

