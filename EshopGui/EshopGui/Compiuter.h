#ifndef COMPIUTER_H
#define COMPIUTER_H
#include "Product.h"


class Compiuter :
	public Product
{
public:
	Compiuter();

	Compiuter(int ram, int hdsize, double cpu, std::string gpu, std::string hdtype);

	Compiuter(std::string strCompiuter);

	int getRam();

	void setRam(int ram);

	int getHdsize();

	void setHdsize(int hdsize);

	double getCpu();

	void setCpu(double cpu);

	std::string getGpu();

	void setGpu(std::string gpu);

	std::string getHdtype();

	void setHdtype(std::string hdtype);

	void save(std::ofstream &outfile);

	std::string toStringPc();

private:
	int ram, hdsize;
	double cpu;
	std::string gpu;
	std::string hdtype;
};

#endif
