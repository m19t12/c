#ifndef USER_H
#define USER_H
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

class User {
public:

	User();

	User(string username, string password, string type);

	User(string strUs);

	string getUsername();

	void setUsername(string username);

	string getPassword();

	void setPassword(string password);

	string getType();

	void setType(string type);

	void save(ofstream &outfile);

	string toStringUs();

private:
	string username, password, type;
};

#endif
