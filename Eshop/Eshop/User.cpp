#include "User.h"

using namespace std;

User::User() {}

User::User(string username, string password, string type) {
	this->username = username;
	this->password = password;
	this->type = type;
}

User::User(string strUs) {
	istringstream issin(strUs);
	string split[3];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->username = split[0];
	this->password = split[1];
	this->type = split[2];
}

string User::getUsername() {
	return this->username;
}

void User::setUsername(string username) {
	this->username = username;
}

string User::getPassword() {
	return this->password;
}

void User::setPassword(string password) {
	this->password = password;
}

string User::getType() {
	return this->type;
}

void User::setType(string type) {
	this->type = type;
}

void User::save(ofstream &outfile) {
	outfile << this->toStringUs() << endl;
}

string User::toStringUs() {
	string str = username + "|" + password + "|" + type;
	return str;
}


