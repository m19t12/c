#pragma once
#include "Physical.h"
#include "Company.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for ForgotUser
	/// </summary>
	public ref class ForgotUser : public System::Windows::Forms::Form
	{
	public:
		ForgotUser(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ForgotUser()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  name_txt;
	private: System::Windows::Forms::TextBox^  sure_txt;
	private: System::Windows::Forms::TextBox^  security_txt;
	private: System::Windows::Forms::Button^  pass_btn;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::ComboBox^  type_cmbx;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->name_txt = (gcnew System::Windows::Forms::TextBox());
			this->sure_txt = (gcnew System::Windows::Forms::TextBox());
			this->security_txt = (gcnew System::Windows::Forms::TextBox());
			this->pass_btn = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->type_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(40, 41);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Name";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(40, 74);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(55, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Surename";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(40, 107);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(71, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Security Pass";
			// 
			// name_txt
			// 
			this->name_txt->Location = System::Drawing::Point(169, 41);
			this->name_txt->Name = L"name_txt";
			this->name_txt->Size = System::Drawing::Size(100, 20);
			this->name_txt->TabIndex = 3;
			// 
			// sure_txt
			// 
			this->sure_txt->Location = System::Drawing::Point(169, 74);
			this->sure_txt->Name = L"sure_txt";
			this->sure_txt->Size = System::Drawing::Size(100, 20);
			this->sure_txt->TabIndex = 4;
			// 
			// security_txt
			// 
			this->security_txt->Location = System::Drawing::Point(169, 107);
			this->security_txt->Name = L"security_txt";
			this->security_txt->Size = System::Drawing::Size(100, 20);
			this->security_txt->TabIndex = 5;
			// 
			// pass_btn
			// 
			this->pass_btn->Location = System::Drawing::Point(43, 197);
			this->pass_btn->Name = L"pass_btn";
			this->pass_btn->Size = System::Drawing::Size(75, 23);
			this->pass_btn->TabIndex = 6;
			this->pass_btn->Text = L"Get";
			this->pass_btn->UseVisualStyleBackColor = true;
			this->pass_btn->Click += gcnew System::EventHandler(this, &ForgotUser::pass_btn_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(169, 196);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 7;
			this->button2->Text = L"Back";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &ForgotUser::button2_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(40, 140);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(31, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Type";
			// 
			// type_cmbx
			// 
			this->type_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->type_cmbx->FormattingEnabled = true;
			this->type_cmbx->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"physical", L"company" });
			this->type_cmbx->Location = System::Drawing::Point(169, 140);
			this->type_cmbx->Name = L"type_cmbx";
			this->type_cmbx->Size = System::Drawing::Size(103, 21);
			this->type_cmbx->TabIndex = 9;
			// 
			// ForgotUser
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->type_cmbx);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->pass_btn);
			this->Controls->Add(this->security_txt);
			this->Controls->Add(this->sure_txt);
			this->Controls->Add(this->name_txt);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"ForgotUser";
			this->Text = L"ForgotUser";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void pass_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::string str, username;
		std::vector<Physical> physicalUsers;
		std::vector<Company> companyUsers;
		int code = 1;

		if (type_cmbx->SelectedItem->Equals("physical")) {

			infile.open("physical.dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					physicalUsers.push_back(Physical(str));
				}
			}
			infile.close();

			for (int i = 0; i < physicalUsers.size(); i++) {
				if (marshal_as<std::string>(name_txt->Text) == physicalUsers[i].getName() && marshal_as<std::string>(sure_txt->Text) == physicalUsers[i].getSurename() && marshal_as<std::string>(security_txt->Text) == physicalUsers[i].getSecurity()) {
					username = physicalUsers[i].getUsername();
					code = 0;
				}
			}
		}
		else if (type_cmbx->SelectedItem->Equals("company")) {
			infile.open("company.dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					companyUsers.push_back(Company(str));
				}
			}
			infile.close();

			for (int i = 0; i < physicalUsers.size(); i++) {
				if (marshal_as<std::string>(name_txt->Text) == companyUsers[i].getManagerName() && marshal_as<std::string>(sure_txt->Text) == companyUsers[i].getManagerSurename() && marshal_as<std::string>(security_txt->Text) == companyUsers[i].getSecurity()) {
					username = companyUsers[i].getUsername();
					code = 0;
				}
			}
		}
		if (code == 1) {
			MessageBox::Show("No Such User Exist");
		}
		else if (code == 0) {
			MessageBox::Show("Password : " + marshal_as<String^>(username));
		}
		this->Close();
	}
	};
}
