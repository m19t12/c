#include "Smartphone.h"

using namespace std;

SmartPhone::SmartPhone() {}

SmartPhone::SmartPhone(double screensize, int battery, bool video) {
	this->screensize = screensize;
	this->battery = battery;
	this->video = video;
}

SmartPhone::SmartPhone(string strPh) {
	istringstream issin(strPh);
	string split[9];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->setCode(stoi(split[0]));
	this->setModel(split[1]);
	this->setManufacturer(split[2]);
	this->setDescription(split[3]);
	this->setPhoto(stoi(split[4]));
	this->setPrice(stod(split[5]));

	this->screensize = stod(split[6]);
	this->battery = stoi(split[7]);
	this->video = stoi(split[8]);
}

double SmartPhone::getScreensize() {
	return this->screensize;
}

void SmartPhone::setScreensize(double screensize) {
	this->screensize = screensize;
}

int SmartPhone::getBattery() {
	return this->battery;
}

void SmartPhone::setBattery(int battery) {
	this->battery = battery;
}

bool SmartPhone::getVideo() {
	return this->video;
}

void SmartPhone::setVideo(bool video) {
	this->video = video;
}

void SmartPhone::save(ofstream &outfile) {
	outfile << this->toString() + "|";
	outfile << this->toStringPh() << endl;
}

string SmartPhone::toStringPh() {
	string str = to_string(screensize) + "|" + to_string(battery) + "|" + to_string(video);
	return str;
}
