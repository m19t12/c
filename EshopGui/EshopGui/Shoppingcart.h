#ifndef SHOPPINGCART_H
#define SHOPPINGCART_H
#include "Compiuter.h"
#include "Smartphone.h"
#include "Television.h"

class ShoppingCart {

public:
	ShoppingCart();

	void addCompiuter(Compiuter c);

	std::vector<Compiuter> getCompiuter();

	void addSmartphone(SmartPhone s);

	std::vector<SmartPhone> getSmartphone();

	void addTelevision(Television t);

	std::vector<Television> getTelevision();

	void setCompiuterVector(std::vector<Compiuter> compiuter);

	void setSmartPhoneVector(std::vector<SmartPhone> smartphone);

	void setTelevisionVector(std::vector<Television> television);

private:
	std::vector<Compiuter> compiuter;
	std::vector<SmartPhone> smartphone;
	std::vector<Television> television;
};

#endif // !SHOPPINGCART_H