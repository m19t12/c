#ifndef COMPANY_H
#define COMPANY_H
#include "Customer.h"

class Company : public Customer {

public:
	Company();

	Company(std::string managername, std::string managersurename, std::string name, double discount, int fax, std::string security);

	Company(std::string strCo);

	std::string getManagerName();

	void setManagerName(std::string managername);

	std::string getManagerSurename();

	void setManagerSurename(std::string managersurename);

	std::string getName();

	void setName(std::string name);

	double getDiscount();

	void setDiscount(double discount);

	int getFax();

	void setFax(int fax);

	std::string getSecurity();

	void setSecurity(std::string security);

	void save(std::ofstream &outfile);

	std::string toStringCo();

private:
	typedef struct manager {
		std::string name, surename;
	}Manager;

	Manager manager;
	std::string name, security;
	double discount;
	int fax;

};

#endif // !COMPANY_H

