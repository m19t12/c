#ifndef ADMIN_H
#define ADMIN_H

#include <string>
#include "User.h"

class Admin : public User {

public:
	Admin();

	Admin(std::string name, std::string surename);

	Admin(std::string strAd);

	void save(std::ofstream &outfile);

	std::string toStringAd();

	std::string getName();

	void setName(std::string name);

	std::string getSurename();

	void setSurename(std::string surename);

private:
	std::string name, surename;
};

#endif // !ADMIN_H

