#pragma once
#include <vector>
#include <string>
#include "UpdateItem.h"
#include "AddItem.h"
#include "Compiuter.h"
#include "Smartphone.h"
#include "Television.h"
#include <msclr\marshal_cppstd.h>


namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for Catalog
	/// </summary>
	public ref class Catalog : public System::Windows::Forms::Form
	{
	public:
		Catalog(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Catalog()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^  pc_lstBox;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ListBox^  phone_lstBox;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ListBox^  tv_lstBox;
	private: System::Windows::Forms::Button^  rmv_btn;
	private: System::Windows::Forms::Button^  upd_btn;
	private: System::Windows::Forms::Button^  bck_btn;
	private: System::Windows::Forms::Button^  add_btn;




	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pc_lstBox = (gcnew System::Windows::Forms::ListBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->phone_lstBox = (gcnew System::Windows::Forms::ListBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->tv_lstBox = (gcnew System::Windows::Forms::ListBox());
			this->rmv_btn = (gcnew System::Windows::Forms::Button());
			this->upd_btn = (gcnew System::Windows::Forms::Button());
			this->bck_btn = (gcnew System::Windows::Forms::Button());
			this->add_btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// pc_lstBox
			// 
			this->pc_lstBox->FormattingEnabled = true;
			this->pc_lstBox->Location = System::Drawing::Point(12, 25);
			this->pc_lstBox->Name = L"pc_lstBox";
			this->pc_lstBox->Size = System::Drawing::Size(472, 95);
			this->pc_lstBox->TabIndex = 0;
			this->pc_lstBox->SelectedIndexChanged += gcnew System::EventHandler(this, &Catalog::pc_lstBox_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(202, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Compiuters";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(202, 123);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(65, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"SmartPhone";
			// 
			// phone_lstBox
			// 
			this->phone_lstBox->FormattingEnabled = true;
			this->phone_lstBox->Location = System::Drawing::Point(12, 139);
			this->phone_lstBox->Name = L"phone_lstBox";
			this->phone_lstBox->Size = System::Drawing::Size(472, 95);
			this->phone_lstBox->TabIndex = 3;
			this->phone_lstBox->SelectedIndexChanged += gcnew System::EventHandler(this, &Catalog::phone_lstBox_SelectedIndexChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(207, 237);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(60, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Televisions";
			// 
			// tv_lstBox
			// 
			this->tv_lstBox->FormattingEnabled = true;
			this->tv_lstBox->Location = System::Drawing::Point(12, 253);
			this->tv_lstBox->Name = L"tv_lstBox";
			this->tv_lstBox->Size = System::Drawing::Size(472, 95);
			this->tv_lstBox->TabIndex = 5;
			this->tv_lstBox->SelectedIndexChanged += gcnew System::EventHandler(this, &Catalog::tv_lstBox_SelectedIndexChanged);
			// 
			// rmv_btn
			// 
			this->rmv_btn->Location = System::Drawing::Point(12, 354);
			this->rmv_btn->Name = L"rmv_btn";
			this->rmv_btn->Size = System::Drawing::Size(80, 23);
			this->rmv_btn->TabIndex = 6;
			this->rmv_btn->Text = L"Remove";
			this->rmv_btn->UseVisualStyleBackColor = true;
			this->rmv_btn->Click += gcnew System::EventHandler(this, &Catalog::rmv_btn_Click);
			// 
			// upd_btn
			// 
			this->upd_btn->Location = System::Drawing::Point(404, 354);
			this->upd_btn->Name = L"upd_btn";
			this->upd_btn->Size = System::Drawing::Size(80, 23);
			this->upd_btn->TabIndex = 7;
			this->upd_btn->Text = L"Update";
			this->upd_btn->UseVisualStyleBackColor = true;
			this->upd_btn->Click += gcnew System::EventHandler(this, &Catalog::upd_btn_Click);
			// 
			// bck_btn
			// 
			this->bck_btn->Location = System::Drawing::Point(186, 418);
			this->bck_btn->Name = L"bck_btn";
			this->bck_btn->Size = System::Drawing::Size(120, 23);
			this->bck_btn->TabIndex = 8;
			this->bck_btn->Text = L"Back";
			this->bck_btn->UseVisualStyleBackColor = true;
			this->bck_btn->Click += gcnew System::EventHandler(this, &Catalog::bck_btn_Click);
			// 
			// add_btn
			// 
			this->add_btn->Location = System::Drawing::Point(210, 354);
			this->add_btn->Name = L"add_btn";
			this->add_btn->Size = System::Drawing::Size(75, 23);
			this->add_btn->TabIndex = 9;
			this->add_btn->Text = L"Add";
			this->add_btn->UseVisualStyleBackColor = true;
			this->add_btn->Click += gcnew System::EventHandler(this, &Catalog::add_btn_Click);
			// 
			// Catalog
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(496, 453);
			this->Controls->Add(this->add_btn);
			this->Controls->Add(this->bck_btn);
			this->Controls->Add(this->upd_btn);
			this->Controls->Add(this->rmv_btn);
			this->Controls->Add(this->tv_lstBox);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->phone_lstBox);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pc_lstBox);
			this->Name = L"Catalog";
			this->Text = L"Catalog";
			this->Load += gcnew System::EventHandler(this, &Catalog::Catalog_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Catalog_Load(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::vector<std::string> stringList;
		int counter;
		std::string help = "", tempSt, str;
		std::ifstream infile;

		infile.open("pc.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				compiuterList.push_back(Compiuter(str));
			}
		}

		infile.close();

		infile.open("tv.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				televisionList.push_back(Television(str));
			}
		}

		infile.close();

		infile.open("phone.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				smartPhoneList.push_back(SmartPhone(str));
			}
		}

		infile.close();

		for (int i = 0; i < compiuterList.size(); i++) {
			pc_lstBox->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
		}

		for (int i = 0; i < smartPhoneList.size(); i++) {
			phone_lstBox->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
		}

		for (int i = 0; i < televisionList.size(); i++) {
			tv_lstBox->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
		}
	}
	private: System::Void upd_btn_Click(System::Object^  sender, System::EventArgs^  e) {

		if (pc_lstBox->SelectedItem) {
			Compiuter pc = Compiuter(marshal_as<std::string>(pc_lstBox->SelectedItem->ToString()));
			UpdateItem^ updateItemForm = gcnew UpdateItem(&pc, pc_lstBox->SelectedItem->ToString());
			this->Hide();
			updateItemForm->ShowDialog();
			this->Show();
		}
		else if (phone_lstBox->SelectedItem) {
			SmartPhone ph = SmartPhone(marshal_as<std::string>(phone_lstBox->SelectedItem->ToString()));
			UpdateItem^ updateItemForm = gcnew UpdateItem(&ph, phone_lstBox->SelectedItem->ToString());
			this->Hide();
			updateItemForm->ShowDialog();
			this->Show();
		}
		else if (tv_lstBox->SelectedItem) {
			Television tv = Television(marshal_as<std::string>(tv_lstBox->SelectedItem->ToString()));
			UpdateItem^ updateItemForm = gcnew UpdateItem(&tv, tv_lstBox->SelectedItem->ToString());
			this->Hide();
			updateItemForm->ShowDialog();
			this->Show();
		}


	}
	private: System::Void bck_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void add_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		AddItem^ addItemForm = gcnew AddItem();
		this->Hide();
		addItemForm->ShowDialog();
		this->Show();
	}
	private: System::Void rmv_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::ofstream outfile;
		std::string str;
		String^ pcTemp;
		String^ phTemp;
		String^ tvTemp;

		if (pc_lstBox->SelectedItem) {
			infile.open("pc.dat");
			outfile.open("temp.dat");

			pcTemp = pc_lstBox->SelectedItem->ToString();

			while (!infile.eof()) {
				getline(infile, str);
				if (marshal_as<String^>(str) == pcTemp) {
				}
				else
					outfile << str << std::endl;
			}

			infile.close();
			outfile.close();

			remove("pc.dat");
			rename("temp.dat", "pc.dat");
			pc_lstBox->Items->Remove(pcTemp);
		}
		if (phone_lstBox->SelectedItem) {
			infile.open("phone.dat");
			outfile.open("temp.dat");

			phTemp = phone_lstBox->SelectedItem->ToString();

			while (!infile.eof()) {
				getline(infile, str);
				if (marshal_as<String^>(str) == phTemp) {
				}
				else
					outfile << str << std::endl;
			}

			infile.close();
			outfile.close();

			remove("phone.dat");
			rename("temp.dat", "phone.dat");
			phone_lstBox->Items->Remove(phTemp);
		}
		if (tv_lstBox->SelectedItem) {
			infile.open("tv.dat");
			outfile.open("temp.dat");

			tvTemp = tv_lstBox->SelectedItem->ToString();

			while (!infile.eof()) {
				getline(infile, str);
				if (marshal_as<String^>(str) == tvTemp) {
				}
				else
					outfile << str << std::endl;
			}

			infile.close();
			outfile.close();

			remove("tv.dat");
			rename("temp.dat", "tv.dat");
			tv_lstBox->Items->Remove(tvTemp);
		}
	}
	private: System::Void pc_lstBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		phone_lstBox->ClearSelected();
		tv_lstBox->ClearSelected();
	}
	private: System::Void phone_lstBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		tv_lstBox->ClearSelected();
		pc_lstBox->ClearSelected();
	}
	private: System::Void tv_lstBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		pc_lstBox->ClearSelected();
		phone_lstBox->ClearSelected();
	}
	};
}
