#pragma once
#include "Physical.h"
#include "Company.h"
#include "Order.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;
	/// <summary>
	/// Summary for Task
	/// </summary>
	public ref class Task : public System::Windows::Forms::Form
	{
	public:
		Task(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Task()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  ordr_lbl;
	private: System::Windows::Forms::Label^  ordrSum_lbl;
	private: System::Windows::Forms::Label^  cust_lbl;
	private: System::Windows::Forms::Label^  phy_lbl;
	private: System::Windows::Forms::Label^  cmomp_lbl;
	private: System::Windows::Forms::Button^  button1;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->ordr_lbl = (gcnew System::Windows::Forms::Label());
			this->ordrSum_lbl = (gcnew System::Windows::Forms::Label());
			this->cust_lbl = (gcnew System::Windows::Forms::Label());
			this->phy_lbl = (gcnew System::Windows::Forms::Label());
			this->cmomp_lbl = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(52, 32);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(38, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Orders";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(52, 64);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(62, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Orders Sum";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(52, 98);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(56, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Customers";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(52, 130);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(46, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Physical";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(52, 157);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(51, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"Company";
			// 
			// ordr_lbl
			// 
			this->ordr_lbl->AutoSize = true;
			this->ordr_lbl->Location = System::Drawing::Point(164, 32);
			this->ordr_lbl->Name = L"ordr_lbl";
			this->ordr_lbl->Size = System::Drawing::Size(35, 13);
			this->ordr_lbl->TabIndex = 5;
			this->ordr_lbl->Text = L"label6";
			// 
			// ordrSum_lbl
			// 
			this->ordrSum_lbl->AutoSize = true;
			this->ordrSum_lbl->Location = System::Drawing::Point(164, 64);
			this->ordrSum_lbl->Name = L"ordrSum_lbl";
			this->ordrSum_lbl->Size = System::Drawing::Size(35, 13);
			this->ordrSum_lbl->TabIndex = 6;
			this->ordrSum_lbl->Text = L"label6";
			// 
			// cust_lbl
			// 
			this->cust_lbl->AutoSize = true;
			this->cust_lbl->Location = System::Drawing::Point(164, 98);
			this->cust_lbl->Name = L"cust_lbl";
			this->cust_lbl->Size = System::Drawing::Size(35, 13);
			this->cust_lbl->TabIndex = 7;
			this->cust_lbl->Text = L"label6";
			// 
			// phy_lbl
			// 
			this->phy_lbl->AutoSize = true;
			this->phy_lbl->Location = System::Drawing::Point(164, 130);
			this->phy_lbl->Name = L"phy_lbl";
			this->phy_lbl->Size = System::Drawing::Size(35, 13);
			this->phy_lbl->TabIndex = 8;
			this->phy_lbl->Text = L"label6";
			// 
			// cmomp_lbl
			// 
			this->cmomp_lbl->AutoSize = true;
			this->cmomp_lbl->Location = System::Drawing::Point(164, 157);
			this->cmomp_lbl->Name = L"cmomp_lbl";
			this->cmomp_lbl->Size = System::Drawing::Size(35, 13);
			this->cmomp_lbl->TabIndex = 9;
			this->cmomp_lbl->Text = L"label6";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(92, 202);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 10;
			this->button1->Text = L"Back";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Task::button1_Click);
			// 
			// Task
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->cmomp_lbl);
			this->Controls->Add(this->phy_lbl);
			this->Controls->Add(this->cust_lbl);
			this->Controls->Add(this->ordrSum_lbl);
			this->Controls->Add(this->ordr_lbl);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Task";
			this->Text = L"Task";
			this->Load += gcnew System::EventHandler(this, &Task::Task_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Task_Load(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::vector<Physical> physical;
		std::vector<Company> company;
		std::vector<Order> orderlist;
		std::string str;
		int count = 0, phCount = 0, coCount = 0;
		double sum = 0;

		infile.open("physical.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				physical.push_back(Physical(str));
				phCount++;
			}
		}

		infile.close();

		infile.open("company.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				company.push_back(Company(str));
				coCount++;
			}
		}

		infile.close();

		for (int i = 0; i < physical.size(); i++) {
			infile.open(physical[i].getUsername() + "_" + "Order.dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					orderlist.push_back(Order(str));
					count++;
				}
			}

			infile.close();
		}

		for (int i = 0; i < company.size(); i++) {
			infile.open(company[i].getUsername() + "_" + "Order.dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					orderlist.push_back(Order(str));
					count++;
				}
			}

			infile.close();
		}

		for (int i = 0; i < orderlist.size(); i++) {
			sum += orderlist[i].getCost();
		}

		ordr_lbl->Text = Convert::ToString(count);
		ordrSum_lbl->Text = Convert::ToString(sum);
		cust_lbl->Text = Convert::ToString(phCount+coCount);
		phy_lbl->Text = Convert::ToString(phCount);
		cmomp_lbl->Text = Convert::ToString(coCount);
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
};
}
