#include "Compiuter.h"

using namespace std;

Compiuter::Compiuter() {}

Compiuter::Compiuter(int ram, int hdsize, double cpu, string gpu, string hdtype) :Product() {
	this->ram = ram;
	this->hdsize = hdsize;
	this->cpu = cpu;
	this->gpu = gpu;
	this->hdtype = hdtype;
}

Compiuter::Compiuter(string strCompiuter) {
	istringstream issin(strCompiuter);
	string split[11];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->setCode(stoi(split[0]));
	this->setModel(split[1]);
	this->setManufacturer(split[2]);
	this->setDescription(split[3]);
	this->setPhoto(stoi(split[4]));
	this->setPrice(stod(split[5]));

	this->ram = stoi(split[6]);
	this->hdsize = stoi(split[7]);
	this->cpu = stod(split[8]);
	this->gpu = split[9];
	this->hdtype = split[10];
}

int Compiuter::getRam() {
	return this->ram;
}

void Compiuter::setRam(int ram) {
	this->ram = ram;
}

int Compiuter::getHdsize() {
	return this->hdsize;
}

void Compiuter::setHdsize(int hdsize) {
	this->hdsize = hdsize;
}

double Compiuter::getCpu() {
	return this->cpu;
}

void Compiuter::setCpu(double cpu) {
	this->cpu = cpu;
}

string Compiuter::getGpu() {
	return this->gpu;
}

void Compiuter::setGpu(string gpu) {
	this->gpu = gpu;
}

string Compiuter::getHdtype() {
	return this->hdtype;
}

void Compiuter::setHdtype(string hdtype) {
	this->hdtype = hdtype;
}

void Compiuter::save(ofstream &outfile) {
	outfile << this->toString() + "|";
	outfile << this->toStringPc() << endl;
}

string Compiuter::toStringPc() {
	string str = to_string(ram) + "|" + to_string(hdsize) + "|" + to_string(cpu) + "|" + gpu + "|" + hdtype;
	return str;
}
