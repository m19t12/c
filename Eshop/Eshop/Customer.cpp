#include "Customer.h"

Customer::Customer() {}

Customer::Customer(int afm, int phonenumber, string address) {
	this->afm = afm;
	this->phonenumber = phonenumber;
	this->address = address;
}

int Customer::getAfm() {
	return this->afm;
}

void Customer::setAfm(int afm) {
	this->afm = afm;
}

int Customer::getPhonenumber() {
	return this->phonenumber;
}

void Customer::setPhonenumber(int phonenumber) {
	this->phonenumber = phonenumber;
}

string Customer::getAddress() {
	return this->address;
}

void Customer::setAddress(string address) {
	this->address = address;
}