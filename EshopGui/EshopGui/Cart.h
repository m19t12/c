#pragma once
#include "Order.h"
#include "Physical.h"
#include "Company.h"
#include "Compiuter.h"
#include "Smartphone.h"
#include "Television.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for Cart
	/// </summary>
	public ref class Cart : public System::Windows::Forms::Form
	{
	public:
		Physical *physical;
		Company *company;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ListBox^  pc_lstbx;
	public:

	public:


	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ListBox^  phone_lstbx;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ListBox^  tv_lstbx;

	private: System::Windows::Forms::Button^  rmv_btn;
	private: System::Windows::Forms::Button^  ordr_btn;
	private: System::Windows::Forms::Button^  back_btn;
	public:
		String^ type;
	public:
		Cart(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		Cart(Physical *physical)
		{
			InitializeComponent();
			this->physical = physical;
			type = "physical";
			//
			//TODO: Add the constructor code here
			//
		}

		Cart(Company *company)
		{
			InitializeComponent();
			this->company = company;
			type = "company";
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Cart()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->pc_lstbx = (gcnew System::Windows::Forms::ListBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->phone_lstbx = (gcnew System::Windows::Forms::ListBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->tv_lstbx = (gcnew System::Windows::Forms::ListBox());
			this->rmv_btn = (gcnew System::Windows::Forms::Button());
			this->ordr_btn = (gcnew System::Windows::Forms::Button());
			this->back_btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(104, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Compiuters";
			// 
			// pc_lstbx
			// 
			this->pc_lstbx->FormattingEnabled = true;
			this->pc_lstbx->Location = System::Drawing::Point(12, 25);
			this->pc_lstbx->Name = L"pc_lstbx";
			this->pc_lstbx->Size = System::Drawing::Size(260, 95);
			this->pc_lstbx->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(107, 127);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(70, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"SmartPhones";
			// 
			// phone_lstbx
			// 
			this->phone_lstbx->FormattingEnabled = true;
			this->phone_lstbx->Location = System::Drawing::Point(12, 143);
			this->phone_lstbx->Name = L"phone_lstbx";
			this->phone_lstbx->Size = System::Drawing::Size(260, 95);
			this->phone_lstbx->TabIndex = 3;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(107, 245);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(60, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Televisions";
			// 
			// tv_lstbx
			// 
			this->tv_lstbx->FormattingEnabled = true;
			this->tv_lstbx->Location = System::Drawing::Point(12, 261);
			this->tv_lstbx->Name = L"tv_lstbx";
			this->tv_lstbx->Size = System::Drawing::Size(260, 95);
			this->tv_lstbx->TabIndex = 5;
			// 
			// rmv_btn
			// 
			this->rmv_btn->Location = System::Drawing::Point(12, 383);
			this->rmv_btn->Name = L"rmv_btn";
			this->rmv_btn->Size = System::Drawing::Size(75, 23);
			this->rmv_btn->TabIndex = 6;
			this->rmv_btn->Text = L"Remove";
			this->rmv_btn->UseVisualStyleBackColor = true;
			this->rmv_btn->Click += gcnew System::EventHandler(this, &Cart::rmv_btn_Click);
			// 
			// ordr_btn
			// 
			this->ordr_btn->Location = System::Drawing::Point(197, 383);
			this->ordr_btn->Name = L"ordr_btn";
			this->ordr_btn->Size = System::Drawing::Size(75, 23);
			this->ordr_btn->TabIndex = 7;
			this->ordr_btn->Text = L"Order";
			this->ordr_btn->UseVisualStyleBackColor = true;
			this->ordr_btn->Click += gcnew System::EventHandler(this, &Cart::ordr_btn_Click);
			// 
			// back_btn
			// 
			this->back_btn->Location = System::Drawing::Point(93, 411);
			this->back_btn->Name = L"back_btn";
			this->back_btn->Size = System::Drawing::Size(98, 23);
			this->back_btn->TabIndex = 8;
			this->back_btn->Text = L"Back";
			this->back_btn->UseVisualStyleBackColor = true;
			this->back_btn->Click += gcnew System::EventHandler(this, &Cart::back_btn_Click);
			// 
			// Cart
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 446);
			this->Controls->Add(this->back_btn);
			this->Controls->Add(this->ordr_btn);
			this->Controls->Add(this->rmv_btn);
			this->Controls->Add(this->tv_lstbx);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->phone_lstbx);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->pc_lstbx);
			this->Controls->Add(this->label1);
			this->Name = L"Cart";
			this->Text = L"Cart";
			this->Load += gcnew System::EventHandler(this, &Cart::Cart_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Cart_Load(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;

		if (type == "physical") {
			compiuterList = physical->getCompiuter();
			smartPhoneList = physical->getSmartphone();
			televisionList = physical->getTelevision();
		}
		else if (type == "company") {
			compiuterList = company->getCompiuter();
			smartPhoneList = company->getSmartphone();
			televisionList = company->getTelevision();
		}

		for (int i = 0; i < compiuterList.size(); i++) {
			pc_lstbx->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
		}

		for (int i = 0; i < smartPhoneList.size(); i++) {
			phone_lstbx->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
		}

		for (int i = 0; i < televisionList.size(); i++) {
			tv_lstbx->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
		}

	}
	private: System::Void back_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void rmv_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::ifstream infile;
		std::ofstream outfile;
		std::string str, temp;
		String^ pcTemp;
		String^ phTemp;
		String^ tvTemp;

		if (type == "physical") {
			if (pc_lstbx->SelectedItem) {
				infile.open(physical->getUsername() + "_" + "Cart" + ".dat");
				outfile.open("temp.dat");

				pcTemp = pc_lstbx->SelectedItem->ToString();

				while (!infile.eof()) {
					getline(infile, str);
					if (marshal_as<String^>(str) == pcTemp) {
					}
					else
						outfile << str << std::endl;
				}

				infile.close();
				outfile.close();

				temp = physical->getUsername() + "_" + "Cart" + ".dat";

				remove(temp.c_str());
				rename("temp.dat", temp.c_str());
				pc_lstbx->Items->Remove(pcTemp);
			}
			else if (phone_lstbx->SelectedItem) {
				infile.open(physical->getUsername() + "_" + "Cart" + ".dat");
				outfile.open("temp.dat");

				phTemp = phone_lstbx->SelectedItem->ToString();

				while (!infile.eof()) {
					getline(infile, str);
					if (marshal_as<String^>(str) == phTemp) {
					}
					else
						outfile << str << std::endl;
				}

				infile.close();
				outfile.close();

				temp = physical->getUsername() + "_" + "Cart" + ".dat";

				remove(temp.c_str());
				rename("temp.dat", temp.c_str());
				phone_lstbx->Items->Remove(phTemp);
			}
			else if (tv_lstbx->SelectedItem) {
				infile.open(physical->getUsername() + "_" + "Cart" + ".dat");
				outfile.open("temp.dat");

				tvTemp = tv_lstbx->SelectedItem->ToString();

				while (!infile.eof()) {
					getline(infile, str);
					if (marshal_as<String^>(str) == tvTemp) {
					}
					else
						outfile << str << std::endl;
				}

				infile.close();
				outfile.close();

				temp = physical->getUsername() + "_" + "Cart" + ".dat";

				remove(temp.c_str());
				rename("temp.dat", temp.c_str());
				tv_lstbx->Items->Remove(tvTemp);
			}
		}
		else if (type == "company") {
			if (pc_lstbx->SelectedItem) {
				compiuterList = company->getCompiuter();

				for (int i = 0; i < compiuterList.size(); i++) {
					if (pc_lstbx->SelectedIndex == compiuterList[i].getCode()) {
						compiuterList.erase(compiuterList.begin() + i);
					}
				}
				company->setCompiuterVector(compiuterList);
				pc_lstbx->Items->Remove(pc_lstbx->SelectedItem->ToString());
			}
			else if (phone_lstbx->SelectedItem) {
				smartPhoneList = company->getSmartphone();

				for (int i = 0; i < smartPhoneList.size(); i++) {
					if (phone_lstbx->SelectedIndex == smartPhoneList[i].getCode()) {
						smartPhoneList.erase(smartPhoneList.begin() + i);
					}
				}
				company->setSmartPhoneVector(smartPhoneList);
				phone_lstbx->Items->Remove(phone_lstbx->SelectedItem->ToString());
			}
			else if (tv_lstbx->SelectedItem) {
				televisionList = company->getTelevision();

				for (int i = 0; i < televisionList.size(); i++) {
					if (tv_lstbx->SelectedIndex == televisionList[i].getCode()) {
						televisionList.erase(televisionList.begin() + i);
					}
				}
				company->setTelevisionVector(televisionList);
				tv_lstbx->Items->Remove(tv_lstbx->SelectedItem->ToString());
			}
		}
	}
	private: System::Void ordr_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::ofstream outfile;
		std::ifstream infile;
		std::string str;
		Order order;
		int counter = 0;

		if (type == "physical") {
			compiuterList = physical->getCompiuter();
			smartPhoneList = physical->getSmartphone();
			televisionList = physical->getTelevision();

			outfile.open(physical->getUsername() + "_" + "Order.dat", std::ios::app);
			infile.open(physical->getUsername() + "_" + "Order.dat");
			getline(infile, str);

			for (int i = 0; i < compiuterList.size(); i++) {
				order.Calculation(compiuterList[i].getPrice());
			}
			for (int i = 0; i < smartPhoneList.size(); i++) {
				order.Calculation(smartPhoneList[i].getPrice());
			}
			for (int i = 0; i < televisionList.size(); i++) {
				order.Calculation(televisionList[i].getPrice());
			}

			if (str == "") {
				order.setId(1);
			}
			else {
				while (str != "") {
					getline(infile, str);
					counter++;
				}
				order.setId(counter);
			}
			infile.close();

			order.setStatus("PENDING");

			for (int i = 0; i < compiuterList.size(); i++) {
				compiuterList.erase(compiuterList.begin() + i, compiuterList.end());
			}
			for (int i = 0; i < smartPhoneList.size(); i++) {
				smartPhoneList.erase(smartPhoneList.begin() + i, smartPhoneList.end());
			}
			for (int i = 0; i < televisionList.size(); i++) {
				televisionList.erase(televisionList.begin() + i, televisionList.end());
			}

			physical->setCompiuterVector(compiuterList);
			physical->setSmartPhoneVector(smartPhoneList);
			physical->setTelevisionVector(televisionList);

			order.save(outfile);
		}
		else if (type == "company") {
			compiuterList = company->getCompiuter();
			smartPhoneList = company->getSmartphone();
			televisionList = company->getTelevision();

			outfile.open(company->getUsername() + "_" + "Order.dat", std::ios::app);
			infile.open(company->getUsername() + "_" + "Order.dat");
			getline(infile, str);

			for (int i = 0; i < compiuterList.size(); i++) {
				order.Calculation(compiuterList[i].getPrice()*(company->getDiscount() / 100));
			}
			for (int i = 0; i < smartPhoneList.size(); i++) {
				order.Calculation(smartPhoneList[i].getPrice()*(company->getDiscount() / 100));
			}
			for (int i = 0; i < televisionList.size(); i++) {
				order.Calculation(televisionList[i].getPrice()*(company->getDiscount() / 100));
			}

			if (str == "") {
				order.setId(1);
			}
			else {
				while (str != "") {
					getline(infile, str);
					counter++;
				}
				order.setId(counter);
			}
			infile.close();

			order.setStatus("PENDING");

			for (int i = 0; i < compiuterList.size(); i++) {
				compiuterList.erase(compiuterList.begin() + i, compiuterList.end());
			}
			for (int i = 0; i < smartPhoneList.size(); i++) {
				smartPhoneList.erase(smartPhoneList.begin() + i, smartPhoneList.end());
			}
			for (int i = 0; i < televisionList.size(); i++) {
				televisionList.erase(televisionList.begin() + i, televisionList.end());
			}

			company->setCompiuterVector(compiuterList);
			company->setSmartPhoneVector(smartPhoneList);
			company->setTelevisionVector(televisionList);

			order.save(outfile);
		}
		outfile.close();
		
		pc_lstbx->Items->Clear();
		phone_lstbx->Items->Clear();
		tv_lstbx->Items->Clear();
	}
};
}
