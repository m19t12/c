#include "Order.h"

Order::Order() {
	cost = 0;
}


Order::Order(int id, double cost, std::string status) {
	this->id = id;
	this->cost = cost;
	this->status = status;
}

Order::Order(std::string strOr) {
	std::istringstream issin(strOr);
	std::string split[3];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}
	this->id = std::stoi(split[0]);
	this->cost = std::stod(split[1]);
	this->status = split[2];
}

void Order::Calculation(double cost) {
		this->cost += cost;
}

int Order::getId() {
	return this->id;
}

void Order::setId(int id) {
	this->id = id;
}

double Order::getCost() {
	return this->cost;
}

void Order::setCost(double cost) {
	this->cost = cost;
}

void Order::setStatus(std::string status) {
	this->status = status;
}

std::string Order::getStatus() {
	return this->status;
}

void Order::save(std::ofstream &outfile) {
	outfile << this->toString() << std::endl;
}

std::string Order::toString() {
	std::string str = std::to_string(id) + "|" + std::to_string(cost) + "|" + status;
	return str;
}