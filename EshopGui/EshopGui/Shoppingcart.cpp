#include "Shoppingcart.h"

using namespace std;

ShoppingCart::ShoppingCart() {};

void ShoppingCart::addCompiuter(Compiuter c) {
	compiuter.push_back(c);
}

vector<Compiuter> ShoppingCart::getCompiuter() {
	return this->compiuter;
}

void ShoppingCart::addSmartphone(SmartPhone s) {
	smartphone.push_back(s);
}

vector<SmartPhone> ShoppingCart::getSmartphone() {
	return this->smartphone;
}

void ShoppingCart::addTelevision(Television t) {
	television.push_back(t);
}

vector<Television> ShoppingCart::getTelevision() {
	return this->television;
}

void ShoppingCart::setCompiuterVector(vector<Compiuter> compiuter) {
	this->compiuter = compiuter;
}

void ShoppingCart::setSmartPhoneVector(vector<SmartPhone> smartphone) {
	this->smartphone = smartphone;
}

void ShoppingCart::setTelevisionVector(vector<Television> television) {
	this->television = television;
}