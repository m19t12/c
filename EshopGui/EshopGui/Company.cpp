#include "Company.h"

using namespace std;

Company::Company() {}

Company::Company(string managername, string managersurename, string name, double discount, int fax, string security) {
	manager.name = managername;
	manager.surename = managersurename;
	this->name = name;
	this->discount = discount;
	this->fax = fax;
	this->security = security;
}

Company::Company(string strCo) {
	istringstream issin(strCo);
	string split[10];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->setUsername(split[0]);
	this->setAfm(stoi(split[1]));
	this->setAddress(split[2]);
	this->setPhonenumber(stoi(split[3]));
	this->manager.name = split[4];
	this->manager.surename = split[5];
	this->name = split[6];
	this->discount = stod(split[7]);
	this->fax = stoi(split[8]);
	this->security = split[9];

}

string Company::getManagerName() {
	return this->manager.name;
}

void Company::setManagerName(string managername) {
	manager.name = managername;
}

string Company::getManagerSurename() {
	return this->manager.surename;
}

void Company::setManagerSurename(string managersurename) {
	manager.surename = managersurename;
}

string Company::getName() {
	return this->name;
}

void Company::setName(string name) {
	this->name = name;
}

double Company::getDiscount() {
	return this->discount;
}

void Company::setDiscount(double discount) {
	this->discount = discount;
}

int Company::getFax() {
	return this->fax;
}

void Company::setFax(int fax) {
	this->fax = fax;
}

string Company::getSecurity() {
	return this->security;
}

void Company::setSecurity(string security) {
	this->security = security;
}

void Company::save(ofstream &outfile) {
	outfile << this->toStringCo() << endl;
}

string Company::toStringCo() {
	string str = this->getUsername() + "|" + to_string(this->getAfm()) + "|" + this->getAddress() + "|" + to_string(this->getPhonenumber()) + "|" + manager.name + "|" + manager.surename + "|" + name + "|" + to_string(discount) + "|" + to_string(fax) + "|" + security;
	return str;
}
