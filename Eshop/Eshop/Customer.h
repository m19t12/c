#ifndef CUSTOMER_H
#define CUSTOMER_H
#include "User.h"
#include "Shoppingcart.h"

class Customer : public ShoppingCart, public User {

public:
	Customer();

	Customer(int afm, int phonenumber, string address);

	int getAfm();

	void setAfm(int afm);

	int getPhonenumber();

	void setPhonenumber(int phonenumber);

	string getAddress();

	void setAddress(string address);

private:
	int afm, phonenumber;
	string address;
};

#endif // !CUSTOMER_H

