#include "Loginscreen.h"

using namespace std;



//othoni login
LoginScreen::LoginScreen() {
	system("CLS");
	cout << "---------------------" << endl;
	cout << "Welcome to Eshop     " << endl;
	cout << "---------------------" << endl;
	cout << "A) : Login           " << endl;
	cout << "B) : Forgot Password " << endl;
	cout << "C) : Exit            " << endl;
	cout << "---------------------" << endl;
	cin >> action;

	if (action == 'A') {
		Login();
		code = 1;
	}
	else if (action == 'B') {
		cout << "Forgot Password" << endl;
		RecoverAccount();
	}
	else if (action == 'C') {
		cout << "GoodBye" << endl;
		code = 0;
	}
	else
		cout << "Code Not Supported" << endl;
}
void LoginScreen::Login() {
	ifstream infile;

	//diauazei olous tous xristes pou eiparxoun sto arxio user.dat
	infile.open("user.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			users.push_back(User(str));
		}
	}

	infile.close();

	do {
		cout << "----------------------------------------------------" << endl;
		cout << "Please Give Username and Password                   " << endl;
		cout << "----------------------------------------------------" << endl;
		cout << "Username : ";
		cin >> username;
		cout << "Password : ";
		cin >> password;

		// elenxos gia to an iparxi to username kai to password kai an iparxi girizi ti tipos einai
		for (int i = 0; i < users.size(); i++) {
			if (username == users[i].getUsername() && password == users[i].getPassword()) {
				type = users[i].getType();
				code = 0;
			}
		}

		if (type == "")
			cout << "Wrong Username Or Password Try Again" << endl;

	} while (code == 1);

	/* diauazei ton user pou exei to sigkekrimeno username kai ton antistixo tipo
	   kai ton pernaei se mia metavliti tou antistixou tipou*/
	if (type == "physical") {

		infile.open(type + ".dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				physicalUsers.push_back(Physical(str));
			}
		}

		infile.close();

		for (int i = 0; i < physicalUsers.size(); i++) {
			if (username == physicalUsers[i].getUsername()) {
				physical = physicalUsers[i];
			}
		}
	}
	else if (type == "company") {

		infile.open(type + ".dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				companyUsers.push_back(Company(str));
			}
		}

		infile.close();

		for (int i = 0; i < companyUsers.size(); i++) {
			if (username == companyUsers[i].getUsername()) {
				company = companyUsers[i];
			}
		}
	}
	else {
		infile.open(type + ".dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				adminUsers.push_back(Admin(str));
			}
		}

		infile.close();

		for (int i = 0; i < adminUsers.size(); i++) {
			if (username == adminUsers[i].getUsername()) {
				admin = adminUsers[i];
			}
		}
	}
	// fortoni ta antikimena pou exei o siggekrimenos xristis sto kalathi tou
	LoadCart();
}
void LoginScreen::LoadCart() {
	ifstream infile;
	string str;
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;

	// fortoni to arxio pou periexi ta antikimena pou vriskonte sto kalathi agoron tou xristi me username
	infile.open(username + "_" + "Cart" + ".dat");
	/* to arxio username_Cart.dat einai xorismenw stis 
	   3 katigories proionton molis brei tin antistixi katigoria diauazei ta antikimena pou eiparxoun */
	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			if (str == "compiuter") {
				getline(infile, str);
				while (str.length() > 1) {
					compiuterList.push_back(Compiuter(str));
					getline(infile, str);
				}
			}
			else if (str == "smartphone") {
				getline(infile, str);
				while (str.length() > 1) {
					smartPhoneList.push_back(SmartPhone(str));
					getline(infile, str);
				}
			}
			else if (str == "television") {
				getline(infile, str);
				while (str.length() > 1) {
					televisionList.push_back(str);
					getline(infile, str);
				}
			}
		}
	}
	infile.close();
	// elenxos an o xristis einai physical h company kai analogos pernaei sto kalathi agoron to antistixo proion
	if (type == "physical") {
		for (int i = 0; i < compiuterList.size(); i++) {
			physical.addCompiuter(compiuterList[i]);
		}

		for (int i = 0; i < smartPhoneList.size(); i++) {
			physical.addSmartphone(smartPhoneList[i]);
		}

		for (int i = 0; i < televisionList.size(); i++) {
			physical.addTelevision(televisionList[i]);
		}
	}
	else {
		for (int i = 0; i < compiuterList.size(); i++) {
			company.addCompiuter(compiuterList[i]);
		}

		for (int i = 0; i < smartPhoneList.size(); i++) {
			company.addSmartphone(smartPhoneList[i]);
		}

		for (int i = 0; i < televisionList.size(); i++) {
			company.addTelevision(televisionList[i]);
		}
	}
}

void LoginScreen::RecoverAccount() {
	/* se periptosi pou eimaste xristis kai exoume xexasi ton kodiko mas 
	mporoume na ton anaktisoume vazontas to idiko sinthimatiko 
	h othoni prosomiazi ta password recovery tools pou exei kathe login screen*/
	cout << "Give Name: " << endl;
	cin >> name;
	cout << "Give Surename: " << endl;
	cin >> surename;
	cout << "Give Type physical or company: " << endl;
	cin >> type;
	cout << "Give Security pass: " << endl;
	cin >> security;

	if (type == "physical") {
		ifstream infile;
		infile.open(type + ".dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				physicalUsers.push_back(Physical(str));
			}
		}
		infile.close();

		for (int i = 0; i < physicalUsers.size(); i++) {
			if (name == physicalUsers[i].getName() && surename == physicalUsers[i].getSurename() && security == physicalUsers[i].getSecurity()) {
				username = physicalUsers[i].getUsername();
			}
		}
	}
	else if (type == "company") {
		ifstream infile;
		infile.open(type + ".dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				companyUsers.push_back(Company(str));
			}
		}
		infile.close();

		for (int i = 0; i < companyUsers.size(); i++) {
			if (name == companyUsers[i].getManagerName() && surename == companyUsers[i].getManagerSurename() && security == companyUsers[i].getSecurity()) {
				username = companyUsers[i].getUsername();
			}
		}
	}
	ifstream infile;
	infile.open("user.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			users.push_back(User(str));
		}
	}
	infile.close();
	//ektiposi tou username kai tou password an iparxi
	for (int i = 0; i < users.size(); i++) {
		if (username == users[i].getUsername()) {
			cout << "Username: " + users[i].getUsername() << endl;
			cout << "Password: " + users[i].getPassword() << endl;
			code = 0;
		}
	}
	if (code == 1)
		cout << "No Such User Exists" << endl;
}

Physical LoginScreen::getPhysicalUser() {
	return this->physical;
}

Company LoginScreen::getCompanyUser() {
	return this->company;
}

Admin LoginScreen::getAdminUser() {
	return this->admin;
}

string LoginScreen::getType() {
	return this->type;
}

int LoginScreen::getCode() {
	return this->code;
}

string LoginScreen::getUsername() {
	return this->username;
}