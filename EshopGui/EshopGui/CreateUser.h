#pragma once
#include "User.h"
#include "Physical.h"
#include "Company.h"
#include "Admin.h"
#include <fstream>
#include <vector>
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for CreateUser
	/// </summary>
	public ref class CreateUser : public System::Windows::Forms::Form
	{
	public:
		CreateUser(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~CreateUser()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;

	private: System::Windows::Forms::Label^  label4;


	private: System::Windows::Forms::TextBox^  password_txt;

	private: System::Windows::Forms::TextBox^  username_txt;

	private: System::Windows::Forms::Panel^  physical_pnl;
	private: System::Windows::Forms::TextBox^  security_txt;


	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::TextBox^  am_txt;

	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TextBox^  surename_txt;

	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::TextBox^  name_txt;

	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::TextBox^  phnumber_txt;

	private: System::Windows::Forms::TextBox^  address_txt;

	private: System::Windows::Forms::TextBox^  afm_txt;

	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Panel^  company_pnl;
	private: System::Windows::Forms::TextBox^  cmp_dis_txt;


	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::TextBox^  cmp_name_txt;

	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::TextBox^  cmp_masure_txt;

	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::TextBox^  cmp_mname_txt;

	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::TextBox^  cmp_phnum_txt;

	private: System::Windows::Forms::TextBox^  cmp_address_txt;

	private: System::Windows::Forms::TextBox^  cmp_afm_txt;

	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::TextBox^  cmp_fax_txt;

	private: System::Windows::Forms::Label^  label21;
	private: System::Windows::Forms::Button^  create_btn;

	private: System::Windows::Forms::Button^  back_btn;

	private: System::Windows::Forms::ComboBox^  type_cmbx;
	private: System::Windows::Forms::Panel^  admin_pnl;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  adm_sure_txt;

	private: System::Windows::Forms::TextBox^  adm_name_txt;

	private: System::Windows::Forms::Label^  label27;
	private: System::Windows::Forms::Label^  label28;
















	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->password_txt = (gcnew System::Windows::Forms::TextBox());
			this->username_txt = (gcnew System::Windows::Forms::TextBox());
			this->physical_pnl = (gcnew System::Windows::Forms::Panel());
			this->security_txt = (gcnew System::Windows::Forms::TextBox());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->am_txt = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->surename_txt = (gcnew System::Windows::Forms::TextBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->name_txt = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->phnumber_txt = (gcnew System::Windows::Forms::TextBox());
			this->address_txt = (gcnew System::Windows::Forms::TextBox());
			this->afm_txt = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->company_pnl = (gcnew System::Windows::Forms::Panel());
			this->cmp_fax_txt = (gcnew System::Windows::Forms::TextBox());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->cmp_dis_txt = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->cmp_name_txt = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->cmp_masure_txt = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->cmp_mname_txt = (gcnew System::Windows::Forms::TextBox());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->cmp_phnum_txt = (gcnew System::Windows::Forms::TextBox());
			this->cmp_address_txt = (gcnew System::Windows::Forms::TextBox());
			this->cmp_afm_txt = (gcnew System::Windows::Forms::TextBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->create_btn = (gcnew System::Windows::Forms::Button());
			this->back_btn = (gcnew System::Windows::Forms::Button());
			this->type_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->admin_pnl = (gcnew System::Windows::Forms::Panel());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->adm_sure_txt = (gcnew System::Windows::Forms::TextBox());
			this->adm_name_txt = (gcnew System::Windows::Forms::TextBox());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->physical_pnl->SuspendLayout();
			this->company_pnl->SuspendLayout();
			this->admin_pnl->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 30);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(55, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Username";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 62);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(53, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Password";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(12, 113);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(0, 13);
			this->label4->TabIndex = 3;
			// 
			// password_txt
			// 
			this->password_txt->Location = System::Drawing::Point(167, 62);
			this->password_txt->Name = L"password_txt";
			this->password_txt->PasswordChar = '*';
			this->password_txt->Size = System::Drawing::Size(100, 20);
			this->password_txt->TabIndex = 5;
			// 
			// username_txt
			// 
			this->username_txt->Location = System::Drawing::Point(167, 30);
			this->username_txt->Name = L"username_txt";
			this->username_txt->Size = System::Drawing::Size(100, 20);
			this->username_txt->TabIndex = 6;
			// 
			// physical_pnl
			// 
			this->physical_pnl->Controls->Add(this->security_txt);
			this->physical_pnl->Controls->Add(this->label12);
			this->physical_pnl->Controls->Add(this->am_txt);
			this->physical_pnl->Controls->Add(this->label11);
			this->physical_pnl->Controls->Add(this->surename_txt);
			this->physical_pnl->Controls->Add(this->label10);
			this->physical_pnl->Controls->Add(this->name_txt);
			this->physical_pnl->Controls->Add(this->label9);
			this->physical_pnl->Controls->Add(this->phnumber_txt);
			this->physical_pnl->Controls->Add(this->address_txt);
			this->physical_pnl->Controls->Add(this->afm_txt);
			this->physical_pnl->Controls->Add(this->label8);
			this->physical_pnl->Controls->Add(this->label7);
			this->physical_pnl->Controls->Add(this->label6);
			this->physical_pnl->Controls->Add(this->label5);
			this->physical_pnl->Location = System::Drawing::Point(1, 149);
			this->physical_pnl->Name = L"physical_pnl";
			this->physical_pnl->Size = System::Drawing::Size(339, 252);
			this->physical_pnl->TabIndex = 7;
			this->physical_pnl->Visible = false;
			// 
			// security_txt
			// 
			this->security_txt->Location = System::Drawing::Point(184, 210);
			this->security_txt->Name = L"security_txt";
			this->security_txt->Size = System::Drawing::Size(100, 20);
			this->security_txt->TabIndex = 36;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(35, 210);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(71, 13);
			this->label12->TabIndex = 35;
			this->label12->Text = L"Security Pass";
			// 
			// am_txt
			// 
			this->am_txt->Location = System::Drawing::Point(184, 182);
			this->am_txt->Name = L"am_txt";
			this->am_txt->Size = System::Drawing::Size(100, 20);
			this->am_txt->TabIndex = 34;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(35, 182);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(22, 13);
			this->label11->TabIndex = 33;
			this->label11->Text = L"Am";
			// 
			// surename_txt
			// 
			this->surename_txt->Location = System::Drawing::Point(184, 151);
			this->surename_txt->Name = L"surename_txt";
			this->surename_txt->Size = System::Drawing::Size(100, 20);
			this->surename_txt->TabIndex = 32;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(35, 154);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(55, 13);
			this->label10->TabIndex = 31;
			this->label10->Text = L"Surename";
			// 
			// name_txt
			// 
			this->name_txt->Location = System::Drawing::Point(184, 119);
			this->name_txt->Name = L"name_txt";
			this->name_txt->Size = System::Drawing::Size(100, 20);
			this->name_txt->TabIndex = 30;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(35, 119);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(35, 13);
			this->label9->TabIndex = 29;
			this->label9->Text = L"Name";
			// 
			// phnumber_txt
			// 
			this->phnumber_txt->Location = System::Drawing::Point(184, 88);
			this->phnumber_txt->Name = L"phnumber_txt";
			this->phnumber_txt->Size = System::Drawing::Size(100, 20);
			this->phnumber_txt->TabIndex = 28;
			// 
			// address_txt
			// 
			this->address_txt->Location = System::Drawing::Point(184, 55);
			this->address_txt->Name = L"address_txt";
			this->address_txt->Size = System::Drawing::Size(100, 20);
			this->address_txt->TabIndex = 27;
			// 
			// afm_txt
			// 
			this->afm_txt->Location = System::Drawing::Point(184, 19);
			this->afm_txt->Name = L"afm_txt";
			this->afm_txt->Size = System::Drawing::Size(100, 20);
			this->afm_txt->TabIndex = 26;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(33, 91);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(73, 13);
			this->label8->TabIndex = 25;
			this->label8->Text = L"Phonenumber";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(33, 58);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(45, 13);
			this->label7->TabIndex = 24;
			this->label7->Text = L"Address";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(35, 26);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(25, 13);
			this->label6->TabIndex = 23;
			this->label6->Text = L"Afm";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(128, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(46, 13);
			this->label5->TabIndex = 22;
			this->label5->Text = L"Physical";
			// 
			// company_pnl
			// 
			this->company_pnl->Controls->Add(this->cmp_fax_txt);
			this->company_pnl->Controls->Add(this->label21);
			this->company_pnl->Controls->Add(this->cmp_dis_txt);
			this->company_pnl->Controls->Add(this->label13);
			this->company_pnl->Controls->Add(this->cmp_name_txt);
			this->company_pnl->Controls->Add(this->label14);
			this->company_pnl->Controls->Add(this->cmp_masure_txt);
			this->company_pnl->Controls->Add(this->label15);
			this->company_pnl->Controls->Add(this->cmp_mname_txt);
			this->company_pnl->Controls->Add(this->label16);
			this->company_pnl->Controls->Add(this->cmp_phnum_txt);
			this->company_pnl->Controls->Add(this->cmp_address_txt);
			this->company_pnl->Controls->Add(this->cmp_afm_txt);
			this->company_pnl->Controls->Add(this->label17);
			this->company_pnl->Controls->Add(this->label18);
			this->company_pnl->Controls->Add(this->label19);
			this->company_pnl->Controls->Add(this->label20);
			this->company_pnl->Location = System::Drawing::Point(1, 149);
			this->company_pnl->Name = L"company_pnl";
			this->company_pnl->Size = System::Drawing::Size(336, 272);
			this->company_pnl->TabIndex = 37;
			this->company_pnl->Visible = false;
			// 
			// cmp_fax_txt
			// 
			this->cmp_fax_txt->Location = System::Drawing::Point(184, 238);
			this->cmp_fax_txt->Name = L"cmp_fax_txt";
			this->cmp_fax_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_fax_txt->TabIndex = 38;
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(35, 239);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(24, 13);
			this->label21->TabIndex = 37;
			this->label21->Text = L"Fax";
			// 
			// cmp_dis_txt
			// 
			this->cmp_dis_txt->Location = System::Drawing::Point(184, 210);
			this->cmp_dis_txt->Name = L"cmp_dis_txt";
			this->cmp_dis_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_dis_txt->TabIndex = 36;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(35, 210);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(49, 13);
			this->label13->TabIndex = 35;
			this->label13->Text = L"Discount";
			// 
			// cmp_name_txt
			// 
			this->cmp_name_txt->Location = System::Drawing::Point(184, 182);
			this->cmp_name_txt->Name = L"cmp_name_txt";
			this->cmp_name_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_name_txt->TabIndex = 34;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(41, 182);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(82, 13);
			this->label14->TabIndex = 33;
			this->label14->Text = L"Company Name";
			// 
			// cmp_masure_txt
			// 
			this->cmp_masure_txt->Location = System::Drawing::Point(184, 151);
			this->cmp_masure_txt->Name = L"cmp_masure_txt";
			this->cmp_masure_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_masure_txt->TabIndex = 32;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(38, 154);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(100, 13);
			this->label15->TabIndex = 31;
			this->label15->Text = L"Manager Surename";
			// 
			// cmp_mname_txt
			// 
			this->cmp_mname_txt->Location = System::Drawing::Point(184, 119);
			this->cmp_mname_txt->Name = L"cmp_mname_txt";
			this->cmp_mname_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_mname_txt->TabIndex = 30;
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(38, 119);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(80, 13);
			this->label16->TabIndex = 29;
			this->label16->Text = L"Manager Name";
			// 
			// cmp_phnum_txt
			// 
			this->cmp_phnum_txt->Location = System::Drawing::Point(184, 88);
			this->cmp_phnum_txt->Name = L"cmp_phnum_txt";
			this->cmp_phnum_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_phnum_txt->TabIndex = 28;
			// 
			// cmp_address_txt
			// 
			this->cmp_address_txt->Location = System::Drawing::Point(184, 55);
			this->cmp_address_txt->Name = L"cmp_address_txt";
			this->cmp_address_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_address_txt->TabIndex = 27;
			// 
			// cmp_afm_txt
			// 
			this->cmp_afm_txt->Location = System::Drawing::Point(184, 19);
			this->cmp_afm_txt->Name = L"cmp_afm_txt";
			this->cmp_afm_txt->Size = System::Drawing::Size(100, 20);
			this->cmp_afm_txt->TabIndex = 26;
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(35, 88);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(73, 13);
			this->label17->TabIndex = 25;
			this->label17->Text = L"Phonenumber";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(35, 55);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(45, 13);
			this->label18->TabIndex = 24;
			this->label18->Text = L"Address";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(38, 26);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(25, 13);
			this->label19->TabIndex = 23;
			this->label19->Text = L"Afm";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(129, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(51, 13);
			this->label20->TabIndex = 22;
			this->label20->Text = L"Company";
			// 
			// create_btn
			// 
			this->create_btn->Location = System::Drawing::Point(32, 427);
			this->create_btn->Name = L"create_btn";
			this->create_btn->Size = System::Drawing::Size(75, 23);
			this->create_btn->TabIndex = 38;
			this->create_btn->Text = L"Create";
			this->create_btn->UseVisualStyleBackColor = true;
			this->create_btn->Click += gcnew System::EventHandler(this, &CreateUser::create_btn_Click);
			// 
			// back_btn
			// 
			this->back_btn->Location = System::Drawing::Point(185, 427);
			this->back_btn->Name = L"back_btn";
			this->back_btn->Size = System::Drawing::Size(100, 23);
			this->back_btn->TabIndex = 39;
			this->back_btn->Text = L"Back";
			this->back_btn->UseVisualStyleBackColor = true;
			this->back_btn->Click += gcnew System::EventHandler(this, &CreateUser::back_btn_Click);
			// 
			// type_cmbx
			// 
			this->type_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->type_cmbx->FormattingEnabled = true;
			this->type_cmbx->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->type_cmbx->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"physical", L"company", L"admin" });
			this->type_cmbx->Location = System::Drawing::Point(90, 105);
			this->type_cmbx->Name = L"type_cmbx";
			this->type_cmbx->Size = System::Drawing::Size(121, 21);
			this->type_cmbx->TabIndex = 40;
			this->type_cmbx->SelectedIndexChanged += gcnew System::EventHandler(this, &CreateUser::type_cmbx_SelectedIndexChanged);
			// 
			// admin_pnl
			// 
			this->admin_pnl->Controls->Add(this->label3);
			this->admin_pnl->Controls->Add(this->adm_sure_txt);
			this->admin_pnl->Controls->Add(this->adm_name_txt);
			this->admin_pnl->Controls->Add(this->label27);
			this->admin_pnl->Controls->Add(this->label28);
			this->admin_pnl->Location = System::Drawing::Point(4, 149);
			this->admin_pnl->Name = L"admin_pnl";
			this->admin_pnl->Size = System::Drawing::Size(333, 269);
			this->admin_pnl->TabIndex = 39;
			this->admin_pnl->Visible = false;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(128, 19);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(36, 13);
			this->label3->TabIndex = 44;
			this->label3->Text = L"Admin";
			// 
			// adm_sure_txt
			// 
			this->adm_sure_txt->Location = System::Drawing::Point(196, 135);
			this->adm_sure_txt->Name = L"adm_sure_txt";
			this->adm_sure_txt->Size = System::Drawing::Size(100, 20);
			this->adm_sure_txt->TabIndex = 43;
			// 
			// adm_name_txt
			// 
			this->adm_name_txt->Location = System::Drawing::Point(196, 99);
			this->adm_name_txt->Name = L"adm_name_txt";
			this->adm_name_txt->Size = System::Drawing::Size(100, 20);
			this->adm_name_txt->TabIndex = 42;
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(47, 135);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(55, 13);
			this->label27->TabIndex = 40;
			this->label27->Text = L"Surename";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(50, 106);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(35, 13);
			this->label28->TabIndex = 39;
			this->label28->Text = L"Name";
			// 
			// CreateUser
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(343, 462);
			this->Controls->Add(this->admin_pnl);
			this->Controls->Add(this->type_cmbx);
			this->Controls->Add(this->back_btn);
			this->Controls->Add(this->create_btn);
			this->Controls->Add(this->company_pnl);
			this->Controls->Add(this->physical_pnl);
			this->Controls->Add(this->username_txt);
			this->Controls->Add(this->password_txt);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"CreateUser";
			this->Text = L"CreateUser";
			this->physical_pnl->ResumeLayout(false);
			this->physical_pnl->PerformLayout();
			this->company_pnl->ResumeLayout(false);
			this->company_pnl->PerformLayout();
			this->admin_pnl->ResumeLayout(false);
			this->admin_pnl->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void type_cmbx_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		String^ type;
		type = type_cmbx->Text;

		if (type == "physical") {
			company_pnl->Visible = false;
			admin_pnl->Visible = false;
			physical_pnl->Visible = true;

		}
		else if (type == "company") {
			physical_pnl->Visible = false;
			admin_pnl->Visible = false;
			company_pnl->Visible = true;
		}
		else if (type == "admin") {
			company_pnl->Visible = false;
			physical_pnl->Visible = false;
			admin_pnl->Visible = true;
		}
	}
	private: System::Void create_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::vector<User> users;
		Admin admin;
		User user;
		Physical physical;
		Company company;
		std::string str, temp;
		std::ofstream outfile;
		std::ifstream infile;
		bool read = true;

		outfile.open("user.dat", std::ios::app);

		infile.open("user.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				users.push_back(User(str));
			}
		}
		infile.close();

		while (read) {
			String^ username = "";
			do {
				for (int i = 0; i < users.size(); i++) {
					if (username == marshal_as<String^>(users[i].getUsername())) {
						temp = users[i].getUsername();
					}
				}
				username = username_txt->Text;
			} while (marshal_as<String^>(temp) == username);
			user.setUsername(marshal_as<std::string>(username));
			String^ password;
			password = password_txt->Text;
			user.setPassword(marshal_as<std::string>(password));
			String^ type;
			type = type_cmbx->Text;
			user.setType(marshal_as<std::string>(type));

			user.save(outfile);

			break;
		}
		outfile.close();

		if (type_cmbx->Text == "physical") {
			physical.setUsername(user.getUsername());

			outfile.open("physical.dat", std::ios::app);

			while (read) {
				physical.setAfm(Convert::ToInt32(afm_txt->Text));

				physical.setAddress(marshal_as<std::string>(address_txt->Text));

				physical.setPhonenumber(Convert::ToInt32(phnumber_txt->Text));

				physical.setName(marshal_as<std::string>(name_txt->Text));

				physical.setSurename(marshal_as<std::string>(surename_txt->Text));

				physical.setAm(marshal_as<std::string>(am_txt->Text));

				physical.setSecurity(marshal_as<std::string>(security_txt->Text));

				physical.save(outfile);

				break;
			}

			outfile.close();
		}
		else if (type_cmbx->Text == "company") {
			company.setUsername(user.getUsername());

			outfile.open("company.dat", std::ios::app);
			while (read) {

				company.setAfm(Convert::ToInt32(cmp_afm_txt->Text));

				company.setAddress(marshal_as<std::string>(cmp_address_txt->Text));

				company.setPhonenumber(Convert::ToInt32(cmp_phnum_txt->Text));

				company.setManagerName(marshal_as<std::string>(cmp_mname_txt->Text));

				company.setManagerSurename(marshal_as<std::string>(cmp_masure_txt->Text));

				company.setName(marshal_as<std::string>(cmp_name_txt->Text));

				company.setDiscount(Convert::ToDouble(cmp_dis_txt->Text));

				company.setFax(Convert::ToInt32(cmp_fax_txt->Text));

				company.save(outfile);
				break;
			}

			outfile.close();
		}
		else {
			admin.setUsername(user.getUsername());

			outfile.open("admin.dat", std::ios::app);

			while (read) {

				admin.setName(marshal_as<std::string>(adm_name_txt->Text));

				admin.setSurename(marshal_as<std::string>(adm_sure_txt->Text));

				admin.save(outfile);
				break;
			}
			outfile.close();
		}
		MessageBox::Show("User Succesfuly Created");
	}
	private: System::Void back_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	};
}
