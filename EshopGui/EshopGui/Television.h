#ifndef TELEVISION_H
#define TELEVISION_H
#include "Product.h"

class Television : public Product {
public:
	Television();

	Television(int curve, std::string manufacturer, bool threed);

	Television(std::string strTv);

	int getCurve();

	void setCurve(int curve);

	bool getThreed();

	void setThreed(bool threed);

	void save(std::ofstream &outfile);

	std::string toStringTv();

private:
	int curve;
	std::string manufacturer;
	bool threed;
};

#endif
