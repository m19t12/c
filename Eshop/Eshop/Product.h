#ifndef PRODUCT_H
#define PRODUCT_H
#include <string>
#include <iomanip>

using namespace std;

class Product {
public:
	Product();

	Product(int code, string model, string manufacturer, string description, bool photo, double price);

	int getCode();

	void setCode(int code);

	string getModel();

	void setModel(string model);

	string getManufacturer();

	void setManufacturer(string manufacturer);

	string getDescription();

	void setDescription(string description);

	bool getPhoto();

	void setPhoto(bool photo);

	double getPrice();

	void setPrice(double price);

	string toString();

private:
	int code;
	string model, manufacturer, description;
	bool photo;
	double price;
};

#endif
