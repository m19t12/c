#pragma once
#include "Physical.h"
#include "Company.h"
#include "Items.h"
#include "Cart.h"
#include "OrderUser.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for UserScreen
	/// </summary>
	public ref class UserScreen : public System::Windows::Forms::Form
	{
	public:
		Physical* physical;
		Company* company;
	private: System::Windows::Forms::Button^  button1;
	public:
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
			 String^ type;
	public:
		UserScreen(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		UserScreen(Physical *physical)
		{
			InitializeComponent();
			this->physical = physical;
			type = "physical";
			//
			//TODO: Add the constructor code here
			//
		}

		UserScreen(Company *company)
		{
			InitializeComponent();
			this->company = company;
			type = "company";
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~UserScreen()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  welcome_lbl;
	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->welcome_lbl = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// welcome_lbl
			// 
			this->welcome_lbl->AutoSize = true;
			this->welcome_lbl->Location = System::Drawing::Point(110, 9);
			this->welcome_lbl->Name = L"welcome_lbl";
			this->welcome_lbl->Size = System::Drawing::Size(35, 13);
			this->welcome_lbl->TabIndex = 0;
			this->welcome_lbl->Text = L"label1";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 81);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Show Items";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &UserScreen::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(190, 81);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(82, 23);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Shopping Cart";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &UserScreen::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(89, 121);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(98, 23);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Order Status";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &UserScreen::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(89, 184);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(98, 23);
			this->button4->TabIndex = 4;
			this->button4->Text = L"Disconect";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &UserScreen::button4_Click);
			// 
			// UserScreen
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->welcome_lbl);
			this->Name = L"UserScreen";
			this->Text = L"UserScreen";
			this->Load += gcnew System::EventHandler(this, &UserScreen::UserScreen_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void UserScreen_Load(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::ifstream infile;
		std::string str;
		if (type == "physical") {
			welcome_lbl->Text = "Weclome " + marshal_as<String^>(physical->getName());
			infile.open(physical->getUsername() + "_" + "Cart" + ".dat");
		}
		else if(type == "company"){
			welcome_lbl->Text = "Weclome " + marshal_as<String^>(company->getName());
			infile.open(company->getUsername() + "_" + "Cart" + ".dat");
		}

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				if (str == "compiuter") {
					getline(infile, str);
					while (str.length() > 1) {
						compiuterList.push_back(Compiuter(str));
						getline(infile, str);
					}
				}
				else if (str == "smartphone") {
					getline(infile, str);
					while (str.length() > 1) {
						smartPhoneList.push_back(SmartPhone(str));
						getline(infile, str);
					}
				}
				else if (str == "television") {
					getline(infile, str);
					while (str.length() > 1) {
						televisionList.push_back(str);
						getline(infile, str);
					}
				}
			}
		}
		infile.close();

		if (type == "physical") {
			for (int i = 0; i < compiuterList.size(); i++) {
				physical->addCompiuter(compiuterList[i]);
			}

			for (int i = 0; i < smartPhoneList.size(); i++) {
				physical->addSmartphone(smartPhoneList[i]);
			}

			for (int i = 0; i < televisionList.size(); i++) {
				physical->addTelevision(televisionList[i]);
			}
		}
		else if (type == "company") {
			for (int i = 0; i < compiuterList.size(); i++) {
				company->addCompiuter(compiuterList[i]);
			}

			for (int i = 0; i < smartPhoneList.size(); i++) {
				company->addSmartphone(smartPhoneList[i]);
			}

			for (int i = 0; i < televisionList.size(); i++) {
				company->addTelevision(televisionList[i]);
			}
		}
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (type == "physical") {
			Items^ itemsForm = gcnew Items(physical);
			this->Hide();
			itemsForm->ShowDialog();
			this->Show();
		}
		else if (type == "company") {
			Items^ itemsForm = gcnew Items(company);
			this->Hide();
			itemsForm->ShowDialog();
			this->Show();
		}
	}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	std::ifstream infile;
	std::ofstream outfile;
	std::string temp;
	std::vector<Compiuter> compiuterList;
	std::vector<SmartPhone> smartPhoneList;
	std::vector<Television> televisionList;


	if (type == "physical") {
		outfile.open(physical->getUsername() + "_Cart.dat");
		compiuterList = physical->getCompiuter();
		outfile << "compiuter" << std::endl;
		for (int i = 0; i < compiuterList.size(); i++) {
			compiuterList[i].save(outfile);
		}
		outfile << std::endl;
		outfile << "smartphone" << std::endl;
		smartPhoneList = physical->getSmartphone();
		for (int i = 0; i < smartPhoneList.size(); i++) {
			smartPhoneList[i].save(outfile);
		}
		outfile << std::endl;
		outfile << "television" << std::endl;
		televisionList = physical->getTelevision();
		for (int i = 0; i < televisionList.size(); i++) {
			televisionList[i].save(outfile);
		}
		outfile.close();
	}
	else if (type == "company") {
		outfile.open(company->getUsername() + "_Cart.dat");
		compiuterList = company->getCompiuter();
		outfile << "compiuter" << std::endl;
		for (int i = 0; i < compiuterList.size(); i++) {
			compiuterList[i].save(outfile);
		}
		outfile << std::endl;
		outfile << "smartphone" << std::endl;
		smartPhoneList = company->getSmartphone();
		for (int i = 0; i < smartPhoneList.size(); i++) {
			smartPhoneList[i].save(outfile);
		}
		outfile << std::endl;
		outfile << "television" << std::endl;
		televisionList = company->getTelevision();
		for (int i = 0; i < televisionList.size(); i++) {
			televisionList[i].save(outfile);
		}
		outfile.close();
	}
	MessageBox::Show("Save Succesful");
	this->Close();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	if (type == "physical") {
		Cart^ cartForm = gcnew Cart(physical);
		this->Hide();
		cartForm->ShowDialog();
		this->Show();
	}
	else if (type == "company") {
		Cart^ cartForm = gcnew Cart(company);
		this->Hide();
		cartForm->ShowDialog();
		this->Show();
	}
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	String^ username;

	if (type == "physical") {
		username = marshal_as<String^>(physical->getUsername());
	}
	else if (type == "company") {
		username = marshal_as<String^>(company->getUsername());
	}

	OrderUser^ orderForm = gcnew OrderUser(username);
	this->Hide();
	orderForm->ShowDialog();
	this->Show();
}
};
}
