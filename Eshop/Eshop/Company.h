#ifndef COMPANY_H
#define COMPANY_H
#include "Customer.h"

class Company : public Customer {

public:
	Company();

	Company(string managername, string managersurename, string name, double discount, int fax, string security);

	Company(string strCo);

	string getManagerName();

	void setManagerName(string managername);

	string getManagerSurename();

	void setManagerSurename(string managersurename);

	string getName();

	void setName(string name);

	double getDiscount();

	void setDiscount(double discount);

	int getFax();

	void setFax(int fax);

	string getSecurity();

	void setSecurity(string security);

	void save(ofstream &outfile);

	string toStringCo();

private:
	typedef struct manager {
		string name, surename;
	}Manager;

	Manager manager;
	string name, security;
	double discount;
	int fax;

};

#endif // !COMPANY_H

