#include "Television.h"

Television::Television() {}

Television::Television(int curve, string manufacturer, bool threed) {
	this->curve = curve;
	this->setManufacturer(manufacturer);
	this->threed = threed;
}

Television::Television(string strTv) {
	istringstream issin(strTv);
	string split[8];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->setCode(stoi(split[0]));
	this->setModel(split[1]);
	this->setManufacturer(split[2]);
	this->setDescription(split[3]);
	this->setPhoto(stoi(split[4]));
	this->setPrice(stod(split[5]));

	this->curve = stoi(split[6]);
	this->threed = stoi(split[7]);
}

int Television::getCurve() {
	return this->curve;
}

void Television::setCurve(int curve) {
	this->curve = curve;
}

bool Television::getThreed() {
	return this->threed;
}

void Television::setThreed(bool threed) {
	this->threed = threed;
}

void Television::save(ofstream &outfile) {
	outfile << this->toString() + "|";
	outfile << this->toStringTv() << endl;
}

string Television::toStringTv() {
	string str = to_string(curve) + "|" + to_string(threed);
	return str;
}
