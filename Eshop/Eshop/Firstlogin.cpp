#include "Firstlogin.h"

using namespace std;
/* Othoni pou emfanizete otan xekinaei proti fora to programma 
   einai ipeuthini na ftiaxi ton admin tin proti fora pou tha xekinisi to programma
   */
FirstLogin::FirstLogin() {
	ofstream outfile;
	ifstream infile;
	string str;

	// arxio pou periexi ti metavliti gia to an prepei na trexi auti h othoni h na xekinisi me to logi
	infile.open("first.dat");

	getline(infile, str);

	infile.close();

	if (str == "") {
		outfile.open("first.dat");
		// metavliti oti to sistima xekinise proti fora
		outfile << 1;
		// dimiourgia admin
		cout << "First Time Create Admin User" << endl;
		cout << "----------------------------" << endl;
		cout << "Give Username: ";
		cin >> username;
		cout << "Give Password: ";
		cin >> password;

		outfile.close();

		outfile.open("user.dat");

		user.setUsername(username);
		user.setPassword(password);
		user.setType("admin");
		user.save(outfile);

		outfile.close();

		admin.setUsername(user.getUsername());

		outfile.open("admin.dat", ios::app);

		while (true) {

			cout << "Give Name: " << endl;
			string name;
			cin >> name;
			admin.setName(name);

			cout << "Give Surename: " << endl;
			string surename;
			cin >> surename;
			admin.setSurename(surename);

			admin.save(outfile);
			break;
		}

		outfile.close();
	}

}