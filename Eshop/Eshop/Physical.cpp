#include "Physical.h"

Physical::Physical() {}

Physical::Physical(string name, string surename, string am, string security) {
	this->name = name;
	this->surename = surename;
	this->am = am;
	this->security = security;
}

Physical::Physical(string strPh) {
	istringstream issin(strPh);
	string split[8];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->setUsername(split[0]);
	this->setAfm(stoi(split[1]));
	this->setAddress(split[2]);
	this->setPhonenumber(stoi(split[3]));
	this->name = split[4];
	this->surename = split[5];
	this->am = split[6];
	this->security = split[7];
}

string Physical::getName() {
	return this->name;
}

void Physical::setName(string name) {
	this->name = name;
}

string Physical::getSurename() {
	return this->surename;
}

void Physical::setSurename(string surename) {
	this->surename = surename;
}

string Physical::getAm() {
	return this->am;
}

void Physical::setAm(string am) {
	this->am = am;
}

string Physical::getSecurity() {
	return this->security;
}

void Physical::setSecurity(string security) {
	this->security = security;
}

void Physical::save(ofstream &outfile) {
	outfile << this->toStringPh() << endl;
}

string Physical::toStringPh() {
	string str = this->getUsername() + "|" + to_string(this->getAfm()) + "|" + this->getAddress() + "|" + to_string(this->getPhonenumber()) + "|" + name + "|" + surename + "|" + am + "|" + security;
	return str;
}
