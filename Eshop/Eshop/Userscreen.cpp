#include "Userscreen.h"

UserScreen::UserScreen(Physical physical, Company company, string type, string username) {
	/* h othoni tou xristi ,poris na deis ola ta antikimena se diafores katigories
	   mporis na deis to kalathi agoron kai tis paragelies sou*/
	this->physical = physical;
	this->company = company;
	this->type = type;
	this->username = username;

	system("CLS");

	do {
		cout << "-------------------------------------------------" << endl;
		cout << "Welcome " + physical.getName() + company.getName() << endl;
		cout << "-----------------------" << endl;
		cout << "A) Show Items          " << endl;
		cout << "B) Show Shopping Cart  " << endl;
		cout << "C) Show Order Status   " << endl;
		cout << "D) Disconect           " << endl;
		cout << "-----------------------" << endl;
		cin >> action;

		switch (action) {
		case 'A':
			cout << "-----------" << endl;
			cout << "Show Items " << endl;
			cout << "-----------" << endl;
			ShowItems();
			break;
		case 'B':
			cout << "-------------------" << endl;
			cout << "Show Shopping Cart " << endl;
			cout << "-------------------" << endl;
			ShowCart();
			break;
		case 'C':
			cout << "------------------" << endl;
			cout << "Show Order Status " << endl;
			cout << "------------------" << endl;
			OrderStatus();
			break;
		case 'D':
			Save();
			code = 0;
			break;
		default:
			cout << "-------------------" << endl;
			cout << "Code Not Supported " << endl;
			cout << "-------------------" << endl;
		}

	} while (code == 1);
}

void UserScreen::ShowItems() {
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;
	ifstream infile;
	// fortonoume ta antikimena pou iparxoun sto katastima
	infile.open("pc.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			compiuterList.push_back(Compiuter(str));
		}
	}

	infile.close();

	infile.open("tv.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			televisionList.push_back(Television(str));
		}
	}

	infile.close();

	infile.open("phone.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			smartPhoneList.push_back(SmartPhone(str));
		}
	}

	infile.close();
	// mporoume na doume ola ta antikimena h na ta doume vasi katigorias, kataskeuasti h sindiasmo
	system("CLS");
	do {
		cout << "----------------------------" << endl;
		cout << "A) Show All Items           " << endl;
		cout << "B) Show Per Category        " << endl;
		cout << "C) Show Per Manufacturer    " << endl;
		cout << "D) Combination              " << endl;
		cout << "E) Back                     " << endl;
		cout << "----------------------------" << endl;
		cin >> action;

		switch (action) {
		case 'A':
			cout << "--------------" << endl;
			cout << "Show All Items" << endl;
			cout << "--------------" << endl;

			cout << "-----------" << endl;
			cout << "Compiuters " << endl;
			cout << "-----------" << endl;
			for (int i = 0; i < compiuterList.size(); i++) {
				cout << compiuterList[i].toString();
				cout << compiuterList[i].toStringPc() << endl;
			}

			cout << "------------" << endl;
			cout << "SmartPhones " << endl;
			cout << "------------" << endl;
			for (int i = 0; i < smartPhoneList.size(); i++) {
				cout << smartPhoneList[i].toString();
				cout << smartPhoneList[i].toStringPh() << endl;
			}

			cout << "------------" << endl;
			cout << "Televisions " << endl;
			cout << "------------" << endl;
			for (int i = 0; i < televisionList.size(); i++) {
				cout << televisionList[i].toString();
				cout << televisionList[i].toStringTv() << endl;
			}
			while (true) {
				cout << "---------------------------------------------------" << endl;
				cout << "Do You Want To Add A Product To Shopping Cart (y/n)" << endl;
				cout << "---------------------------------------------------" << endl;
				cin >> action;

				if (action == 'y') {
					AddItemToCart();
				}
				else
					break;
			}
			break;
		case 'B':
			do {
				cout << "------------------" << endl;
				cout << "Show Per Category " << endl;
				cout << "------------------" << endl;
				cout << "Choose Category   " << endl;
				cout << "------------------" << endl;
				cout << "A) Compiuters     " << endl;
				cout << "B) SmartPhones    " << endl;
				cout << "C) Televisions    " << endl;
				cout << "D) Back           " << endl;
				cout << "------------------" << endl;
				cin >> action;

				switch (action) {
				case 'A':
					cout << "-----------" << endl;
					cout << "Compiuters " << endl;
					cout << "-----------" << endl;
					for (int i = 0; i < compiuterList.size(); i++) {
						cout << "------------------------------------" << endl;
						cout << compiuterList[i].toString();
						cout << compiuterList[i].toStringPc() << endl;
						cout << "------------------------------------" << endl;
					}
					// vazoume atikimeno sto kalathi agoron
					while (true) {
						cout << "---------------------------------------------------" << endl;
						cout << "Do You Want To Add A Product To Shopping Cart (y/n)" << endl;
						cout << "---------------------------------------------------" << endl;
						cin >> action;

						if (action == 'y') {
							cout << "------------------" << endl;
							cout << "Give Product Code " << endl;
							cout << "------------------" << endl;
							cin >> id;

							for (int i = 0; i < compiuterList.size(); i++) {
								if (id == compiuterList[i].getCode()) {
									physical.addCompiuter(compiuterList[i]);
									company.addCompiuter(compiuterList[i]);
									temp = 1;
									break;
								}
							}
							if (temp == 0) {
								cout << "--------------------" << endl;
								cout << "No Such Item Exists " << endl;
								cout << "--------------------" << endl;
							}
						}
						else
							break;
					}
					break;
				case 'B':
					cout << "------------" << endl;
					cout << "SmartPhones " << endl;
					cout << "------------" << endl;
					for (int i = 0; i < smartPhoneList.size(); i++) {
						cout << "---------------------------------" << endl;
						cout << smartPhoneList[i].toString();
						cout << smartPhoneList[i].toStringPh() << endl;
						cout << "---------------------------------" << endl;
					}
					while (true) {
						cout << "---------------------------------------------------" << endl;
						cout << "Do You Want To Add A Product To Shopping Cart (y/n)" << endl;
						cout << "---------------------------------------------------" << endl;
						cin >> action;

						if (action == 'y') {
							cout << "------------------" << endl;
							cout << "Give Product Code " << endl;
							cout << "------------------" << endl;
							cin >> id;

							for (int i = 0; i < smartPhoneList.size(); i++) {
								if (id == smartPhoneList[i].getCode()) {
									physical.addSmartphone(smartPhoneList[i]);
									company.addSmartphone(smartPhoneList[i]);
									temp = 1;
									break;
								}
							}
							if (temp == 0) {
								cout << "--------------------" << endl;
								cout << "No Such Item Exists " << endl;
								cout << "--------------------" << endl;
							}
						}
						else
							break;
					}
					break;
				case 'C':
					cout << "------------" << endl;
					cout << "Televisions " << endl;
					cout << "------------" << endl;
					for (int i = 0; i < televisionList.size(); i++) {
						cout << "--------------------------------" << endl;
						cout << televisionList[i].toString();
						cout << televisionList[i].toStringTv() << endl;
						cout << "--------------------------------" << endl;
					}
					while (true) {
						cout << "---------------------------------------------------" << endl;
						cout << "Do You Want To Add A Product To Shopping Cart (y/n)" << endl;
						cout << "---------------------------------------------------" << endl;
						cin >> action;

						if (action == 'y') {
							cout << "------------------" << endl;
							cout << "Give Product Code " << endl;
							cout << "------------------" << endl;
							cin >> id;
							for (int i = 0; i < televisionList.size(); i++) {
								if (id == televisionList[i].getCode()) {
									physical.addTelevision(televisionList[i]);
									company.addTelevision(televisionList[i]);
									temp = 1;
									break;
								}
							}
							if (temp == 0) {
								cout << "--------------------" << endl;
								cout << "No Such Item Exists " << endl;
								cout << "--------------------" << endl;
							}
						}
						else
							break;
					}
					break;
				case 'D':
					code = 0;
					break;
				default:
					cout << "------------------" << endl;
					cout << "Code Not Supported" << endl;
					cout << "------------------" << endl;
				}
			} while (code == 1);
			code = 1;
			break;
		case 'C':
			cout << "----------------------" << endl;
			cout << "Show Per Manufacturer " << endl;
			cout << "Give Manufacturer Name" << endl;
			cout << "----------------------" << endl;
			cin >> manufacturer;

			for (int i = 0; i < compiuterList.size(); i++) {
				if (manufacturer == compiuterList[i].getManufacturer()) {
					cout << "----------" << endl;
					cout << "Compiuters" << endl;
					cout << "----------" << endl;
					cout << compiuterList[i].toString();
					cout << compiuterList[i].toStringPc() << endl;
					temp = 1;
				}
			}

			for (int i = 0; i < smartPhoneList.size(); i++) {
				if (manufacturer == smartPhoneList[i].getManufacturer()) {
					cout << "-----------" << endl;
					cout << "SmartPhones" << endl;
					cout << "-----------" << endl;
					cout << smartPhoneList[i].toString();
					cout << smartPhoneList[i].toStringPh() << endl;
					temp = 1;
				}
			}

			for (int i = 0; i < televisionList.size(); i++) {
				if (manufacturer == televisionList[i].getManufacturer()) {
					cout << "-----------" << endl;
					cout << "Televisions" << endl;
					cout << "-----------" << endl;
					cout << televisionList[i].toString();
					cout << televisionList[i].toStringTv() << endl;
					temp = 1;
				}
			}
			if (temp == 0) {
				cout << "-------------------------" << endl;
				cout << "Manufacturer Doesnt Exist" << endl;
				cout << "-------------------------" << endl;
			}
			break;
		case 'D':
			do {
				cout << "------------------------------" << endl;
				cout << "Combination                   " << endl;
				cout << "Choose Product Category       " << endl;
				cout << "A) Compiuters                 " << endl;
				cout << "B) SmartPhones                " << endl;
				cout << "C) Television                 " << endl;
				cout << "D) Back                       " << endl;
				cout << "------------------------------" << endl;
				cin >> action;

				switch (action) {
				case 'A':
					cout << "----------------------" << endl;
					cout << "Compiuters            " << endl;
					cout << "Give Manufacturer Name" << endl;
					cout << "----------------------" << endl;
					cin >> manufacturer;

					for (int i = 0; i < compiuterList.size(); i++) {
						if (manufacturer == compiuterList[i].getManufacturer()) {
							cout << "----------" << endl;
							cout << "Compiuters" << endl;
							cout << "----------" << endl;
							cout << compiuterList[i].toString();
							cout << compiuterList[i].toStringPc() << endl;
							temp = 1;
						}
					}

					if (temp == 0) {
						cout << "-------------------------" << endl;
						cout << "Manufacturer Doesnt Exist" << endl;
						cout << "-------------------------" << endl;
					}
					break;
				case 'B':
					cout << "-----------------------" << endl;
					cout << "SmartPhones            " << endl;
					cout << "Give Manufacturer Name " << endl;
					cout << "-----------------------" << endl;
					cin >> manufacturer;

					for (int i = 0; i < smartPhoneList.size(); i++) {
						if (manufacturer == smartPhoneList[i].getManufacturer()) {
							cout << "-----------" << endl;
							cout << "SmartPhones" << endl;
							cout << "-----------" << endl;
							cout << smartPhoneList[i].toString();
							cout << smartPhoneList[i].toStringPh() << endl;
							temp = 1;
						}
					}
					if (temp == 0) {
						cout << "-------------------------" << endl;
						cout << "Manufacturer Doesnt Exist" << endl;
						cout << "-------------------------" << endl;
					}
					break;
				case 'C':
					cout << "-----------------------" << endl;
					cout << "Television             " << endl;
					cout << "Give Manufacturer Name " << endl;
					cout << "-----------------------" << endl;
					cin >> manufacturer;

					for (int i = 0; i < televisionList.size(); i++) {
						if (manufacturer == televisionList[i].getManufacturer()) {
							cout << "-----------" << endl;
							cout << "Televisions" << endl;
							cout << "-----------" << endl;
							cout << televisionList[i].toString();
							cout << televisionList[i].toStringTv() << endl;
							temp = 1;
						}
					}
					if (temp == 0) {
						cout << "-------------------------" << endl;
						cout << "Manufacturer Doesnt Exist" << endl;
						cout << "-------------------------" << endl;
					}
					break;
				case 'D':
					code = 0;
					break;
				default:
					cout << "------------------" << endl;
					cout << "Code Not Supported" << endl;
					cout << "------------------" << endl;
				}
			} while (code == 1);
			code = 1;
			break;
		case 'E':
			code = 0;
			break;
		default:
			cout << "------------------" << endl;
			cout << "Code Not Supported" << endl;
			cout << "------------------" << endl;
		}
	} while (code == 1);
	code = 1;
}
void UserScreen::AddItemToCart() {
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;
	ifstream infile;

	// vazoume antikimeno sto kalathi agoron
	infile.open("pc.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			compiuterList.push_back(Compiuter(str));
		}
	}

	infile.close();

	infile.open("tv.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			televisionList.push_back(Television(str));
		}
	}

	infile.close();

	infile.open("phone.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			smartPhoneList.push_back(SmartPhone(str));
		}
	}

	infile.close();

	cout << "Choose Category" << endl;
	cout << "A) Compiuters  " << endl;
	cout << "B) SmartPhones " << endl;
	cout << "C) Televisions " << endl;
	cin >> action;

	switch (action) {
	case 'A':
		cout << "Compiuters   " << endl;
		cout << "Give Product Code " << endl;
		cin >> id;

		for (int i = 0; i < compiuterList.size(); i++) {
			if (id == compiuterList[i].getCode()) {
				physical.addCompiuter(compiuterList[i]);
				company.addCompiuter(compiuterList[i]);
				temp = 1;
				break;
			}
		}
		if (temp == 0) {
			cout << "No Such Item Exists " << endl;
		}
		break;
	case 'B':
		cout << "SmartPhones     " << endl;
		cout << "Give Product Code " << endl;
		cin >> id;

		for (int i = 0; i < smartPhoneList.size(); i++) {
			if (id == smartPhoneList[i].getCode()) {
				physical.addSmartphone(smartPhoneList[i]);
				company.addSmartphone(smartPhoneList[i]);
				temp = 1;
				break;
			}
		}
		if (temp == 0) {
			cout << "No Such Item Exists " << endl;
		}
		break;
	case 'C':
		cout << "Televisions     " << endl;
		cout << "Give Product Code " << endl;
		cin >> id;
		for (int i = 0; i < televisionList.size(); i++) {
			if (id == televisionList[i].getCode()) {
				physical.addTelevision(televisionList[i]);
				company.addTelevision(televisionList[i]);
				temp = 1;
				break;
			}
		}
		if (temp == 0) {
			cout << "No Such Item Exists " << endl;
		}
		break;
	default:
		cout << "Code Not Supported " << endl;
	}
}
void UserScreen::ShowCart() {
	// vlepoume to kalathi agoron
	do {
		if (type == "physical") {
			compiuterList = physical.getCompiuter();
			smartPhoneList = physical.getSmartphone();
			televisionList = physical.getTelevision();

			cout << "-----------" << endl;
			cout << "Compiuters " << endl;
			cout << "-----------" << endl;
			for (int i = 0; i < compiuterList.size(); i++) {
				cout << "---------------------------------" << endl;
				cout << compiuterList[i].toString();
				cout << compiuterList[i].toStringPc() << endl;
				cout << "---------------------------------" << endl;
			}
			cout << "------------" << endl;
			cout << "SmartPhones " << endl;
			cout << "------------" << endl;
			for (int i = 0; i < smartPhoneList.size(); i++) {
				cout << "---------------------------------" << endl;
				cout << smartPhoneList[i].toString();
				cout << smartPhoneList[i].toStringPh() << endl;
				cout << "---------------------------------" << endl;
			}

			cout << "------------" << endl;
			cout << "Televisions " << endl;
			cout << "------------" << endl;
			for (int i = 0; i < televisionList.size(); i++) {
				cout << "---------------------------------" << endl;
				cout << televisionList[i].toString();
				cout << televisionList[i].toStringTv() << endl;
				cout << "---------------------------------" << endl;
			}
		}
		else if (type == "company") {
			compiuterList = company.getCompiuter();
			smartPhoneList = company.getSmartphone();
			televisionList = company.getTelevision();

			cout << "-----------" << endl;
			cout << "Compiuters " << endl;
			cout << "-----------" << endl;
			for (int i = 0; i < compiuterList.size(); i++) {
				cout << "---------------------------------" << endl;
				cout << compiuterList[i].toString();
				cout << compiuterList[i].toStringPc() << endl;
				cout << "---------------------------------" << endl;
			}

			cout << "------------" << endl;
			cout << "SmartPhones " << endl;
			cout << "------------" << endl;
			for (int i = 0; i < smartPhoneList.size(); i++) {
				cout << "---------------------------------" << endl;
				cout << smartPhoneList[i].toString();
				cout << smartPhoneList[i].toStringPh() << endl;
				cout << "---------------------------------" << endl;
			}

			cout << "------------" << endl;
			cout << "Televisions " << endl;
			cout << "------------" << endl;
			for (int i = 0; i < televisionList.size(); i++) {
				cout << "---------------------------------" << endl;
				cout << televisionList[i].toString();
				cout << televisionList[i].toStringTv() << endl;
				cout << "---------------------------------" << endl;
			}
		}
		// mporoume na paragiloume ta antikimena h na bgaloume antikimena apo to kalathi
		cout << "------------------------" << endl;
		cout << "A) Remove Item From Cart" << endl;
		cout << "B) Order Items          " << endl;
		cout << "C) Back                 " << endl;
		cout << "------------------------" << endl;
		cin >> action;
		if (action == 'A') {
			RemoveItemFromCart();
		}
		else if (action == 'B') {
			OrderItems();
		}
		else if (action == 'C')
			break;
		else {
			cout << "------------------" << endl;
			cout << "Code Not Supported" << endl;
			cout << "------------------" << endl;
		}
	} while (true);
}
void UserScreen::OrderStatus() {
	ifstream infile;
	ofstream outfile;
	vector <Order> orderList;
	int actionId;
	string temp;

	cout << "--------------" << endl;
	cout << "Order Status  " << endl;
	cout << "--------------" << endl;

	infile.open(username + "_" + "Order.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			orderList.push_back(Order(str));
		}
	}
	infile.close();

	for (int i = 0; i < orderList.size(); i++) {
		actionId = orderList[i].getStatus();
		switch (actionId)
		{
		case 1:

			cout << orderList[i].getId() << " " << orderList[i].getCost() << " EMPTY" << endl;
			cout << "---------------------------------------------------------------" << endl;
			break;
		case 2:
			cout << orderList[i].getId() << " " << orderList[i].getCost() << " PENDING" << endl;
			cout << "-----------------------------------------------------------------" << endl;
			break;
		case 3:
			cout << orderList[i].getId() << " " << orderList[i].getCost() << " SENDING" << endl;
			cout << "-----------------------------------------------------------------" << endl;
			break;
		case 4:
			cout << orderList[i].getId() << " " << orderList[i].getCost() << " COMPLETE" << endl;
			cout << "------------------------------------------------------------------" << endl;
			break;
		case 5:
			cout << orderList[i].getId() << " " << orderList[i].getCost() << " CANCELED" << endl;
			cout << "------------------------------------------------------------------" << endl;
			break;
		default:
			break;
		}

		cout << "---------------------------------" << endl;
		cout << "Do You Want To Cancel Order (y/n)" << endl;
		cout << "---------------------------------" << endl;
		cin >> action;

		if (action == 'y') {
			cout << "-------------" << endl;
			cout << "Give Order Id" << endl;
			cout << "-------------" << endl;
			cin >> id;

			for (int i = 0; i < orderList.size(); i++) {
				if (id == orderList[i].getId()) {
					temp = orderList[i].toString();
				}
			}

			cout << orderList[i].getId() << " " << orderList[i].getCost() << " CANCELED" << endl;
			cout << "------------------------------------------------------------------" << endl;


			infile.open(username + "_" + "Order.dat");
			outfile.open("temp.dat");

			while (!infile.eof()) {
				getline(infile, str);
				if (str == temp) {
				}
				else
					outfile << str << endl;
			}

			infile.close();
			outfile.close();

			temp = username + "_" + "Order.dat";

			remove(temp.c_str());
			rename("temp.dat", temp.c_str());
		}
	}
}
void UserScreen::RemoveItemFromCart() {
	string category;

	cout << "Choose Category (pc/phone/tv)" << endl;
	cin >> category;
	cout << "Give Item Id" << endl;
	cin >> id;

	if (type == "physical") {
		if (category == "pc") {
			compiuterList = physical.getCompiuter();

			for (int i = 0; i < compiuterList.size(); i++) {
				if (id == compiuterList[i].getCode()) {
					compiuterList.erase(compiuterList.begin() + i);
				}
			}
			physical.setCompiuterVector(compiuterList);
		}
		else if (category == "phone") {
			smartPhoneList = physical.getSmartphone();

			for (int i = 0; i < smartPhoneList.size(); i++) {
				if (id == smartPhoneList[i].getCode()) {
					smartPhoneList.erase(smartPhoneList.begin() + i);
				}
			}
			physical.setSmartPhoneVector(smartPhoneList);
		}
		else if (category == "tv") {
			televisionList = physical.getTelevision();

			for (int i = 0; i < televisionList.size(); i++) {
				if (id == televisionList[i].getCode()) {
					televisionList.erase(televisionList.begin() + i);
				}
			}
			physical.setTelevisionVector(televisionList);
		}
	}
	else if (type == "company") {
		if (category == "pc") {
			compiuterList = company.getCompiuter();

			for (int i = 0; i < compiuterList.size(); i++) {
				if (id == compiuterList[i].getCode()) {
					compiuterList.erase(compiuterList.begin() + i);
				}
			}
			company.setCompiuterVector(compiuterList);
		}
		else if (category == "phone") {
			smartPhoneList = company.getSmartphone();

			for (int i = 0; i < smartPhoneList.size(); i++) {
				if (id == smartPhoneList[i].getCode()) {
					smartPhoneList.erase(smartPhoneList.begin() + i);
				}
			}
			company.setSmartPhoneVector(smartPhoneList);
		}
		else if (category == "tv") {
			televisionList = physical.getTelevision();

			for (int i = 0; i < televisionList.size(); i++) {
				if (id == televisionList[i].getCode()) {
					televisionList.erase(televisionList.begin() + i);
				}
			}
			company.setTelevisionVector(televisionList);
		}
	}
}
void UserScreen::OrderItems() {
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;
	ofstream outfile;
	ifstream infile;
	Order order;
	int counter = 0;

	cout << "--------" << endl;
	cout << "Ordering" << endl;
	cout << "--------" << endl;

	outfile.open(username + "_" + "Order.dat", ios::app);
	infile.open(username + "_" + "Order.dat");
	getline(infile, str);

	if (type == "physical") {
		compiuterList = physical.getCompiuter();
		smartPhoneList = physical.getSmartphone();
		televisionList = physical.getTelevision();

		for (int i = 0; i < compiuterList.size(); i++) {
			order.Calculation(compiuterList[i].getPrice());
		}
		for (int i = 0; i < smartPhoneList.size(); i++) {
			order.Calculation(smartPhoneList[i].getPrice());
		}
		for (int i = 0; i < televisionList.size(); i++) {
			order.Calculation(televisionList[i].getPrice());
		}

		if (str == "") {
			order.setId(1);
		}
		else {
			while (str != "") {
				getline(infile, str);
				counter++;
			}
			order.setId(counter);
		}
		infile.close();

		order.setStatus(2);

		for (int i = 0; i < compiuterList.size(); i++) {
			compiuterList.erase(compiuterList.begin() + i, compiuterList.end());
		}
		for (int i = 0; i < smartPhoneList.size(); i++) {
			smartPhoneList.erase(smartPhoneList.begin() + i, smartPhoneList.end());
		}
		for (int i = 0; i < televisionList.size(); i++) {
			televisionList.erase(televisionList.begin() + i, televisionList.end());
		}

		physical.setCompiuterVector(compiuterList);
		physical.setSmartPhoneVector(smartPhoneList);
		physical.setTelevisionVector(televisionList);

		order.save(outfile);
	}
	else if (type == "company") {
		compiuterList = company.getCompiuter();
		smartPhoneList = company.getSmartphone();
		televisionList = company.getTelevision();

		for (int i = 0; i < compiuterList.size(); i++) {
			order.Calculation(compiuterList[i].getPrice()*(company.getDiscount() / 100));
		}
		for (int i = 0; i < smartPhoneList.size(); i++) {
			order.Calculation(smartPhoneList[i].getPrice()*(company.getDiscount() / 100));
		}
		for (int i = 0; i < televisionList.size(); i++) {
			order.Calculation(televisionList[i].getPrice()*(company.getDiscount() / 100));
		}

		if (str == "") {
			order.setId(1);
		}
		else {
			while (str != "") {
				getline(infile, str);
				counter++;
			}
			order.setId(counter);
		}
		infile.close();

		order.setStatus(2);

		for (int i = 0; i < compiuterList.size(); i++) {
			compiuterList.erase(compiuterList.begin() + i, compiuterList.end());
		}
		for (int i = 0; i < smartPhoneList.size(); i++) {
			smartPhoneList.erase(smartPhoneList.begin() + i, smartPhoneList.end());
		}
		for (int i = 0; i < televisionList.size(); i++) {
			televisionList.erase(televisionList.begin() + i, televisionList.end());
		}

		company.setCompiuterVector(compiuterList);
		company.setSmartPhoneVector(smartPhoneList);
		company.setTelevisionVector(televisionList);

		order.save(outfile);
	}
	outfile.close();
}
void UserScreen::Save() {
	ifstream infile;
	ofstream outfile;
	string temp;
	// apothikeui to kalathi agoron otan kanoume disconect
	cout << "Saving....." << endl;

	outfile.open(username + "_Cart.dat");

	if (type == "physical") {
		compiuterList = physical.getCompiuter();
		outfile << "compiuter" << endl;
		for (int i = 0; i < compiuterList.size(); i++) {
			compiuterList[i].save(outfile);
		}
		outfile << endl;
		outfile << "smartphone" << endl;
		smartPhoneList = physical.getSmartphone();
		for (int i = 0; i < smartPhoneList.size(); i++) {
			smartPhoneList[i].save(outfile);
		}
		outfile << endl;
		outfile << "television" << endl;
		televisionList = physical.getTelevision();
		for (int i = 0; i < televisionList.size(); i++) {
			televisionList[i].save(outfile);
		}
	}
	else if (type == "company") {
		compiuterList = company.getCompiuter();
		outfile << "compiuter" << endl;
		for (int i = 0; i < compiuterList.size(); i++) {
			compiuterList[i].save(outfile);
		}
		outfile << endl;
		outfile << "smartphone" << endl;
		smartPhoneList = company.getSmartphone();
		for (int i = 0; i < smartPhoneList.size(); i++) {
			smartPhoneList[i].save(outfile);
		}
		outfile << endl;
		outfile << "television" << endl;
		televisionList = company.getTelevision();
		for (int i = 0; i < televisionList.size(); i++) {
			televisionList[i].save(outfile);
		}
	}
	outfile.close();
}
