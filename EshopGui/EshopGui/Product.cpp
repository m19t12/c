#include "Product.h"

using namespace std;

Product::Product() {}

Product::Product(int code, string model, string manufacturer, string description, bool photo, double price) {
	this->code = code;
	this->model = model;
	this->manufacturer = manufacturer;
	this->description = description;
	this->photo = photo;
	this->price = price;
}

int Product::getCode() {
	return this->code;
}

void Product::setCode(int code) {
	this->code = code;
}

string Product::getModel() {
	return this->model;
}

void Product::setModel(string model) {
	this->model = model;
}

string Product::getManufacturer() {
	return this->manufacturer;
}

void Product::setManufacturer(string manufacturer) {
	this->manufacturer = manufacturer;
}

string Product::getDescription() {
	return this->description;
}

void Product::setDescription(string description) {
	this->description = description;
}

bool Product::getPhoto() {
	return this->photo;
}

void Product::setPhoto(bool photo) {
	this->photo = photo;
}

double Product::getPrice() {
	return this->price;
}

void Product::setPrice(double price) {
	this->price = price;
}

string Product::toString() {
	string str = to_string(code) + "|" + model + "|" + manufacturer + "|" + description + "|" + to_string(photo) + "|" + to_string(price);
	return str;
}
