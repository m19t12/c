#pragma once
#include "Order.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for OrderUser
	/// </summary>
	public ref class OrderUser : public System::Windows::Forms::Form
	{
	public:
		String^ username;
	public:
		OrderUser(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		OrderUser(String^ username)
		{
			InitializeComponent();
			this->username = username;
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~OrderUser()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^  order_lbx;
	protected:
	private: System::Windows::Forms::Label^  order_lbl;
	private: System::Windows::Forms::Button^  bck_btn;
	private: System::Windows::Forms::Button^  upd_btn;

	protected:









	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->order_lbx = (gcnew System::Windows::Forms::ListBox());
			this->order_lbl = (gcnew System::Windows::Forms::Label());
			this->bck_btn = (gcnew System::Windows::Forms::Button());
			this->upd_btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// order_lbx
			// 
			this->order_lbx->FormattingEnabled = true;
			this->order_lbx->Location = System::Drawing::Point(12, 52);
			this->order_lbx->Name = L"order_lbx";
			this->order_lbx->Size = System::Drawing::Size(260, 95);
			this->order_lbx->TabIndex = 0;
			this->order_lbx->SelectedIndexChanged += gcnew System::EventHandler(this, &OrderUser::order_lbx_SelectedIndexChanged);
			// 
			// order_lbl
			// 
			this->order_lbl->AutoSize = true;
			this->order_lbl->Location = System::Drawing::Point(102, 18);
			this->order_lbl->Name = L"order_lbl";
			this->order_lbl->Size = System::Drawing::Size(35, 13);
			this->order_lbl->TabIndex = 1;
			this->order_lbl->Text = L"label1";
			// 
			// bck_btn
			// 
			this->bck_btn->Location = System::Drawing::Point(197, 200);
			this->bck_btn->Name = L"bck_btn";
			this->bck_btn->Size = System::Drawing::Size(75, 23);
			this->bck_btn->TabIndex = 2;
			this->bck_btn->Text = L"Back";
			this->bck_btn->UseVisualStyleBackColor = true;
			this->bck_btn->Click += gcnew System::EventHandler(this, &OrderUser::bck_btn_Click);
			// 
			// upd_btn
			// 
			this->upd_btn->Location = System::Drawing::Point(12, 199);
			this->upd_btn->Name = L"upd_btn";
			this->upd_btn->Size = System::Drawing::Size(75, 23);
			this->upd_btn->TabIndex = 3;
			this->upd_btn->Text = L"Cancel";
			this->upd_btn->UseVisualStyleBackColor = true;
			this->upd_btn->Click += gcnew System::EventHandler(this, &OrderUser::upd_btn_Click);
			// 
			// OrderUser
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->upd_btn);
			this->Controls->Add(this->bck_btn);
			this->Controls->Add(this->order_lbl);
			this->Controls->Add(this->order_lbx);
			this->Name = L"OrderUser";
			this->Text = L"OrderUser";
			this->Load += gcnew System::EventHandler(this, &OrderUser::OrderUser_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void OrderUser_Load(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::vector<Order> orderList;
		String^ temp = username;
		std::string str;

		infile.open(marshal_as<std::string>(temp) + "_" + "Order.dat");
		if (infile.is_open()) {
			order_lbl->Text = "Users :" + temp + " Orders";

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					orderList.push_back(Order(str));
				}
			}
			infile.close();

			for (int i = 0; i < orderList.size(); i++) {
				order_lbx->Items->Add(marshal_as<String^>(orderList[i].toString()));
			}
		}
		else {
			MessageBox::Show("No Order Exist");
			this->Close();
		}
	}
	private: System::Void bck_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void upd_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::ofstream outfile;
		std::vector <Order> orderList;
		std::string str, help;
		String^ temp = username;

		
		infile.open(marshal_as<std::string>(temp) + "_" + "Order.dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (marshal_as<String^>(str) == order_lbx->SelectedItem->ToString()) {
			}
			else
				outfile << str << std::endl;
		}

		infile.close();
		outfile.close();

		help = marshal_as<std::string>(temp) + "_" + "Order.dat";

		remove(help.c_str());
		rename("temp.dat", help.c_str());

		order_lbx->Items->Remove(order_lbx->SelectedItem->ToString());

	}
	private: System::Void order_lbx_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	}
	};
}
