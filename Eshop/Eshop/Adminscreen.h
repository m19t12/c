#ifndef ADMINSCREEN_H
#define ADMINSCREEN_H
#include <iostream>
#include <vector>
#include "Admin.h"
#include "Physical.h"
#include "Company.h"
#include "Order.h"

class AdminScreen {

public:
	AdminScreen(Admin admin);

	void ShowOrders();
	void CreateUser();
	void Catalog();
	void AddItems();
	void UpdateItems();
	void RemoveItems();
	void ChangeStatus();
	void ShowCustomers();
	void Tasks();

private:
	char action;
	bool read = true;
	int code = 1;
	string type, str, temp;
	User user;
	Physical physical;
	Company company;
	Admin admin;
	vector<User> users;
};

#endif // !ADMINSCREEN_H
