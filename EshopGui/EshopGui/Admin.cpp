#include "Admin.h"

using namespace std;

Admin::Admin() {}

Admin::Admin(string name, string surename) {
	this->name = name;
	this->surename = surename;
}

Admin::Admin(string strAd) {
	istringstream issin(strAd);
	string split[3];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}

	this->setUsername(split[0]);
	this->name = split[1];
	this->surename = split[2];
}

void Admin::save(ofstream &outfile) {
	outfile << this->toStringAd() << endl;
}

string Admin::toStringAd() {
	string str = this->getUsername() + "|" + name + "|" + surename;
	return str;
}

string Admin::getName() {
	return this->name;
}

void Admin::setName(string name) {
	this->name = name;
}

string Admin::getSurename() {
	return this->surename;
}

void Admin::setSurename(string surename) {
	this->surename = surename;
}