#ifndef ORDER_H
#define ORDER_H
#include "Shoppingcart.h"

class Order : public ShoppingCart {

public:

	Order();

	Order(int id, double cost, int status);

	Order(string strOr);

	void Calculation(double cost);

	int getId();

	void setId(int id);

	double getCost();

	void setCost(double cost);
	
	void setStatus(int status);

	int getStatus();

	void save(ofstream &outfile);

	string toString();

private:
	int id, status;
	double cost;
	
};

#endif // !ORDER_H

