#ifndef PRODUCT_H
#define PRODUCT_H
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

class Product {
public:
	Product();

	Product(int code, std::string model, std::string manufacturer, std::string description, bool photo, double price);

	int getCode();

	void setCode(int code);

	std::string getModel();

	void setModel(std::string model);

	std::string getManufacturer();

	void setManufacturer(std::string manufacturer);

	std::string getDescription();

	void setDescription(std::string description);

	bool getPhoto();

	void setPhoto(bool photo);

	double getPrice();

	void setPrice(double price);

	std::string toString();

private:
	int code;
	std::string model, manufacturer, description;
	bool photo;
	double price;
};

#endif
