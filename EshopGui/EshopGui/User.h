#ifndef USER_H
#define USER_H
#include <fstream>
#include <sstream>
#include <string>

class User {
public:

	User();

	User(std::string username, std::string password, std::string type);

	User(std::string strUs);

	std::string getUsername();

	void setUsername(std::string username);

	std::string getPassword();

	void setPassword(std::string password);

	std::string getType();

	void setType(std::string type);

	void save(std::ofstream &outfile);

	std::string toStringUs();

private:
	std::string username, password, type;
};

#endif
