#pragma once
#include "User.h"
#include "Admin.h"
#include "LoginScreen.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for FirstLogin
	/// </summary>
	public ref class FirstLogin : public System::Windows::Forms::Form
	{
	public:
		FirstLogin(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FirstLogin()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  usernm_txt;
	private: System::Windows::Forms::TextBox^  pass_txt;
	private: System::Windows::Forms::TextBox^  name_txt;
	private: System::Windows::Forms::TextBox^  sure_txt;
	private: System::Windows::Forms::Button^  create_btn;
	private: System::Windows::Forms::Button^  back_btn;






	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->usernm_txt = (gcnew System::Windows::Forms::TextBox());
			this->pass_txt = (gcnew System::Windows::Forms::TextBox());
			this->name_txt = (gcnew System::Windows::Forms::TextBox());
			this->sure_txt = (gcnew System::Windows::Forms::TextBox());
			this->create_btn = (gcnew System::Windows::Forms::Button());
			this->back_btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(39, 40);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(55, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Username";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(39, 69);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(53, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Password";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(39, 100);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Name";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(39, 131);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(55, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Surename";
			// 
			// usernm_txt
			// 
			this->usernm_txt->Location = System::Drawing::Point(145, 37);
			this->usernm_txt->Name = L"usernm_txt";
			this->usernm_txt->Size = System::Drawing::Size(100, 20);
			this->usernm_txt->TabIndex = 4;
			// 
			// pass_txt
			// 
			this->pass_txt->Location = System::Drawing::Point(145, 69);
			this->pass_txt->Name = L"pass_txt";
			this->pass_txt->PasswordChar = '*';
			this->pass_txt->Size = System::Drawing::Size(100, 20);
			this->pass_txt->TabIndex = 5;
			// 
			// name_txt
			// 
			this->name_txt->Location = System::Drawing::Point(145, 100);
			this->name_txt->Name = L"name_txt";
			this->name_txt->Size = System::Drawing::Size(100, 20);
			this->name_txt->TabIndex = 6;
			// 
			// sure_txt
			// 
			this->sure_txt->Location = System::Drawing::Point(145, 131);
			this->sure_txt->Name = L"sure_txt";
			this->sure_txt->Size = System::Drawing::Size(100, 20);
			this->sure_txt->TabIndex = 7;
			// 
			// create_btn
			// 
			this->create_btn->Location = System::Drawing::Point(42, 193);
			this->create_btn->Name = L"create_btn";
			this->create_btn->Size = System::Drawing::Size(75, 23);
			this->create_btn->TabIndex = 8;
			this->create_btn->Text = L"Create";
			this->create_btn->UseVisualStyleBackColor = true;
			this->create_btn->Click += gcnew System::EventHandler(this, &FirstLogin::create_btn_Click);
			// 
			// back_btn
			// 
			this->back_btn->Location = System::Drawing::Point(170, 193);
			this->back_btn->Name = L"back_btn";
			this->back_btn->Size = System::Drawing::Size(75, 23);
			this->back_btn->TabIndex = 9;
			this->back_btn->Text = L"Back";
			this->back_btn->UseVisualStyleBackColor = true;
			this->back_btn->Click += gcnew System::EventHandler(this, &FirstLogin::button2_Click);
			// 
			// FirstLogin
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->back_btn);
			this->Controls->Add(this->create_btn);
			this->Controls->Add(this->sure_txt);
			this->Controls->Add(this->name_txt);
			this->Controls->Add(this->pass_txt);
			this->Controls->Add(this->usernm_txt);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"FirstLogin";
			this->Text = L"FirstLogin";
			this->Load += gcnew System::EventHandler(this, &FirstLogin::FirstLogin_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void FirstLogin_Load(System::Object^  sender, System::EventArgs^  e) {
		std::ofstream outfile;
		std::ifstream infile;
		std::string str;

		infile.open("first.dat");

		if (infile.is_open()) {
			getline(infile, str);
			infile.close();
		}
		else {
			outfile.open("first.dat");
			outfile.close();
		}

		if (str != "") {
			LoginScreen^ loginForm = gcnew LoginScreen();
			this->Hide();
			loginForm->ShowDialog();
			this->Close();
		}
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}

	private: System::Void create_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ofstream outfile;
		std::ifstream infile;
		std::string str;
		User user;
		Admin admin;

		outfile.open("first.dat");
		outfile << 1;

		outfile.close();

		user.setUsername(marshal_as<std::string>(usernm_txt->Text));
		user.setPassword(marshal_as<std::string>(pass_txt->Text));
		user.setType("admin");

		outfile.open("user.dat", std::ios::app);

		user.save(outfile);

		outfile.close();

		admin.setUsername(user.getUsername());

		outfile.open("admin.dat", std::ios::app);

		while (true) {
			admin.setName(marshal_as<std::string>(name_txt->Text));
			admin.setSurename(marshal_as<std::string>(sure_txt->Text));
			admin.save(outfile);
			break;
		}
		outfile.close();

		LoginScreen^ loginForm = gcnew LoginScreen();
		this->Hide();
		loginForm->ShowDialog();
		this->Close();
	}
	};
}
