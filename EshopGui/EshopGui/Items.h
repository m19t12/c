#pragma once
#include "Company.h"
#include "Smartphone.h"
#include "Television.h"
#include "Physical.h"
#include "Company.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for Items
	/// </summary>
	public ref class Items : public System::Windows::Forms::Form
	{
	public:
		Physical *physical;
		Company *company;
		String^ type;
	public:
		Items(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		Items(Physical *physical)
		{
			InitializeComponent();
			this->physical = physical;
			type = "physical";
			//
			//TODO: Add the constructor code here
			//
		}

		Items(Company *company)
		{
			InitializeComponent();
			this->company = company;
			type = "company";
			//
			//TODO: Add the constructor code here
			//
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Items()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ComboBox^  cat_cmbx;
	private: System::Windows::Forms::TextBox^  manu_txt;
	private: System::Windows::Forms::ListBox^  item_lstbx;

	private: System::Windows::Forms::Label^  type_lbl;
	private: System::Windows::Forms::Button^  fnd_btn;
	private: System::Windows::Forms::Button^  back_btn;
	private: System::Windows::Forms::Button^  add_btn;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->cat_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->manu_txt = (gcnew System::Windows::Forms::TextBox());
			this->item_lstbx = (gcnew System::Windows::Forms::ListBox());
			this->type_lbl = (gcnew System::Windows::Forms::Label());
			this->fnd_btn = (gcnew System::Windows::Forms::Button());
			this->back_btn = (gcnew System::Windows::Forms::Button());
			this->add_btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 30);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(49, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Category";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 68);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(70, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Manufacturer";
			// 
			// cat_cmbx
			// 
			this->cat_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->cat_cmbx->FormattingEnabled = true;
			this->cat_cmbx->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"Compiuters", L"SmartPhones", L"Televisions", L"All" });
			this->cat_cmbx->Location = System::Drawing::Point(127, 30);
			this->cat_cmbx->Name = L"cat_cmbx";
			this->cat_cmbx->Size = System::Drawing::Size(121, 21);
			this->cat_cmbx->TabIndex = 2;
			this->cat_cmbx->SelectedIndexChanged += gcnew System::EventHandler(this, &Items::cat_cmbx_SelectedIndexChanged);
			// 
			// manu_txt
			// 
			this->manu_txt->Location = System::Drawing::Point(127, 60);
			this->manu_txt->Name = L"manu_txt";
			this->manu_txt->Size = System::Drawing::Size(121, 20);
			this->manu_txt->TabIndex = 3;
			// 
			// item_lstbx
			// 
			this->item_lstbx->FormattingEnabled = true;
			this->item_lstbx->Location = System::Drawing::Point(15, 128);
			this->item_lstbx->Name = L"item_lstbx";
			this->item_lstbx->SelectionMode = System::Windows::Forms::SelectionMode::None;
			this->item_lstbx->Size = System::Drawing::Size(336, 95);
			this->item_lstbx->TabIndex = 4;
			// 
			// type_lbl
			// 
			this->type_lbl->AutoSize = true;
			this->type_lbl->Location = System::Drawing::Point(144, 98);
			this->type_lbl->Name = L"type_lbl";
			this->type_lbl->Size = System::Drawing::Size(46, 13);
			this->type_lbl->TabIndex = 5;
			this->type_lbl->Text = L"All Items";
			// 
			// fnd_btn
			// 
			this->fnd_btn->Location = System::Drawing::Point(15, 243);
			this->fnd_btn->Name = L"fnd_btn";
			this->fnd_btn->Size = System::Drawing::Size(75, 23);
			this->fnd_btn->TabIndex = 6;
			this->fnd_btn->Text = L"Find";
			this->fnd_btn->UseVisualStyleBackColor = true;
			this->fnd_btn->Click += gcnew System::EventHandler(this, &Items::fnd_btn_Click);
			// 
			// back_btn
			// 
			this->back_btn->Location = System::Drawing::Point(147, 282);
			this->back_btn->Name = L"back_btn";
			this->back_btn->Size = System::Drawing::Size(75, 23);
			this->back_btn->TabIndex = 7;
			this->back_btn->Text = L"Back";
			this->back_btn->UseVisualStyleBackColor = true;
			this->back_btn->Click += gcnew System::EventHandler(this, &Items::back_btn_Click);
			// 
			// add_btn
			// 
			this->add_btn->Location = System::Drawing::Point(276, 243);
			this->add_btn->Name = L"add_btn";
			this->add_btn->Size = System::Drawing::Size(75, 23);
			this->add_btn->TabIndex = 8;
			this->add_btn->Text = L"Add To Cart";
			this->add_btn->UseVisualStyleBackColor = true;
			this->add_btn->Click += gcnew System::EventHandler(this, &Items::add_btn_Click);
			// 
			// Items
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(363, 326);
			this->Controls->Add(this->add_btn);
			this->Controls->Add(this->back_btn);
			this->Controls->Add(this->fnd_btn);
			this->Controls->Add(this->type_lbl);
			this->Controls->Add(this->item_lstbx);
			this->Controls->Add(this->manu_txt);
			this->Controls->Add(this->cat_cmbx);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Items";
			this->Text = L"Items";
			this->Load += gcnew System::EventHandler(this, &Items::Items_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Items_Load(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::ifstream infile;
		std::string str;

		infile.open("pc.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				compiuterList.push_back(Compiuter(str));
			}
		}

		infile.close();

		infile.open("tv.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				televisionList.push_back(Television(str));
			}
		}

		infile.close();

		infile.open("phone.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				smartPhoneList.push_back(SmartPhone(str));
			}
		}
		infile.close();

		item_lstbx->Items->Add("Compiuters");
		for (int i = 0; i < compiuterList.size(); i++) {
			item_lstbx->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
		}

		item_lstbx->Items->Add("SmartPhones");
		for (int i = 0; i < smartPhoneList.size(); i++) {
			item_lstbx->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
		}

		item_lstbx->Items->Add("Televisions");
		for (int i = 0; i < televisionList.size(); i++) {
			item_lstbx->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
		}
	}
	private: System::Void back_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void cat_cmbx_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::ifstream infile;
		std::string str;

		infile.open("pc.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				compiuterList.push_back(Compiuter(str));
			}
		}

		infile.close();

		infile.open("phone.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				smartPhoneList.push_back(SmartPhone(str));
			}
		}
		infile.close();

		infile.open("tv.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				televisionList.push_back(Television(str));
			}
		}

		infile.close();

		if (cat_cmbx->SelectedIndex == 0) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "Compiuters";
			item_lstbx->SelectionMode = SelectionMode::One;

			for (int i = 0; i < compiuterList.size(); i++) {
				item_lstbx->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
			}
		}
		else if (cat_cmbx->SelectedIndex == 1) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "SmartPhones";
			item_lstbx->SelectionMode = SelectionMode::One;

			for (int i = 0; i < smartPhoneList.size(); i++) {
				item_lstbx->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
			}
		}
		else if (cat_cmbx->SelectedIndex == 2) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "Televisions";
			item_lstbx->SelectionMode = SelectionMode::One;

			for (int i = 0; i < televisionList.size(); i++) {
				item_lstbx->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
			}
		}
		else if (cat_cmbx->SelectedIndex == 3) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "All Items";
			item_lstbx->SelectionMode = SelectionMode::None;

			item_lstbx->Items->Add("Compiuters");
			for (int i = 0; i < compiuterList.size(); i++) {
				item_lstbx->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
			}

			item_lstbx->Items->Add("SmartPhones");
			for (int i = 0; i < smartPhoneList.size(); i++) {
				item_lstbx->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
			}

			item_lstbx->Items->Add("Televisions");
			for (int i = 0; i < televisionList.size(); i++) {
				item_lstbx->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
			}
		}
	}
	private: System::Void fnd_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::vector<Compiuter> compiuterList;
		std::vector<SmartPhone> smartPhoneList;
		std::vector<Television> televisionList;
		std::ifstream infile;
		std::string str;

		infile.open("pc.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				compiuterList.push_back(Compiuter(str));
			}
		}

		infile.close();

		infile.open("phone.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				smartPhoneList.push_back(SmartPhone(str));
			}
		}
		infile.close();

		infile.open("tv.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				televisionList.push_back(Television(str));
			}
		}

		infile.close();

		if (cat_cmbx->SelectedIndex == 0) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "Compiuters";
			item_lstbx->SelectionMode = SelectionMode::One;

			for (int i = 0; i < compiuterList.size(); i++) {
				if (marshal_as<std::string>(manu_txt->Text) == compiuterList[i].getManufacturer()) {
					item_lstbx->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
				}
			}
		}
		else if (cat_cmbx->SelectedIndex == 1) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "SmartPhones";
			item_lstbx->SelectionMode = SelectionMode::One;

			for (int i = 0; i < smartPhoneList.size(); i++) {
				if (marshal_as<std::string>(manu_txt->Text) == smartPhoneList[i].getManufacturer()) {
					item_lstbx->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
				}
			}
		}
		else if (cat_cmbx->SelectedIndex == 2) {
			item_lstbx->Items->Clear();
			type_lbl->Text = "Televisions";
			item_lstbx->SelectionMode = SelectionMode::One;

			for (int i = 0; i < televisionList.size(); i++) {
				if (marshal_as<std::string>(manu_txt->Text) == televisionList[i].getManufacturer()) {
					item_lstbx->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
				}
			}
		}
		else{
			item_lstbx->Items->Clear();
			type_lbl->Text = "All Items";
			item_lstbx->SelectionMode = SelectionMode::None;

			item_lstbx->Items->Add("Compiuters");
			for (int i = 0; i < compiuterList.size(); i++) {
				if (marshal_as<std::string>(manu_txt->Text) == compiuterList[i].getManufacturer()) {
					item_lstbx->Items->Add(marshal_as<String^>(compiuterList[i].toString() + "|" + compiuterList[i].toStringPc()));
				}
			}

			item_lstbx->Items->Add("SmartPhones");
			for (int i = 0; i < smartPhoneList.size(); i++) {
				if (marshal_as<std::string>(manu_txt->Text) == smartPhoneList[i].getManufacturer()) {
					item_lstbx->Items->Add(marshal_as<String^>(smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh()));
				}
			}

			item_lstbx->Items->Add("Televisions");
			for (int i = 0; i < televisionList.size(); i++) {
				if (marshal_as<std::string>(manu_txt->Text) == televisionList[i].getManufacturer()) {
					item_lstbx->Items->Add(marshal_as<String^>(televisionList[i].toString() + "|" + televisionList[i].toStringTv()));
				}
			}
		}
	}
	private: System::Void add_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		if (type == "physical") {
			if (cat_cmbx->SelectedIndex == 0) {
				physical->addCompiuter(Compiuter(marshal_as<std::string>(item_lstbx->SelectedItem->ToString())));
			}
			else if (cat_cmbx->SelectedIndex == 1) {
				physical->addSmartphone(SmartPhone(marshal_as<std::string>(item_lstbx->SelectedItem->ToString())));
			}
			else if (cat_cmbx->SelectedIndex == 2) {
				physical->addTelevision(Television(marshal_as<std::string>(item_lstbx->SelectedItem->ToString())));
			}
		}
		else if (type == "company") {
			if (cat_cmbx->SelectedIndex == 0) {
				company->addCompiuter(Compiuter(marshal_as<std::string>(item_lstbx->SelectedItem->ToString())));
			}
			else if (cat_cmbx->SelectedIndex == 1) {
				company->addSmartphone(SmartPhone(marshal_as<std::string>(item_lstbx->SelectedItem->ToString())));
			}
			else if (cat_cmbx->SelectedIndex == 2) {
				company->addTelevision(Television(marshal_as<std::string>(item_lstbx->SelectedItem->ToString())));
			}
		}
		MessageBox::Show("Item Succesfuly Added To Cart");
	}
};
}
