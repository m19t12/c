#pragma once
#include "Order.h"
#include "Physical.h"
#include "Company.h"
#include "OrderStatus.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for Customers
	/// </summary>
	public ref class Customers : public System::Windows::Forms::Form
	{
	public:
		Customers(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Customers()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^  physical_lbox;
	protected:
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ListBox^  company_lbox;
	private: System::Windows::Forms::Button^  status_btn;
	private: System::Windows::Forms::Button^  bck_btn;
	private: System::Windows::Forms::ComboBox^  phy_cmbx;
	private: System::Windows::Forms::ComboBox^  comp_cmbx;







	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->physical_lbox = (gcnew System::Windows::Forms::ListBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->company_lbox = (gcnew System::Windows::Forms::ListBox());
			this->status_btn = (gcnew System::Windows::Forms::Button());
			this->bck_btn = (gcnew System::Windows::Forms::Button());
			this->phy_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->comp_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->SuspendLayout();
			// 
			// physical_lbox
			// 
			this->physical_lbox->FormattingEnabled = true;
			this->physical_lbox->Location = System::Drawing::Point(12, 77);
			this->physical_lbox->Name = L"physical_lbox";
			this->physical_lbox->Size = System::Drawing::Size(368, 147);
			this->physical_lbox->TabIndex = 1;
			this->physical_lbox->SelectedIndexChanged += gcnew System::EventHandler(this, &Customers::physical_lbox_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(167, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(46, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Physical";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(162, 240);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(51, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Company";
			// 
			// company_lbox
			// 
			this->company_lbox->FormattingEnabled = true;
			this->company_lbox->Location = System::Drawing::Point(12, 299);
			this->company_lbox->Name = L"company_lbox";
			this->company_lbox->Size = System::Drawing::Size(368, 147);
			this->company_lbox->TabIndex = 4;
			this->company_lbox->SelectedIndexChanged += gcnew System::EventHandler(this, &Customers::company_lbox_SelectedIndexChanged);
			// 
			// status_btn
			// 
			this->status_btn->Location = System::Drawing::Point(12, 463);
			this->status_btn->Name = L"status_btn";
			this->status_btn->Size = System::Drawing::Size(92, 23);
			this->status_btn->TabIndex = 5;
			this->status_btn->Text = L"Change Status";
			this->status_btn->UseVisualStyleBackColor = true;
			this->status_btn->Click += gcnew System::EventHandler(this, &Customers::status_btn_Click);
			// 
			// bck_btn
			// 
			this->bck_btn->Location = System::Drawing::Point(288, 463);
			this->bck_btn->Name = L"bck_btn";
			this->bck_btn->Size = System::Drawing::Size(92, 23);
			this->bck_btn->TabIndex = 6;
			this->bck_btn->Text = L"Back";
			this->bck_btn->UseVisualStyleBackColor = true;
			this->bck_btn->Click += gcnew System::EventHandler(this, &Customers::bck_btn_Click);
			// 
			// phy_cmbx
			// 
			this->phy_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->phy_cmbx->FormattingEnabled = true;
			this->phy_cmbx->Location = System::Drawing::Point(12, 34);
			this->phy_cmbx->Name = L"phy_cmbx";
			this->phy_cmbx->Size = System::Drawing::Size(368, 21);
			this->phy_cmbx->TabIndex = 7;
			this->phy_cmbx->SelectedIndexChanged += gcnew System::EventHandler(this, &Customers::phy_cmbx_SelectedIndexChanged);
			// 
			// comp_cmbx
			// 
			this->comp_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comp_cmbx->FormattingEnabled = true;
			this->comp_cmbx->Location = System::Drawing::Point(12, 266);
			this->comp_cmbx->Name = L"comp_cmbx";
			this->comp_cmbx->Size = System::Drawing::Size(368, 21);
			this->comp_cmbx->TabIndex = 8;
			this->comp_cmbx->SelectedIndexChanged += gcnew System::EventHandler(this, &Customers::comp_cmbx_SelectedIndexChanged);
			// 
			// Customers
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(392, 498);
			this->Controls->Add(this->comp_cmbx);
			this->Controls->Add(this->phy_cmbx);
			this->Controls->Add(this->bck_btn);
			this->Controls->Add(this->status_btn);
			this->Controls->Add(this->company_lbox);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->physical_lbox);
			this->Name = L"Customers";
			this->Text = L"Customers";
			this->Load += gcnew System::EventHandler(this, &Customers::Customers_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Customers_Load(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::vector<Physical> physical;
		std::vector<Company> company;
		std::string str;

		infile.open("physical.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				physical.push_back(Physical(str));
			}
		}

		infile.close();

		infile.open("company.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				company.push_back(Company(str));
			}
		}

		infile.close();

		for (int i = 0; i < physical.size(); i++) {
			phy_cmbx->Items->Add(marshal_as<String^>(physical[i].toStringPh()));

		}

		for (int i = 0; i < company.size(); i++) {
			comp_cmbx->Items->Add(marshal_as<String^>(company[i].toStringCo()));
		}
	}
	private: System::Void phy_cmbx_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		std::string str;
		std::vector<Order> orderlist;
		std::ifstream infile;
		int actionId;
		Physical ph = Physical(marshal_as<std::string>(phy_cmbx->SelectedItem->ToString()));

		infile.open(ph.getUsername() + "_" + "Order.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				orderlist.push_back(Order(str));
			}
		}

		infile.close();

		physical_lbox->Items->Clear();

		for (int i = 0; i < orderlist.size(); i++) {
			physical_lbox->Items->Add(marshal_as<String^>(orderlist[i].toString()));
		}

	}
	private: System::Void bck_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
private: System::Void comp_cmbx_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	std::string str;
	std::vector<Order> orderlist;
	std::ifstream infile;
	int actionId;
	Company co = Company(marshal_as<std::string>(comp_cmbx->SelectedItem->ToString()));

	infile.open(co.getUsername() + "_" + "Order.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			orderlist.push_back(Order(str));
		}
	}

	infile.close();

	company_lbox->Items->Clear();

	for (int i = 0; i < orderlist.size(); i++) {
		company_lbox->Items->Add(marshal_as<String^>(orderlist[i].toString()));
	}

}
private: System::Void status_btn_Click(System::Object^  sender, System::EventArgs^  e) {
	if (physical_lbox->SelectedItem) {
		Physical ph = Physical(marshal_as<std::string>(phy_cmbx->SelectedItem->ToString()));
		OrderStatus^ orderStatusForm = gcnew OrderStatus(physical_lbox->SelectedItem->ToString(), marshal_as<String^>(ph.getUsername()));
		this->Hide();
		orderStatusForm->ShowDialog();
		this->Show();
	}
	else if (company_lbox->SelectedItem) {
		Company co = Company(marshal_as<std::string>(comp_cmbx->SelectedItem->ToString()));
		OrderStatus^ orderStatusForm = gcnew OrderStatus(company_lbox->SelectedItem->ToString(), marshal_as<String^>(co.getUsername()));
		this->Hide();
		orderStatusForm->ShowDialog();
		this->Show();
	}
}
private: System::Void physical_lbox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	company_lbox->ClearSelected();
}
private: System::Void company_lbox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	physical_lbox->ClearSelected();
}
};
}
