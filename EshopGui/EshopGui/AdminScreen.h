#pragma once
#include "Admin.h"
#include "Catalog.h"
#include "CreateUser.h"
#include "Customers.h"
#include "Task.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for AdminScreen
	/// </summary>
	public ref class AdminScreen : public System::Windows::Forms::Form
	{
	public:
		Admin *admin;
	public:
		AdminScreen(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		AdminScreen(Admin *admin)
		{
			InitializeComponent();
			this->admin = admin;
			//
			//TODO: Add the constructor code here
			//
		}

	public:
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AdminScreen()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  welcome_lbl;
	private: System::Windows::Forms::Button^  create_btn;
	private: System::Windows::Forms::Button^  show_items_btn;


	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  customers_btn;

	private: System::Windows::Forms::Button^  disco_btn;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->welcome_lbl = (gcnew System::Windows::Forms::Label());
			this->create_btn = (gcnew System::Windows::Forms::Button());
			this->show_items_btn = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->customers_btn = (gcnew System::Windows::Forms::Button());
			this->disco_btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// welcome_lbl
			// 
			this->welcome_lbl->AutoSize = true;
			this->welcome_lbl->Location = System::Drawing::Point(96, 9);
			this->welcome_lbl->Name = L"welcome_lbl";
			this->welcome_lbl->Size = System::Drawing::Size(35, 13);
			this->welcome_lbl->TabIndex = 0;
			this->welcome_lbl->Text = L"label1";
			// 
			// create_btn
			// 
			this->create_btn->Location = System::Drawing::Point(12, 73);
			this->create_btn->Name = L"create_btn";
			this->create_btn->Size = System::Drawing::Size(100, 23);
			this->create_btn->TabIndex = 1;
			this->create_btn->Text = L"Create User";
			this->create_btn->UseVisualStyleBackColor = true;
			this->create_btn->Click += gcnew System::EventHandler(this, &AdminScreen::create_btn_Click);
			// 
			// show_items_btn
			// 
			this->show_items_btn->Location = System::Drawing::Point(167, 73);
			this->show_items_btn->Name = L"show_items_btn";
			this->show_items_btn->Size = System::Drawing::Size(105, 23);
			this->show_items_btn->TabIndex = 2;
			this->show_items_btn->Text = L"Show Items";
			this->show_items_btn->UseVisualStyleBackColor = true;
			this->show_items_btn->Click += gcnew System::EventHandler(this, &AdminScreen::show_items_btn_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(167, 136);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(105, 23);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Task Calculation";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &AdminScreen::button3_Click);
			// 
			// customers_btn
			// 
			this->customers_btn->Location = System::Drawing::Point(12, 136);
			this->customers_btn->Name = L"customers_btn";
			this->customers_btn->Size = System::Drawing::Size(100, 23);
			this->customers_btn->TabIndex = 4;
			this->customers_btn->Text = L"Show Customers";
			this->customers_btn->UseVisualStyleBackColor = true;
			this->customers_btn->Click += gcnew System::EventHandler(this, &AdminScreen::customers_btn_Click);
			// 
			// disco_btn
			// 
			this->disco_btn->Location = System::Drawing::Point(82, 186);
			this->disco_btn->Name = L"disco_btn";
			this->disco_btn->Size = System::Drawing::Size(116, 23);
			this->disco_btn->TabIndex = 5;
			this->disco_btn->Text = L"Disconect";
			this->disco_btn->UseVisualStyleBackColor = true;
			this->disco_btn->Click += gcnew System::EventHandler(this, &AdminScreen::disco_btn_Click);
			// 
			// AdminScreen
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->disco_btn);
			this->Controls->Add(this->customers_btn);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->show_items_btn);
			this->Controls->Add(this->create_btn);
			this->Controls->Add(this->welcome_lbl);
			this->Name = L"AdminScreen";
			this->Text = L"AdminScreen";
			this->Load += gcnew System::EventHandler(this, &AdminScreen::AdminScreen_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void test_lbl_Click(System::Object^  sender, System::EventArgs^  e) {

	}
	private: System::Void AdminScreen_Load(System::Object^  sender, System::EventArgs^  e) {
		welcome_lbl->Text = "Welcome :" + " " + marshal_as<String^>(admin->getName());
	}
	private: System::Void create_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		CreateUser^ createForm = gcnew CreateUser();
		this->Hide();
		createForm->ShowDialog();
		this->Show();
	}
	private: System::Void disco_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
	private: System::Void show_items_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		Catalog^ catalogForm = gcnew Catalog();
		this->Hide();
		catalogForm->ShowDialog();
		this->Show();
	}
	private: System::Void customers_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		Customers^ customerForm = gcnew Customers();
		this->Hide();
		customerForm->ShowDialog();
		this->Show();
	}
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
		Task^ taskForm = gcnew Task();
		this->Hide();
		taskForm->ShowDialog();
		this->Show();
	}
};
}
