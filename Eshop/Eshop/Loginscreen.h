#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H
#include <string>
#include <iostream>
#include "Admin.h"
#include "Physical.h"
#include "Company.h"

class LoginScreen {

public:
	LoginScreen();

	void Login();

	void LoadCart();

	void RecoverAccount();

	Physical getPhysicalUser();

	Company getCompanyUser();

	Admin getAdminUser();

	string getType();

	int getCode();

	string getUsername();

private:
	Company company;
	Physical physical;
	Admin admin;
	string str, username, password, type = "", name, surename, security;
	int code = 1;
	char action;
	vector<User> users;
	vector<Physical> physicalUsers;
	vector<Company> companyUsers;
	vector<Admin> adminUsers;
};

#endif // !LOGINSCREEN_H

