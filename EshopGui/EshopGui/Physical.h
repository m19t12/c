#ifndef PHYSICAL_H
#define PHYSICAL_H
#include "Customer.h"

class Physical : public Customer {

public:
	Physical();

	Physical(std::string name, std::string surename, std::string am, std::string security);

	Physical(std::string strPh);

	std::string getName();

	void setName(std::string name);

	std::string getSurename();

	void setSurename(std::string surename);

	std::string getAm();

	void setAm(std::string am);

	std::string getSecurity();

	void setSecurity(std::string security);

	void save(std::ofstream &outfile);

	std::string toStringPh();

private:
	std::string name, surename, am, security;
};

#endif // !PHYSICAL_H

