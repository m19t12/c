#pragma once
#include "Compiuter.h"
#include "Smartphone.h"
#include "Television.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for UpdateItem
	/// </summary>
	public ref class UpdateItem : public System::Windows::Forms::Form
	{
	public:
		String^ type;
		String^ item;
		Compiuter *pc;
		SmartPhone *ph;
		Television *tv;
	private: System::Windows::Forms::TextBox^  price_txt;
	public:
	private: System::Windows::Forms::TextBox^  ph_txt;
	private: System::Windows::Forms::TextBox^  descri_txt;
	private: System::Windows::Forms::TextBox^  manu_txt;
	private: System::Windows::Forms::TextBox^  model_txt;
	private: System::Windows::Forms::TextBox^  cd_txt;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Panel^  pc_pnl;

	private: System::Windows::Forms::TextBox^  hdtype_txt;
	private: System::Windows::Forms::TextBox^  gpu_txt;
	private: System::Windows::Forms::TextBox^  cpu_txt;
	private: System::Windows::Forms::TextBox^  hdsize_txt;
	private: System::Windows::Forms::TextBox^  ram_txt;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Panel^  phone_pnl;

	private: System::Windows::Forms::TextBox^  video_txt;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::TextBox^  btr_txt;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::TextBox^  scrSize_txt;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Panel^  tv_pnl;

	private: System::Windows::Forms::TextBox^  three_txt;
	private: System::Windows::Forms::TextBox^  curve_txt;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Button^  update;
	private: System::Windows::Forms::Button^  back;

	public:
		UpdateItem(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		UpdateItem(Compiuter *pc, String^ item)
		{
			InitializeComponent();
			this->item = item;
			this->pc = pc;
			type = "pc";
			//
			//TODO: Add the constructor code here
			//
		}

		UpdateItem(SmartPhone *ph, String^ item)
		{
			InitializeComponent();
			this->item = item;
			this->ph = ph;
			type = "phone";
			//
			//TODO: Add the constructor code here
			//
		}

		UpdateItem(Television *tv, String^ item)
		{
			InitializeComponent();
			this->item = item;
			this->tv = tv;
			type = "tv";
			//
			//TODO: Add the constructor code here
			//
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~UpdateItem()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->price_txt = (gcnew System::Windows::Forms::TextBox());
			this->ph_txt = (gcnew System::Windows::Forms::TextBox());
			this->descri_txt = (gcnew System::Windows::Forms::TextBox());
			this->manu_txt = (gcnew System::Windows::Forms::TextBox());
			this->model_txt = (gcnew System::Windows::Forms::TextBox());
			this->cd_txt = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->pc_pnl = (gcnew System::Windows::Forms::Panel());
			this->hdtype_txt = (gcnew System::Windows::Forms::TextBox());
			this->gpu_txt = (gcnew System::Windows::Forms::TextBox());
			this->cpu_txt = (gcnew System::Windows::Forms::TextBox());
			this->hdsize_txt = (gcnew System::Windows::Forms::TextBox());
			this->ram_txt = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->phone_pnl = (gcnew System::Windows::Forms::Panel());
			this->video_txt = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->btr_txt = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->scrSize_txt = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->tv_pnl = (gcnew System::Windows::Forms::Panel());
			this->three_txt = (gcnew System::Windows::Forms::TextBox());
			this->curve_txt = (gcnew System::Windows::Forms::TextBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->update = (gcnew System::Windows::Forms::Button());
			this->back = (gcnew System::Windows::Forms::Button());
			this->pc_pnl->SuspendLayout();
			this->phone_pnl->SuspendLayout();
			this->tv_pnl->SuspendLayout();
			this->SuspendLayout();
			// 
			// price_txt
			// 
			this->price_txt->Location = System::Drawing::Point(191, 178);
			this->price_txt->Name = L"price_txt";
			this->price_txt->Size = System::Drawing::Size(100, 20);
			this->price_txt->TabIndex = 26;
			// 
			// ph_txt
			// 
			this->ph_txt->Location = System::Drawing::Point(191, 145);
			this->ph_txt->Name = L"ph_txt";
			this->ph_txt->Size = System::Drawing::Size(100, 20);
			this->ph_txt->TabIndex = 25;
			// 
			// descri_txt
			// 
			this->descri_txt->Location = System::Drawing::Point(191, 115);
			this->descri_txt->Name = L"descri_txt";
			this->descri_txt->Size = System::Drawing::Size(100, 20);
			this->descri_txt->TabIndex = 24;
			// 
			// manu_txt
			// 
			this->manu_txt->Location = System::Drawing::Point(191, 85);
			this->manu_txt->Name = L"manu_txt";
			this->manu_txt->Size = System::Drawing::Size(100, 20);
			this->manu_txt->TabIndex = 23;
			// 
			// model_txt
			// 
			this->model_txt->Location = System::Drawing::Point(191, 59);
			this->model_txt->Name = L"model_txt";
			this->model_txt->Size = System::Drawing::Size(100, 20);
			this->model_txt->TabIndex = 22;
			// 
			// cd_txt
			// 
			this->cd_txt->Location = System::Drawing::Point(191, 27);
			this->cd_txt->Name = L"cd_txt";
			this->cd_txt->Size = System::Drawing::Size(100, 20);
			this->cd_txt->TabIndex = 21;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(63, 178);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(31, 13);
			this->label6->TabIndex = 20;
			this->label6->Text = L"Price";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(63, 152);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(35, 13);
			this->label5->TabIndex = 19;
			this->label5->Text = L"Photo";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(63, 123);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(60, 13);
			this->label4->TabIndex = 18;
			this->label4->Text = L"Description";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(63, 92);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(70, 13);
			this->label3->TabIndex = 17;
			this->label3->Text = L"Manufacturer";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(63, 66);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(36, 13);
			this->label2->TabIndex = 16;
			this->label2->Text = L"Model";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(66, 34);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(32, 13);
			this->label1->TabIndex = 15;
			this->label1->Text = L"Code";
			// 
			// pc_pnl
			// 
			this->pc_pnl->Controls->Add(this->hdtype_txt);
			this->pc_pnl->Controls->Add(this->gpu_txt);
			this->pc_pnl->Controls->Add(this->cpu_txt);
			this->pc_pnl->Controls->Add(this->hdsize_txt);
			this->pc_pnl->Controls->Add(this->ram_txt);
			this->pc_pnl->Controls->Add(this->label13);
			this->pc_pnl->Controls->Add(this->label12);
			this->pc_pnl->Controls->Add(this->label11);
			this->pc_pnl->Controls->Add(this->label10);
			this->pc_pnl->Controls->Add(this->label9);
			this->pc_pnl->Location = System::Drawing::Point(38, 207);
			this->pc_pnl->Name = L"pc_pnl";
			this->pc_pnl->Size = System::Drawing::Size(266, 151);
			this->pc_pnl->TabIndex = 27;
			this->pc_pnl->Visible = false;
			// 
			// hdtype_txt
			// 
			this->hdtype_txt->Location = System::Drawing::Point(151, 118);
			this->hdtype_txt->Name = L"hdtype_txt";
			this->hdtype_txt->Size = System::Drawing::Size(100, 20);
			this->hdtype_txt->TabIndex = 20;
			// 
			// gpu_txt
			// 
			this->gpu_txt->Location = System::Drawing::Point(151, 91);
			this->gpu_txt->Name = L"gpu_txt";
			this->gpu_txt->Size = System::Drawing::Size(100, 20);
			this->gpu_txt->TabIndex = 19;
			// 
			// cpu_txt
			// 
			this->cpu_txt->Location = System::Drawing::Point(151, 64);
			this->cpu_txt->Name = L"cpu_txt";
			this->cpu_txt->Size = System::Drawing::Size(100, 20);
			this->cpu_txt->TabIndex = 18;
			// 
			// hdsize_txt
			// 
			this->hdsize_txt->Location = System::Drawing::Point(151, 37);
			this->hdsize_txt->Name = L"hdsize_txt";
			this->hdsize_txt->Size = System::Drawing::Size(100, 20);
			this->hdsize_txt->TabIndex = 17;
			// 
			// ram_txt
			// 
			this->ram_txt->Location = System::Drawing::Point(151, 10);
			this->ram_txt->Name = L"ram_txt";
			this->ram_txt->Size = System::Drawing::Size(100, 20);
			this->ram_txt->TabIndex = 16;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(23, 121);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(81, 13);
			this->label13->TabIndex = 15;
			this->label13->Text = L"Hard Disk Type";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(23, 94);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(27, 13);
			this->label12->TabIndex = 14;
			this->label12->Text = L"Gpu";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(23, 67);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(26, 13);
			this->label11->TabIndex = 13;
			this->label11->Text = L"Cpu";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(23, 44);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(77, 13);
			this->label10->TabIndex = 12;
			this->label10->Text = L"Hard Disk Size";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(23, 17);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(29, 13);
			this->label9->TabIndex = 11;
			this->label9->Text = L"Ram";
			// 
			// phone_pnl
			// 
			this->phone_pnl->Controls->Add(this->video_txt);
			this->phone_pnl->Controls->Add(this->label15);
			this->phone_pnl->Controls->Add(this->btr_txt);
			this->phone_pnl->Controls->Add(this->label14);
			this->phone_pnl->Controls->Add(this->scrSize_txt);
			this->phone_pnl->Controls->Add(this->label7);
			this->phone_pnl->Location = System::Drawing::Point(22, 217);
			this->phone_pnl->Name = L"phone_pnl";
			this->phone_pnl->Size = System::Drawing::Size(295, 166);
			this->phone_pnl->TabIndex = 21;
			this->phone_pnl->Visible = false;
			// 
			// video_txt
			// 
			this->video_txt->Location = System::Drawing::Point(159, 73);
			this->video_txt->Name = L"video_txt";
			this->video_txt->Size = System::Drawing::Size(100, 20);
			this->video_txt->TabIndex = 11;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(31, 81);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(34, 13);
			this->label15->TabIndex = 10;
			this->label15->Text = L"Video";
			// 
			// btr_txt
			// 
			this->btr_txt->Location = System::Drawing::Point(159, 39);
			this->btr_txt->Name = L"btr_txt";
			this->btr_txt->Size = System::Drawing::Size(100, 20);
			this->btr_txt->TabIndex = 9;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(31, 45);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(40, 13);
			this->label14->TabIndex = 8;
			this->label14->Text = L"Battery";
			// 
			// scrSize_txt
			// 
			this->scrSize_txt->Location = System::Drawing::Point(159, 8);
			this->scrSize_txt->Name = L"scrSize_txt";
			this->scrSize_txt->Size = System::Drawing::Size(100, 20);
			this->scrSize_txt->TabIndex = 7;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(31, 14);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(64, 13);
			this->label7->TabIndex = 6;
			this->label7->Text = L"Screen Size";
			// 
			// tv_pnl
			// 
			this->tv_pnl->Controls->Add(this->three_txt);
			this->tv_pnl->Controls->Add(this->curve_txt);
			this->tv_pnl->Controls->Add(this->label17);
			this->tv_pnl->Controls->Add(this->label16);
			this->tv_pnl->Location = System::Drawing::Point(12, 204);
			this->tv_pnl->Name = L"tv_pnl";
			this->tv_pnl->Size = System::Drawing::Size(329, 173);
			this->tv_pnl->TabIndex = 12;
			this->tv_pnl->Visible = false;
			// 
			// three_txt
			// 
			this->three_txt->Location = System::Drawing::Point(174, 49);
			this->three_txt->Name = L"three_txt";
			this->three_txt->Size = System::Drawing::Size(100, 20);
			this->three_txt->TabIndex = 7;
			// 
			// curve_txt
			// 
			this->curve_txt->Location = System::Drawing::Point(174, 21);
			this->curve_txt->Name = L"curve_txt";
			this->curve_txt->Size = System::Drawing::Size(100, 20);
			this->curve_txt->TabIndex = 6;
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(49, 52);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(21, 13);
			this->label17->TabIndex = 5;
			this->label17->Text = L"3D";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(49, 21);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(35, 13);
			this->label16->TabIndex = 4;
			this->label16->Text = L"Curve";
			// 
			// update
			// 
			this->update->Location = System::Drawing::Point(38, 400);
			this->update->Name = L"update";
			this->update->Size = System::Drawing::Size(75, 23);
			this->update->TabIndex = 28;
			this->update->Text = L"Update";
			this->update->UseVisualStyleBackColor = true;
			this->update->Click += gcnew System::EventHandler(this, &UpdateItem::update_Click);
			// 
			// back
			// 
			this->back->Location = System::Drawing::Point(229, 400);
			this->back->Name = L"back";
			this->back->Size = System::Drawing::Size(75, 23);
			this->back->TabIndex = 29;
			this->back->Text = L"Back";
			this->back->UseVisualStyleBackColor = true;
			this->back->Click += gcnew System::EventHandler(this, &UpdateItem::back_Click);
			// 
			// UpdateItem
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(375, 463);
			this->Controls->Add(this->back);
			this->Controls->Add(this->update);
			this->Controls->Add(this->tv_pnl);
			this->Controls->Add(this->phone_pnl);
			this->Controls->Add(this->pc_pnl);
			this->Controls->Add(this->price_txt);
			this->Controls->Add(this->ph_txt);
			this->Controls->Add(this->descri_txt);
			this->Controls->Add(this->manu_txt);
			this->Controls->Add(this->model_txt);
			this->Controls->Add(this->cd_txt);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"UpdateItem";
			this->Text = L"UpdateItem";
			this->Load += gcnew System::EventHandler(this, &UpdateItem::UpdateItem_Load);
			this->pc_pnl->ResumeLayout(false);
			this->pc_pnl->PerformLayout();
			this->phone_pnl->ResumeLayout(false);
			this->phone_pnl->PerformLayout();
			this->tv_pnl->ResumeLayout(false);
			this->tv_pnl->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void UpdateItem_Load(System::Object^  sender, System::EventArgs^  e) {
		if (type == "pc") {
			pc_pnl->Visible = true;

			cd_txt->Text = Convert::ToString(pc->getCode());
			model_txt->Text = marshal_as<String^>(pc->getModel());
			manu_txt->Text = marshal_as<String^>(pc->getManufacturer());
			descri_txt->Text = marshal_as<String^>(pc->getDescription());
			ph_txt->Text = Convert::ToString(pc->getPhoto());
			price_txt->Text = Convert::ToString(pc->getPrice());
			ram_txt->Text = Convert::ToString(pc->getRam());
			hdsize_txt->Text = Convert::ToString(pc->getHdsize());
			cpu_txt->Text = Convert::ToString(pc->getCpu());
			gpu_txt->Text = marshal_as<String^>(pc->getGpu());
			hdtype_txt->Text = marshal_as<String^>(pc->getHdtype());
		}
		else if (type == "phone") {
			phone_pnl->Visible = true;

			cd_txt->Text = Convert::ToString(ph->getCode());
			model_txt->Text = marshal_as<String^>(ph->getModel());
			manu_txt->Text = marshal_as<String^>(ph->getManufacturer());
			descri_txt->Text = marshal_as<String^>(ph->getDescription());
			ph_txt->Text = Convert::ToString(ph->getPhoto());
			price_txt->Text = Convert::ToString(ph->getPrice());
			scrSize_txt->Text = Convert::ToString(ph->getScreensize());
			btr_txt->Text = Convert::ToString(ph->getBattery());
			video_txt->Text = Convert::ToString(ph->getVideo());
		}
		else if (type == "tv") {
			tv_pnl->Visible = true;

			cd_txt->Text = Convert::ToString(tv->getCode());
			model_txt->Text = marshal_as<String^>(tv->getModel());
			manu_txt->Text = marshal_as<String^>(tv->getManufacturer());
			descri_txt->Text = marshal_as<String^>(tv->getDescription());
			ph_txt->Text = Convert::ToString(tv->getPhoto());
			price_txt->Text = Convert::ToString(tv->getPrice());
			curve_txt->Text = Convert::ToString(tv->getCurve());
			three_txt->Text = Convert::ToString(tv->getThreed());
		}
	}
	private: System::Void back_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void update_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::ofstream outfile;
		std::string str;

		if (type == "pc") {
			pc->setCode(Convert::ToInt32(cd_txt->Text));
			pc->setModel(marshal_as<std::string>(model_txt->Text));
			pc->setManufacturer(marshal_as<std::string>(manu_txt->Text));
			pc->setDescription(marshal_as<std::string>(descri_txt->Text));
			pc->setPhoto(Convert::ToInt32(ph_txt->Text));
			pc->setPrice(Convert::ToDouble(price_txt->Text));
			pc->setRam(Convert::ToInt32(ram_txt->Text));
			pc->setHdsize(Convert::ToInt32(hdsize_txt->Text));
			pc->setCpu(Convert::ToDouble(cpu_txt->Text));
			pc->setGpu(marshal_as<std::string>(gpu_txt->Text));
			pc->setHdtype(marshal_as<std::string>(hdtype_txt->Text));

			infile.open("pc.dat");
			outfile.open("temp.dat");

			while (!infile.eof()) {
				getline(infile, str);
				if (marshal_as<String^>(str) == item) {
					pc->save(outfile);
				}
				else
					outfile << str << std::endl;
			}

			infile.close();
			outfile.close();

			remove("pc.dat");
			rename("temp.dat", "pc.dat");
			MessageBox::Show("Item Succesfuly Updated");
		}
		else if (type == "phone") {
			ph->setCode(Convert::ToInt32(cd_txt->Text));
			ph->setModel(marshal_as<std::string>(model_txt->Text));
			ph->setManufacturer(marshal_as<std::string>(manu_txt->Text));
			ph->setDescription(marshal_as<std::string>(descri_txt->Text));
			ph->setPhoto(Convert::ToInt32(ph_txt->Text));
			ph->setPrice(Convert::ToDouble(price_txt->Text));
			ph->setScreensize(Convert::ToDouble(scrSize_txt->Text));
			ph->setBattery(Convert::ToInt32(btr_txt->Text));
			ph->setVideo(Convert::ToInt32(video_txt->Text));

			infile.open("phone.dat");
			outfile.open("temp.dat");

			while (!infile.eof()) {
				getline(infile, str);
				if (marshal_as<String^>(str) == item) {
					ph->save(outfile);
				}
				else
					outfile << str << std::endl;
			}

			infile.close();
			outfile.close();

			remove("phone.dat");
			rename("temp.dat", "phone.dat");
			MessageBox::Show("Item Succesfuly Updated");
		}
		else if (type == "tv") {
			tv->setCode(Convert::ToInt32(cd_txt->Text));
			tv->setModel(marshal_as<std::string>(model_txt->Text));
			tv->setManufacturer(marshal_as<std::string>(manu_txt->Text));
			tv->setDescription(marshal_as<std::string>(descri_txt->Text));
			tv->setPhoto(Convert::ToInt32(ph_txt->Text));
			tv->setPrice(Convert::ToDouble(price_txt->Text));
			tv->setCurve(Convert::ToInt32(curve_txt->Text));
			tv->setThreed(Convert::ToDouble(three_txt->Text));

			infile.open("tv.dat");
			outfile.open("temp.dat");

			while (!infile.eof()) {
				getline(infile, str);
				if (marshal_as<String^>(str) == item) {
					tv->save(outfile);
				}
				else
					outfile << str << std::endl;
			}

			infile.close();
			outfile.close();

			remove("tv.dat");
			rename("temp.dat", "tv.dat");
			MessageBox::Show("Item Succesfuly Updated");
		}
	}
	};
}
