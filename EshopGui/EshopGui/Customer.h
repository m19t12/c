#ifndef CUSTOMER_H
#define CUSTOMER_H
#include "User.h"
#include "Shoppingcart.h"

class Customer : public ShoppingCart, public User {

public:
	Customer();

	Customer(int afm, int phonenumber, std::string address);

	int getAfm();

	void setAfm(int afm);

	int getPhonenumber();

	void setPhonenumber(int phonenumber);

	std::string getAddress();

	void setAddress(std::string address);

private:
	int afm, phonenumber;
	std::string address;
};

#endif // !CUSTOMER_H

