#ifndef TELEVISION_H
#define TELEVISION_H
#include "Product.h"
#include <string>
#include <fstream>
#include <sstream>

class Television : public Product {
public:
	Television();

	Television(int curve, string manufacturer, bool threed);

	Television(string strTv);

	int getCurve();

	void setCurve(int curve);

	bool getThreed();

	void setThreed(bool threed);

	void save(ofstream &outfile);

	string toStringTv();

private:
	int curve;
	string manufacturer;
	bool threed;
};

#endif
