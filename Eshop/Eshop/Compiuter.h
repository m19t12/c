#ifndef COMPIUTER_H
#define COMPIUTER_H
#include "Product.h"
#include <fstream>
#include <sstream>
#include <string>

class Compiuter :
	public Product
{
public:
	Compiuter();

	Compiuter(int ram, int hdsize, double cpu, string gpu, string hdtype);

	Compiuter(string strCompiuter);

	int getRam();

	void setRam(int ram);

	int getHdsize();

	void setHdsize(int hdsize);

	double getCpu();

	void setCpu(double cpu);

	string getGpu();

	void setGpu(string gpu);

	string getHdtype();

	void setHdtype(string hdtype);

	void save(ofstream &outfile);

	string toStringPc();

private:
	int ram, hdsize;
	double cpu;
	string gpu;
	string hdtype;
};

#endif
