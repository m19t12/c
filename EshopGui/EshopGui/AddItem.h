#pragma once
#include "Compiuter.h"
#include "Smartphone.h"
#include "Television.h"
#include <fstream>
#include <vector>
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for AddItem
	/// </summary>
	public ref class AddItem : public System::Windows::Forms::Form
	{
	public:
		AddItem(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AddItem()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::TextBox^  cd_txt;
	private: System::Windows::Forms::TextBox^  model_txt;
	private: System::Windows::Forms::TextBox^  manu_txt;
	private: System::Windows::Forms::TextBox^  descri_txt;
	private: System::Windows::Forms::TextBox^  ph_txt;
	private: System::Windows::Forms::TextBox^  price_txt;







	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::ComboBox^  type_cmbx;

	private: System::Windows::Forms::Panel^  pc_pnl;
	private: System::Windows::Forms::TextBox^  hdtype_txt;
	private: System::Windows::Forms::TextBox^  gpu_txt;
	private: System::Windows::Forms::TextBox^  cpu_txt;
	private: System::Windows::Forms::TextBox^  hdsize_txt;
	private: System::Windows::Forms::TextBox^  ram_txt;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Button^  crt_btn;
	private: System::Windows::Forms::Button^  bck_btn;
	private: System::Windows::Forms::Panel^  phone_pnl;

	private: System::Windows::Forms::TextBox^  video_txt;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::TextBox^  btr_txt;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::TextBox^  scrSize_txt;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::TextBox^  curve_txt;
	private: System::Windows::Forms::TextBox^  three_txt;
	private: System::Windows::Forms::Panel^  tv_pnl;









	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->cd_txt = (gcnew System::Windows::Forms::TextBox());
			this->model_txt = (gcnew System::Windows::Forms::TextBox());
			this->manu_txt = (gcnew System::Windows::Forms::TextBox());
			this->descri_txt = (gcnew System::Windows::Forms::TextBox());
			this->ph_txt = (gcnew System::Windows::Forms::TextBox());
			this->price_txt = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->type_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->pc_pnl = (gcnew System::Windows::Forms::Panel());
			this->hdtype_txt = (gcnew System::Windows::Forms::TextBox());
			this->gpu_txt = (gcnew System::Windows::Forms::TextBox());
			this->cpu_txt = (gcnew System::Windows::Forms::TextBox());
			this->hdsize_txt = (gcnew System::Windows::Forms::TextBox());
			this->ram_txt = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->phone_pnl = (gcnew System::Windows::Forms::Panel());
			this->video_txt = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->btr_txt = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->scrSize_txt = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->crt_btn = (gcnew System::Windows::Forms::Button());
			this->bck_btn = (gcnew System::Windows::Forms::Button());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->curve_txt = (gcnew System::Windows::Forms::TextBox());
			this->three_txt = (gcnew System::Windows::Forms::TextBox());
			this->tv_pnl = (gcnew System::Windows::Forms::Panel());
			this->pc_pnl->SuspendLayout();
			this->phone_pnl->SuspendLayout();
			this->tv_pnl->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(38, 38);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(32, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Code";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(35, 70);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(36, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Model";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(35, 96);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(70, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Manufacturer";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(35, 127);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(60, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Description";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(35, 156);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(35, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"Photo";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(35, 182);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(31, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Price";
			// 
			// cd_txt
			// 
			this->cd_txt->Location = System::Drawing::Point(163, 31);
			this->cd_txt->Name = L"cd_txt";
			this->cd_txt->Size = System::Drawing::Size(100, 20);
			this->cd_txt->TabIndex = 6;
			// 
			// model_txt
			// 
			this->model_txt->Location = System::Drawing::Point(163, 63);
			this->model_txt->Name = L"model_txt";
			this->model_txt->Size = System::Drawing::Size(100, 20);
			this->model_txt->TabIndex = 7;
			// 
			// manu_txt
			// 
			this->manu_txt->Location = System::Drawing::Point(163, 89);
			this->manu_txt->Name = L"manu_txt";
			this->manu_txt->Size = System::Drawing::Size(100, 20);
			this->manu_txt->TabIndex = 8;
			// 
			// descri_txt
			// 
			this->descri_txt->Location = System::Drawing::Point(163, 119);
			this->descri_txt->Name = L"descri_txt";
			this->descri_txt->Size = System::Drawing::Size(100, 20);
			this->descri_txt->TabIndex = 9;
			// 
			// ph_txt
			// 
			this->ph_txt->Location = System::Drawing::Point(163, 149);
			this->ph_txt->Name = L"ph_txt";
			this->ph_txt->Size = System::Drawing::Size(100, 20);
			this->ph_txt->TabIndex = 10;
			// 
			// price_txt
			// 
			this->price_txt->Location = System::Drawing::Point(163, 182);
			this->price_txt->Name = L"price_txt";
			this->price_txt->Size = System::Drawing::Size(100, 20);
			this->price_txt->TabIndex = 11;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(35, 215);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(31, 13);
			this->label8->TabIndex = 13;
			this->label8->Text = L"Type";
			// 
			// type_cmbx
			// 
			this->type_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->type_cmbx->FormattingEnabled = true;
			this->type_cmbx->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"pc", L"phone", L"tv" });
			this->type_cmbx->Location = System::Drawing::Point(163, 215);
			this->type_cmbx->Name = L"type_cmbx";
			this->type_cmbx->Size = System::Drawing::Size(100, 21);
			this->type_cmbx->TabIndex = 14;
			this->type_cmbx->SelectedIndexChanged += gcnew System::EventHandler(this, &AddItem::type_cmbx_SelectedIndexChanged);
			// 
			// pc_pnl
			// 
			this->pc_pnl->Controls->Add(this->hdtype_txt);
			this->pc_pnl->Controls->Add(this->gpu_txt);
			this->pc_pnl->Controls->Add(this->cpu_txt);
			this->pc_pnl->Controls->Add(this->hdsize_txt);
			this->pc_pnl->Controls->Add(this->ram_txt);
			this->pc_pnl->Controls->Add(this->label13);
			this->pc_pnl->Controls->Add(this->label12);
			this->pc_pnl->Controls->Add(this->label11);
			this->pc_pnl->Controls->Add(this->label10);
			this->pc_pnl->Controls->Add(this->label9);
			this->pc_pnl->Location = System::Drawing::Point(4, 242);
			this->pc_pnl->Name = L"pc_pnl";
			this->pc_pnl->Size = System::Drawing::Size(270, 151);
			this->pc_pnl->TabIndex = 15;
			this->pc_pnl->Visible = false;
			// 
			// hdtype_txt
			// 
			this->hdtype_txt->Location = System::Drawing::Point(160, 124);
			this->hdtype_txt->Name = L"hdtype_txt";
			this->hdtype_txt->Size = System::Drawing::Size(100, 20);
			this->hdtype_txt->TabIndex = 10;
			// 
			// gpu_txt
			// 
			this->gpu_txt->Location = System::Drawing::Point(160, 97);
			this->gpu_txt->Name = L"gpu_txt";
			this->gpu_txt->Size = System::Drawing::Size(100, 20);
			this->gpu_txt->TabIndex = 9;
			// 
			// cpu_txt
			// 
			this->cpu_txt->Location = System::Drawing::Point(160, 70);
			this->cpu_txt->Name = L"cpu_txt";
			this->cpu_txt->Size = System::Drawing::Size(100, 20);
			this->cpu_txt->TabIndex = 8;
			// 
			// hdsize_txt
			// 
			this->hdsize_txt->Location = System::Drawing::Point(160, 43);
			this->hdsize_txt->Name = L"hdsize_txt";
			this->hdsize_txt->Size = System::Drawing::Size(100, 20);
			this->hdsize_txt->TabIndex = 7;
			// 
			// ram_txt
			// 
			this->ram_txt->Location = System::Drawing::Point(160, 16);
			this->ram_txt->Name = L"ram_txt";
			this->ram_txt->Size = System::Drawing::Size(100, 20);
			this->ram_txt->TabIndex = 6;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(32, 127);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(81, 13);
			this->label13->TabIndex = 5;
			this->label13->Text = L"Hard Disk Type";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(32, 100);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(27, 13);
			this->label12->TabIndex = 4;
			this->label12->Text = L"Gpu";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(32, 73);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(26, 13);
			this->label11->TabIndex = 3;
			this->label11->Text = L"Cpu";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(32, 50);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(77, 13);
			this->label10->TabIndex = 2;
			this->label10->Text = L"Hard Disk Size";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(32, 23);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(29, 13);
			this->label9->TabIndex = 1;
			this->label9->Text = L"Ram";
			// 
			// phone_pnl
			// 
			this->phone_pnl->Controls->Add(this->video_txt);
			this->phone_pnl->Controls->Add(this->label15);
			this->phone_pnl->Controls->Add(this->btr_txt);
			this->phone_pnl->Controls->Add(this->label14);
			this->phone_pnl->Controls->Add(this->scrSize_txt);
			this->phone_pnl->Controls->Add(this->label7);
			this->phone_pnl->Location = System::Drawing::Point(4, 251);
			this->phone_pnl->Name = L"phone_pnl";
			this->phone_pnl->Size = System::Drawing::Size(285, 139);
			this->phone_pnl->TabIndex = 11;
			this->phone_pnl->Visible = false;
			// 
			// video_txt
			// 
			this->video_txt->Location = System::Drawing::Point(160, 78);
			this->video_txt->Name = L"video_txt";
			this->video_txt->Size = System::Drawing::Size(100, 20);
			this->video_txt->TabIndex = 5;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(32, 86);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(34, 13);
			this->label15->TabIndex = 4;
			this->label15->Text = L"Video";
			// 
			// btr_txt
			// 
			this->btr_txt->Location = System::Drawing::Point(160, 44);
			this->btr_txt->Name = L"btr_txt";
			this->btr_txt->Size = System::Drawing::Size(100, 20);
			this->btr_txt->TabIndex = 3;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(32, 50);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(40, 13);
			this->label14->TabIndex = 2;
			this->label14->Text = L"Battery";
			// 
			// scrSize_txt
			// 
			this->scrSize_txt->Location = System::Drawing::Point(160, 13);
			this->scrSize_txt->Name = L"scrSize_txt";
			this->scrSize_txt->Size = System::Drawing::Size(100, 20);
			this->scrSize_txt->TabIndex = 1;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(32, 19);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(64, 13);
			this->label7->TabIndex = 0;
			this->label7->Text = L"Screen Size";
			// 
			// crt_btn
			// 
			this->crt_btn->Location = System::Drawing::Point(20, 436);
			this->crt_btn->Name = L"crt_btn";
			this->crt_btn->Size = System::Drawing::Size(75, 23);
			this->crt_btn->TabIndex = 16;
			this->crt_btn->Text = L"Create";
			this->crt_btn->UseVisualStyleBackColor = true;
			this->crt_btn->Click += gcnew System::EventHandler(this, &AddItem::crt_btn_Click);
			// 
			// bck_btn
			// 
			this->bck_btn->Location = System::Drawing::Point(214, 436);
			this->bck_btn->Name = L"bck_btn";
			this->bck_btn->Size = System::Drawing::Size(75, 23);
			this->bck_btn->TabIndex = 17;
			this->bck_btn->Text = L"Back";
			this->bck_btn->UseVisualStyleBackColor = true;
			this->bck_btn->Click += gcnew System::EventHandler(this, &AddItem::bck_btn_Click);
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(35, 19);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(35, 13);
			this->label16->TabIndex = 0;
			this->label16->Text = L"Curve";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(35, 50);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(21, 13);
			this->label17->TabIndex = 1;
			this->label17->Text = L"3D";
			// 
			// curve_txt
			// 
			this->curve_txt->Location = System::Drawing::Point(160, 19);
			this->curve_txt->Name = L"curve_txt";
			this->curve_txt->Size = System::Drawing::Size(100, 20);
			this->curve_txt->TabIndex = 2;
			// 
			// three_txt
			// 
			this->three_txt->Location = System::Drawing::Point(160, 47);
			this->three_txt->Name = L"three_txt";
			this->three_txt->Size = System::Drawing::Size(100, 20);
			this->three_txt->TabIndex = 3;
			// 
			// tv_pnl
			// 
			this->tv_pnl->Controls->Add(this->three_txt);
			this->tv_pnl->Controls->Add(this->curve_txt);
			this->tv_pnl->Controls->Add(this->label17);
			this->tv_pnl->Controls->Add(this->label16);
			this->tv_pnl->Location = System::Drawing::Point(4, 242);
			this->tv_pnl->Name = L"tv_pnl";
			this->tv_pnl->Size = System::Drawing::Size(315, 179);
			this->tv_pnl->TabIndex = 6;
			this->tv_pnl->Visible = false;
			// 
			// AddItem
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(319, 481);
			this->Controls->Add(this->tv_pnl);
			this->Controls->Add(this->phone_pnl);
			this->Controls->Add(this->bck_btn);
			this->Controls->Add(this->crt_btn);
			this->Controls->Add(this->pc_pnl);
			this->Controls->Add(this->type_cmbx);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->price_txt);
			this->Controls->Add(this->ph_txt);
			this->Controls->Add(this->descri_txt);
			this->Controls->Add(this->manu_txt);
			this->Controls->Add(this->model_txt);
			this->Controls->Add(this->cd_txt);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"AddItem";
			this->Text = L"AddItem";
			this->pc_pnl->ResumeLayout(false);
			this->pc_pnl->PerformLayout();
			this->phone_pnl->ResumeLayout(false);
			this->phone_pnl->PerformLayout();
			this->tv_pnl->ResumeLayout(false);
			this->tv_pnl->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void crt_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ofstream outfile;
		if (type_cmbx->Text == "pc") {

			outfile.open("pc.dat", std::ios::app);

			Compiuter pc = Compiuter();

			pc.setCode(Convert::ToInt32(cd_txt->Text));

			pc.setModel(marshal_as<std::string>(model_txt->Text));

			pc.setManufacturer(marshal_as<std::string>(manu_txt->Text));

			pc.setDescription(marshal_as<std::string>(descri_txt->Text));

			pc.setPhoto(Convert::ToInt32(ph_txt->Text));

			pc.setPrice(Convert::ToDouble(price_txt->Text));

			pc.setRam(Convert::ToInt32(ram_txt->Text));

			pc.setHdsize(Convert::ToInt32(hdsize_txt->Text));

			pc.setCpu(Convert::ToDouble(cpu_txt->Text));

			pc.setGpu(marshal_as<std::string>(gpu_txt->Text));

			pc.setHdtype(marshal_as<std::string>(hdtype_txt->Text));

			pc.save(outfile);
			MessageBox::Show("Item Succesfuly Added To List");
			outfile.close();
		}
		else if (type_cmbx->Text == "phone") {
			outfile.open("phone.dat", std::ios::app);

			SmartPhone phone = SmartPhone();

			phone.setCode(Convert::ToInt32(cd_txt->Text));

			phone.setModel(marshal_as<std::string>(model_txt->Text));

			phone.setManufacturer(marshal_as<std::string>(manu_txt->Text));

			phone.setDescription(marshal_as<std::string>(descri_txt->Text));

			phone.setPhoto(Convert::ToInt32(ph_txt->Text));

			phone.setPrice(Convert::ToDouble(price_txt->Text));

			phone.setScreensize(Convert::ToDouble(scrSize_txt->Text));

			phone.setBattery(Convert::ToInt32(btr_txt->Text));

			phone.setVideo(Convert::ToInt32(video_txt->Text));

			phone.save(outfile);
			MessageBox::Show("Item Succesfuly Added To List");
			outfile.close();
		}
		else if (type_cmbx->Text == "tv") {
			outfile.open("tv.dat", std::ios::app);

			Television tv = Television();

			tv.setCode(Convert::ToInt32(cd_txt->Text));

			tv.setModel(marshal_as<std::string>(model_txt->Text));

			tv.setManufacturer(marshal_as<std::string>(manu_txt->Text));

			tv.setDescription(marshal_as<std::string>(descri_txt->Text));

			tv.setPhoto(Convert::ToInt32(ph_txt->Text));

			tv.setPrice(Convert::ToDouble(price_txt->Text));
			
			tv.setCurve(Convert::ToInt32(curve_txt->Text));

			tv.setThreed(Convert::ToDouble(three_txt->Text));

			tv.save(outfile);
			MessageBox::Show("Item Succesfuly Added To List");
			outfile.close();
		}
	}
	private: System::Void type_cmbx_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		String^ type;
		type = type_cmbx->Text;

		if (type == "pc") {
			tv_pnl->Visible = false;
			phone_pnl->Visible = false;
			pc_pnl->Visible = true;
		}
		else if (type == "phone") {
			pc_pnl->Visible = false;
			tv_pnl->Visible = false;
			phone_pnl->Visible = true;
		}
		else if (type == "tv") {
			pc_pnl->Visible = false;
			phone_pnl->Visible = false;
			tv_pnl->Visible = true;
		}
		
		
	}
	private: System::Void bck_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	};
}
