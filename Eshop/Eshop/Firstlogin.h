#ifndef FIRSTLOGIN_H
#define FIRSTLOGIN_H
#include <iostream>
#include <string>
#include "User.h"
#include "Admin.h"

class FirstLogin {

public :
	FirstLogin();

private :
	string username, password;
	User user;
	Admin admin;
};


#endif // !FIRSTLOGIN_H

