#ifndef SMARTPHONE_H
#define SMARTPHONE_H
#include "Product.h"

class SmartPhone : public Product {
public:
	SmartPhone();

	SmartPhone(double screensize, int battery, bool video);

	SmartPhone(std::string strPh);

	double getScreensize();

	void setScreensize(double screensize);

	int getBattery();

	void setBattery(int battery);

	bool getVideo();

	void setVideo(bool video);

	void save(std::ofstream &outfile);

	std::string toStringPh();
private:
	double screensize;
	int battery;
	bool video;
};

#endif

