#include "Adminscreen.h"

using namespace std;

AdminScreen::AdminScreen(Admin admin) {
	this->admin = admin;
	/* h othoni tou admin ipeuthini gia ti dimiourgia xriston antikimenon kai ton genikon stixion tou eshop */
	system("CLS");
	do {
		cout << "---------------------------------------------" << endl;
		cout << "Welcome Admin " << admin.getName() << endl;
		cout << "---------------------------------------------" << endl;
		cout << "A) Create User                               " << endl;
		cout << "B) Show Items                                " << endl;
		cout << "C) Show Customers                            " << endl;
		cout << "D) Task Calculation                          " << endl;
		cout << "E) Disconect                                 " << endl;
		cout << "---------------------------------------------" << endl;
		cin >> action;

		switch (action) {
		case 'A':
			CreateUser();
			break;
		case 'B':
			do {
				cout << "Show Items                                   " << endl;
				Catalog();
				cout << "A) Add Items                                 " << endl;
				cout << "B) Remove Items                              " << endl;
				cout << "C) Update Items                              " << endl;
				cout << "D) Back                                      " << endl;
				cin >> action;

				switch (action) {
				case 'A':
					AddItems();
					break;
				case 'B':
					cout << "Remove Items" << endl;
					RemoveItems();
					break;
				case 'C':
					cout << "Update Items" << endl;
					UpdateItems();
					break;
				case 'D':
					ShowOrders();
					code = 0;
					break;
				default:
					cout << "Code Not Supported" << endl;
					break;
				}

			} while (code == 1);
			code = 1;
			break;
		case 'C':
			do {
				cout << "--------------" << endl;
				cout << "Show Customers" << endl;
				cout << "--------------" << endl;
				ShowOrders();
				cout << "-----------------------" << endl;
				cout << "A) Change Order Status " << endl;
				cout << "B) Back                " << endl;
				cout << "-----------------------" << endl;
				cin >> action;

				switch (action) {
				case 'A':
					ChangeStatus();
					break;
				case 'B':
					code = 0;
					break;
				default:
					cout << "Code Not Supported" << endl;
					break;
				}
			} while (code == 1);
			code = 1;
			break;
		case 'D':
			cout << "-----------------" << endl;
			cout << "Task Calculation " << endl;
			cout << "-----------------" << endl;
			Tasks();
			break;
		case 'E':
			cout << "--------------" << endl;
			cout << "Disconecting  " << endl;
			code = 0;
			break;
		default:
			cout << "Code not Supported" << endl;
		}
	} while (code == 1);
}
void AdminScreen::Tasks() {
	ifstream infile;
	vector<Physical> physical;
	vector<Company> company;
	vector<Order> orderlist;
	int count = 0, phCount = 0, coCount = 0;
	double sum = 0;
	// metrai poses paragelies iparxoun sto katastima
	cout << "------------" << endl;
	cout << "Task        " << endl;
	cout << "Order Count " << endl;

	infile.open("physical.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			physical.push_back(Physical(str));
			phCount++;
		}
	}

	infile.close();

	infile.open("company.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			company.push_back(Company(str));
			coCount++;
		}
	}

	infile.close();

	for (int i = 0; i < physical.size(); i++) {
		infile.open(physical[i].getUsername() + "_" + "Order.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				orderlist.push_back(Order(str));
				count++;
			}
		}

		infile.close();
	}

	for (int i = 0; i < company.size(); i++) {
		infile.open(company[i].getUsername() + "_" + "Order.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				orderlist.push_back(Order(str));
				count++;
			}
		}

		infile.close();
	}
	cout << "------------------------------" << endl;
	cout << count << " Orders In The System" << endl;
	cout << "------------------------------" << endl;
	// to sinolo ton paragelion pou iparxoun sto sistima
	cout << "Sum Of Orders In The Systm " << endl;
	cout << "---------------------------" << endl;

	for (int i = 0; i < orderlist.size(); i++) {
		sum += orderlist[i].getCost();
	}
	cout << sum << endl;
	cout << "------------" << endl;
	// posi xristes iparxoun sto sistima genika kai posi einai physical kai posi company
	cout << "Number Of Clients In The System" << endl;
	cout << "-------------------------------" << endl;
	cout << phCount + coCount << endl;
	cout << "------------------------------" << endl;

	cout << "Physical Clients " << endl;
	cout << "-----------------" << endl;
	cout << phCount << endl;

	cout << "Company Clients " << endl;
	cout << "----------------" << endl;
	cout << coCount << endl;

}
void AdminScreen::ShowOrders() {
	ifstream infile;
	vector<Physical> physical;
	vector<Company> company;
	int actionId;

	// vlepoume oles tis paragelies ana pelati
	infile.open("physical.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			physical.push_back(Physical(str));
		}
	}

	infile.close();

	infile.open("company.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			company.push_back(Company(str));
		}
	}

	infile.close();

	for (int i = 0; i < physical.size(); i++) {
		vector<Order> orderlist;

		infile.open(physical[i].getUsername() + "_" + "Order.dat");

		cout << "------------------" << endl;
		cout << "Physical Customer " << endl;
		cout << "------------------" << endl;

		cout << "-------------------------" << endl;
		cout << physical[i].toStringPh() << endl;
		cout << "-------------------------" << endl;

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				orderlist.push_back(Order(str));
			}
		}

		infile.close();

		cout << "--------" << endl;
		cout << "Orders  " << endl;
		cout << "--------" << endl;
		// analogos to status code vlepoume se ti katastasi vriskete h paragelia
		for (int i = 0; i < orderlist.size(); i++) {
			actionId = orderlist[i].getStatus();

			switch (actionId)
			{
			case 1:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " EMPTY" << endl;
				break;
			case 2:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " PENDING" << endl;
				break;
			case 3:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " SENDING" << endl;
				break;
			case 4:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " COMPLETE" << endl;
				break;
			case 5:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " CANCELED" << endl;
				break;
			default:
				break;
			}
		}
	}

	for (int i = 0; i < company.size(); i++) {
		vector<Order> orderlist;

		infile.open(company[i].getUsername() + "_" + "Order.dat");

		cout << "------------------" << endl;
		cout << "Company Customer " << endl;
		cout << "------------------" << endl;

		cout << "-------------------------" << endl;
		cout << company[i].toStringCo() << endl;
		cout << "-------------------------" << endl;

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				orderlist.push_back(Order(str));
			}
		}

		infile.close();

		cout << "--------" << endl;
		cout << "Orders  " << endl;
		cout << "--------" << endl;

		for (int i = 0; i < orderlist.size(); i++) {
			actionId = orderlist[i].getStatus();
			switch (actionId)
			{
			case 1:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " EMPTY" << endl;
				break;
			case 2:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " PENDING" << endl;
				break;
			case 3:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " SENDING" << endl;
				break;
			case 4:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " COMPLETE" << endl;
				break;
			case 5:
				cout << orderlist[i].getId() << " " << orderlist[i].getCost() << " CANCELED" << endl;
				break;
			default:
				break;
			}
		}
	}

}
void AdminScreen::CreateUser() {
	ofstream outfile;
	ifstream infile;
	// dimiourgia xristi sto sistima
	outfile.open("user.dat", ios::app);

	infile.open("user.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			users.push_back(User(str));
		}
	}
	infile.close();

	while (read) {
		cout << "Please enter Username: " << endl;
		string username = "";
		do {
			for (int i = 0; i < users.size(); i++) {
				// elenxoume an to username iparxi
				if (username == users[i].getUsername()) {
					cout << "This Username Exists" << endl;
					temp = users[i].getUsername();
				}
			}
			cin >> username;
		} while (temp == username);
		user.setUsername(username);
		cout << "Please enter Password: " << endl;
		string password;
		cin >> password;
		user.setPassword(password);
		cout << "Please enter Type physical or company or admin: " << endl;
		cin >> type;
		user.setType(type);

		user.save(outfile);

		break;
	}

	outfile.close();

	if (type == "physical") {
		physical.setUsername(user.getUsername());

		outfile.open(type + ".dat", ios::app);

		while (read) {

			cout << "Give Afm: " << endl;
			int afm;
			cin >> afm;
			physical.setAfm(afm);

			cout << "Give Address: " << endl;
			string address;
			cin >> address;
			physical.setAddress(address);

			cout << "Give Phonenumber: " << endl;
			int phonenumber;
			cin >> phonenumber;
			physical.setPhonenumber(phonenumber);

			cout << "Give Name: " << endl;
			string name;
			cin >> name;
			physical.setName(name);

			cout << "Give Surename: " << endl;
			string surename;
			cin >> surename;
			physical.setSurename(surename);

			cout << "Give Am: " << endl;
			string am;
			cin >> am;
			physical.setAm(am);

			cout << "Give Security pass: " << endl;
			string security;
			cin >> security;
			physical.setSecurity(security);

			physical.save(outfile);

			break;
		}

		outfile.close();
	}
	else if (type == "company") {
		company.setUsername(user.getUsername());

		outfile.open(type + ".dat", ios::app);
		while (read) {

			cout << "Give Afm: " << endl;
			int afm;
			cin >> afm;
			company.setAfm(afm);

			cout << "Give Address: " << endl;
			string address;
			cin >> address;
			company.setAddress(address);

			cout << "Give Phonenumber: " << endl;
			int phonenumber;
			cin >> phonenumber;
			company.setPhonenumber(phonenumber);

			cout << "Give Manager Name: " << endl;
			string name;
			cin >> name;
			company.setManagerName(name);

			cout << "Give Manager Surename: " << endl;
			string surename;
			cin >> surename;
			company.setManagerSurename(surename);

			cout << "Give Company Name: " << endl;
			string compname;
			cin >> compname;
			company.setName(compname);

			cout << "Give Discount: " << endl;
			double discount;
			cin >> discount;
			company.setDiscount(discount);

			cout << "Give Fax: " << endl;
			int fax;
			cin >> fax;
			company.setFax(fax);

			company.save(outfile);
			break;
		}

		outfile.close();
	}
	else {
		admin.setUsername(user.getUsername());

		outfile.open(type + ".dat", ios::app);

		while (read) {

			cout << "Give Name: " << endl;
			string name;
			cin >> name;
			admin.setName(name);

			cout << "Give Surename: " << endl;
			string surename;
			std::cin >> surename;
			admin.setSurename(surename);

			admin.save(outfile);
			break;
		}

		outfile.close();
	}
}
void AdminScreen::Catalog() {
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;
	vector<string> stringList;
	int counter;
	string help = "", tempSt;
	ifstream infile;
	// katalogos olon ton antikimenon
	infile.open("pc.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			compiuterList.push_back(Compiuter(str));
		}
	}

	infile.close();

	infile.open("tv.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			televisionList.push_back(Television(str));
		}
	}

	infile.close();

	infile.open("phone.dat");

	while (infile.good()) {
		getline(infile, str);
		if (str.length() > 1) {
			smartPhoneList.push_back(SmartPhone(str));
		}
	}

	infile.close();

	cout << "Compiuters " << endl;
	for (int i = 0; i < compiuterList.size(); i++) {
		cout << compiuterList[i].toString();
		cout << compiuterList[i].toStringPc() << endl;
	}

	cout << "SmartPhones " << endl;
	for (int i = 0; i < smartPhoneList.size(); i++) {
		cout << smartPhoneList[i].toString();
		cout << smartPhoneList[i].toStringPh() << endl;
	}

	cout << "Televisions " << endl;
	for (int i = 0; i < televisionList.size(); i++) {
		cout << televisionList[i].toString();
		cout << televisionList[i].toStringTv() << endl;
	}
}
void AdminScreen::AddItems() {
	cout << "What Type Of Item You Want To Add (pc/phone/tv)" << endl;
	cin >> type;
	// prosthiki antikimenon sto katastima
	if (type == "pc") {
		ofstream outfile;
		outfile.open(type + ".dat", ios::app);

		bool read = true;
		while (read) {
			Compiuter pc = Compiuter();

			cout << "Give Code: " << endl;
			int code;
			cin >> code;
			pc.setCode(code);

			cout << "Give Model: " << endl;
			string model;
			cin >> model;
			pc.setModel(model);

			cout << "Give Manufacturer: " << endl;
			string manufacturer;
			cin >> manufacturer;
			pc.setManufacturer(manufacturer);

			cout << "Give Description: " << endl;
			string description;
			cin >> description;
			pc.setDescription(description);

			cout << "Give Photo: (1/0)" << endl;
			bool photo;
			cin >> photo;
			pc.setPhoto(photo);

			cout << "Give Price: " << endl;
			double price;
			cin >> price;
			pc.setPrice(price);

			cout << "Give Ram: " << endl;
			int ram;
			cin >> ram;
			pc.setRam(ram);

			cout << "Give Hard Drive Size: " << endl;
			int hdsize;
			cin >> hdsize;
			pc.setHdsize(hdsize);

			cout << "Give Cpu: " << endl;
			double cpu;
			cin >> cpu;
			pc.setCpu(cpu);

			cout << "Give Gpu: " << endl;
			string gpu;
			cin >> gpu;
			pc.setGpu(gpu);

			cout << "Give Hard Drive Type (SSD/HDD/SSHD): " << endl;
			string hdtype;
			cin >> hdtype;
			pc.setHdtype(hdtype);

			pc.save(outfile);

			cout << "Would you like to add another Compiuter? (y/n): ";
			cin >> action;

			if (action == 'y') {
				continue;
			}
			else {
				break;
			}
		}
	}
	else if (type == "phone") {
		ofstream outfile;
		outfile.open(type + ".dat", ios::app);

		bool read = true;
		while (read) {
			SmartPhone phone = SmartPhone();

			cout << "Give Code: " << endl;
			int code;
			cin >> code;
			phone.setCode(code);

			cout << "Give Model: " << endl;
			string model;
			cin >> model;
			phone.setModel(model);

			cout << "Give Manufacturer: " << endl;
			string manufacturer;
			cin >> manufacturer;
			phone.setManufacturer(manufacturer);

			cout << "Give Description: " << endl;
			string description;
			cin >> description;
			phone.setDescription(description);

			cout << "Give Photo: (1/0)" << endl;
			bool photo;
			cin >> photo;
			phone.setPhoto(photo);

			cout << "Give Price: " << endl;
			double price;
			cin >> price;
			phone.setPrice(price);

			cout << "Give Screen Size: " << endl;
			double screensize;
			cin >> screensize;
			phone.setScreensize(screensize);

			cout << "Give Battery Life: " << endl;
			int battery;
			cin >> battery;
			phone.setBattery(battery);

			cout << "Give Video: (1/0) " << endl;
			bool video;
			cin >> video;
			phone.setVideo(video);

			phone.save(outfile);

			cout << "Would you like to add another Smart Phone? (y/n): ";
			cin >> action;

			if (action == 'y') {
				continue;
			}
			else {
				break;
			}
		}
	}
	else {
		ofstream outfile;
		outfile.open(type + ".dat", ios::app);

		bool read = true;
		while (read) {
			Television tv = Television();

			cout << "Give Code: " << endl;
			int code;
			cin >> code;
			tv.setCode(code);

			cout << "Give Model: " << endl;
			string model;
			cin >> model;
			tv.setModel(model);

			cout << "Give Manufacturer: " << endl;
			string manufacturer;
			cin >> manufacturer;
			tv.setManufacturer(manufacturer);

			cout << "Give Description: " << endl;
			string description;
			cin >> description;
			tv.setDescription(description);

			cout << "Give Photo: (1/0)" << endl;
			bool photo;
			cin >> photo;
			tv.setPhoto(photo);

			cout << "Give Price: " << endl;
			double price;
			cin >> price;
			tv.setPrice(price);

			cout << "Give Curve: " << endl;
			int curve;
			cin >> curve;
			tv.setCurve(curve);

			cout << "Give 3D: (1/0) " << endl;
			bool threed;
			cin >> threed;
			tv.setThreed(threed);

			tv.save(outfile);

			cout << "Would you like to add another Television? (y/n): ";
			cin >> action;

			if (action == 'y') {
				continue;
			}
			else {
				break;
			}
		}
	}
}
void AdminScreen::RemoveItems() {
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;
	ifstream infile;
	ofstream outfile;
	string type, temp;
	int id;
	// diagrafi proionton apo to katastima
	cout << "-------------------------------" << endl;
	cout << "Choose Item Type (pc/phone/tv) " << endl;
	cout << "-------------------------------" << endl;
	cin >> type;
	cout << "---------------" << endl;
	cout << "Choose Item Id " << endl;
	cout << "---------------" << endl;
	cin >> id;

	/* h diagrafi enos proiontos ginete me ena temp.dat arxio pou pername ola ta antikimena 
	   ektos apo auto pou theloume na zvisoume kai to metonomazoume to arxio sti katigoria pou antistixi */
	if (type == "pc") {
		infile.open(type + ".dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str.length() > 1) {
				compiuterList.push_back(Compiuter(str));
			}
		}

		infile.close();

		for (int i = 0; i < compiuterList.size(); i++) {
			if (id == compiuterList[i].getCode()) {
				temp = compiuterList[i].toString() + "|" + compiuterList[i].toStringPc();
			}
		}
		
		infile.open(type + ".dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str == temp) {
			}
			else
				outfile << str << endl;
		}

		infile.close();
		outfile.close();

		remove("pc.dat");
		rename("temp.dat", "pc.dat");

	}
	else if (type == "phone") {
		infile.open(type + ".dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str.length() > 1) {
				smartPhoneList.push_back(SmartPhone(str));
			}
		}

		infile.close();

		for (int i = 0; i < smartPhoneList.size(); i++) {
			if (id == smartPhoneList[i].getCode()) {
				temp = smartPhoneList[i].toString() + "|" + smartPhoneList[i].toStringPh();
			}
		}

		infile.open(type + ".dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str == temp) {
			}
			else
				outfile << str << endl;
		}

		infile.close();
		outfile.close();

		remove("phone.dat");
		rename("temp.dat", "phone.dat");
	}
	else if (type == "tv") {
		infile.open(type + ".dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str.length() > 1) {
				televisionList.push_back(Television(str));
			}
		}

		infile.close();

		for (int i = 0; i < televisionList.size(); i++) {
			if (id == televisionList[i].getCode()) {
				temp = televisionList[i].toString() + "|" + televisionList[i].toStringTv();
			}
		}

		infile.open(type + ".dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str == temp) {
			}
			else
				outfile << str << endl;
		}

		infile.close();
		outfile.close();

		remove("tv.dat");
		rename("temp.dat", "tv.dat");
	}

}
void AdminScreen::UpdateItems() {
	Compiuter pc = Compiuter();
	SmartPhone phone = SmartPhone();
	Television tv = Television();
	vector<Compiuter> compiuterList;
	vector<SmartPhone> smartPhoneList;
	vector<Television> televisionList;
	ifstream infile;
	ofstream outfile;
	string type, model, manufacturer, description, gpu, hdtype, help;
	int id, code, ram, hdsize, battery, curve;
	double price, cpu, screen, threed;
	bool photo, video;

	// alazoume kapio pedio tou antikimenou pou theloume
	cout << "--------------------" << endl;
	cout << "Choose Item Category" << endl;
	cout << "pc/phone/tv         " << endl;
	cout << "--------------------" << endl;
	cin >> type;

	if (type == "pc") {
		infile.open(type + ".dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str.length() > 1) {
				compiuterList.push_back(Compiuter(str));
			}
		}

		infile.close();

		cout << "---------------------------------" << endl;
		cout << "Choose Item Id                   " << endl;
		cout << "---------------------------------" << endl;
		cin >> id;

		// h metavliti help krataei to antikimeno prin to ananeosoume gia na to xrisimopoieisoume gia sigrisi
		for (int i = 0; i < compiuterList.size(); i++) {
			if (id == compiuterList[i].getCode()) {
				pc = compiuterList[i];
				help = pc.toString() + "|" + pc.toStringPc();
			}
		}

		cout << "---------------------------------" << endl;
		cout << "Choose Attribute To Update       " << endl;
		cout << "A) Code                          " << endl;
		cout << "B) Model                         " << endl;
		cout << "C) Manufacturer                  " << endl;
		cout << "D) Description                   " << endl;
		cout << "E) Photo                         " << endl;
		cout << "F) Price                         " << endl;
		cout << "G) Ram                           " << endl;
		cout << "H) Hard Disk Size                " << endl;
		cout << "I) Cpu                           " << endl;
		cout << "J) Gpu                           " << endl;
		cout << "K) Hard Disk Type                " << endl;
		cin >> action;

		switch (action)
		{
		case 'A':
			cout << "Give Code: " << endl;
			cin >> code;
			pc.setCode(code);
			break;
		case 'B':
			cout << "Give Model: " << endl;
			cin >> model;
			pc.setModel(model);
			break;
		case 'C':
			cout << "Give Manufacturer: " << endl;
			cin >> manufacturer;
			pc.setManufacturer(manufacturer);
			break;
		case 'D':
			cout << "Give Description: " << endl;
			cin >> description;
			pc.setDescription(description);
			break;
		case 'E':
			cout << "Give Photo: (1/0)" << endl;
			cin >> photo;
			pc.setPhoto(photo);
			break;
		case 'F':
			cout << "Give Price: " << endl;
			cin >> price;
			pc.setPrice(price);
			break;
		case 'G':
			cout << "Give Ram: " << endl;
			cin >> ram;
			pc.setRam(ram);
			break;
		case 'H':
			cout << "Give Hard Drive Size: " << endl;
			cin >> hdsize;
			pc.setHdsize(hdsize);
			break;
		case 'I':
			cout << "Give Cpu: " << endl;
			cin >> cpu;
			pc.setCpu(cpu);
			break;
		case 'J':
			cout << "Give Gpu: " << endl;
			cin >> gpu;
			pc.setGpu(gpu);
			break;
		case 'K':
			cout << "Give Hard Drive Type (SSD/HDD/SSHD): " << endl;
			cin >> hdtype;
			pc.setHdtype(hdtype);
			break;
		default:
			cout << "Code Not Supported" << endl;
			break;
		}
		infile.open(type + ".dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str == help) {
				pc.save(outfile);
			}
			else
				outfile << str << endl;
		}

		infile.close();
		outfile.close();

		remove("pc.dat");
		rename("temp.dat", "pc.dat");
	}

	else if (type == "phone") {

		infile.open(type + ".dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str.length() > 1) {
				smartPhoneList.push_back(SmartPhone(str));
			}
		}

		infile.close();
		cout << "---------------------------------" << endl;
		cout << "Choose Item Id                   " << endl;
		cout << "---------------------------------" << endl;
		cin >> id;

		for (int i = 0; i < smartPhoneList.size(); i++) {
			if (id == smartPhoneList[i].getCode()) {
				phone = smartPhoneList[i];
				help = phone.toString() + "|" + phone.toStringPh();
			}
		}

		cout << "---------------------------------" << endl;
		cout << "Choose Attribute To Update       " << endl;
		cout << "A) Code                          " << endl;
		cout << "B) Model                         " << endl;
		cout << "C) Manufacturer                  " << endl;
		cout << "D) Description                   " << endl;
		cout << "E) Photo                         " << endl;
		cout << "F) Price                         " << endl;
		cout << "G) Screen Size                   " << endl;
		cout << "H) Battery                       " << endl;
		cout << "I) Video                         " << endl;
		cin >> action;

		switch (action)
		{
		case 'A':
			cout << "Give Code: " << endl;
			cin >> code;
			phone.setCode(code);
			break;
		case 'B':
			cout << "Give Model: " << endl;
			cin >> model;
			phone.setModel(model);
			break;
		case 'C':
			cout << "Give Manufacturer: " << endl;
			cin >> manufacturer;
			phone.setManufacturer(manufacturer);
			break;
		case 'D':
			cout << "Give Description: " << endl;
			cin >> description;
			phone.setDescription(description);
			break;
		case 'E':
			cout << "Give Photo: (1/0)" << endl;
			cin >> photo;
			phone.setPhoto(photo);
			break;
		case 'F':
			cout << "Give Price: " << endl;
			cin >> price;
			phone.setPrice(price);
			break;
		case 'G':
			cout << "Give Screen Size: " << endl;
			cin >> screen;
			phone.setScreensize(screen);
			break;
		case 'H':
			cout << "Give Battery Life: " << endl;
			cin >> battery;
			phone.setBattery(battery);
			break;
		case 'I':
			cout << "Give Video (1/0): " << endl;
			cin >> video;
			phone.setVideo(video);
			break;
		default:
			cout << "Code Not Supported" << endl;
			break;
		}
		infile.open(type + ".dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str == help) {
				phone.save(outfile);
			}
			else
				outfile << str << endl;
		}

		infile.close();
		outfile.close();

		remove("phone.dat");
		rename("temp.dat", "phone.dat");

	}
	else if (type == "tv") {
		infile.open(type + ".dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str.length() > 1) {
				televisionList.push_back(Television(str));
			}
		}

		infile.close();

		cout << "---------------------------------" << endl;
		cout << "Choose Item Id                   " << endl;
		cout << "---------------------------------" << endl;
		cin >> id;

		for (int i = 0; i < televisionList.size(); i++) {
			if (id == televisionList[i].getCode()) {
				tv = televisionList[i];
				help = tv.toString() + "|" + tv.toStringTv();
			}
		}

		cout << "---------------------------------" << endl;
		cout << "Choose Attribute To Update       " << endl;
		cout << "A) Code                          " << endl;
		cout << "B) Model                         " << endl;
		cout << "C) Manufacturer                  " << endl;
		cout << "D) Description                   " << endl;
		cout << "E) Photo                         " << endl;
		cout << "F) Price                         " << endl;
		cout << "G) Curve                         " << endl;
		cout << "H) 3D                            " << endl;
		cout << "---------------------------------" << endl;
		cin >> action;

		switch (action)
		{
		case 'A':
			cout << "Give Code: " << endl;
			cin >> code;
			tv.setCode(code);
			break;
		case 'B':
			cout << "Give Model: " << endl;
			cin >> model;
			tv.setModel(model);
			break;
		case 'C':
			cout << "Give Manufacturer: " << endl;
			cin >> manufacturer;
			tv.setManufacturer(manufacturer);
			break;
		case 'D':
			cout << "Give Description: " << endl;
			cin >> description;
			tv.setDescription(description);
			break;
		case 'E':
			cout << "Give Photo: (1/0)" << endl;
			cin >> photo;
			tv.setPhoto(photo);
			break;
		case 'F':
			cout << "Give Price: " << endl;
			cin >> price;
			tv.setPrice(price);
			break;
		case 'G':
			cout << "Give Curve: " << endl;
			cin >> curve;
			tv.setCurve(curve);
			break;
		case 'H':
			cout << "Give 3D (1/0): " << endl;
			cin >> threed;
			tv.setThreed(threed);
			break;
		default:
			cout << "Code Not Supported" << endl;
			break;
		}

		infile.open(type + ".dat");
		outfile.open("temp.dat");

		while (!infile.eof()) {
			getline(infile, str);
			if (str == help) {
				tv.save(outfile);
			}
			else
				outfile << str << endl;
		}

		infile.close();
		outfile.close();

		remove("tv.dat");
		rename("temp.dat", "tv.dat");
	}

}
void AdminScreen::ChangeStatus() {
	vector<Order> orderlist;
	ifstream infile;
	ofstream outfile;
	string name, str, temp;
	Order order;
	int id, status;

	// alazoume to status kapias paragelias
	cout << "------------------------------" << endl;
	cout << "Change Order Status           " << endl;
	cout << "Choose User Order by Username " << endl;
	cout << "------------------------------" << endl;
	cin >> name;
	cout << "------------------------------" << endl;
	cout << "Choose Order Id               " << endl;
	cout << "------------------------------" << endl;
	cin >> id;
	cout << "------------------------------" << endl;
	cout << "Change Order Status To        " << endl;
	cout << "1) EMPTY                      " << endl;
	cout << "2) PENDING                    " << endl;
	cout << "3) SENDING                    " << endl;
	cout << "4) COMPLETE                   " << endl;
	cout << "5) CANCELED                   " << endl;
	cin >> status;

	infile.open(name + "_" + "Order.dat");

	while (!infile.eof()) {
		getline(infile, str);
		if (str.length() > 1) {
			orderlist.push_back(Order(str));
		}
	}

	infile.close();

	for (int i = 0; i < orderlist.size(); i++) {
		if (id == orderlist[i].getId()) {
			order = orderlist[i];
		}
	}

	infile.open(name + "_" + "Order.dat");
	outfile.open("temp.dat");

	while (!infile.eof()) {
		getline(infile, str);
		if (str == order.toString()) {
			order.setStatus(status);
			outfile << order.toString() << endl;
		}
		else
			outfile << str << endl;
	}

	infile.close();
	outfile.close();

	temp = name + "_" + "Order.dat";

	remove(temp.c_str());
	rename("temp.dat", temp.c_str());
}
