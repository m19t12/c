#pragma once
#include "Order.h"
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for OrderStatus
	/// </summary>
	public ref class OrderStatus : public System::Windows::Forms::Form
	{
	public:
		String^ item;
	private: System::Windows::Forms::Label^  id_lbl;
	public:
	private: System::Windows::Forms::Label^  cost_lbl;
			 String^ username;
	public:
		OrderStatus(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}
		OrderStatus(String^ item, String^ username)
		{
			InitializeComponent();
			this->item = item;
			this->username = username;
			//
			//TODO: Add the constructor code here
			//
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~OrderStatus()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;


	private: System::Windows::Forms::ComboBox^  status_cmbx;



	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->status_cmbx = (gcnew System::Windows::Forms::ComboBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->id_lbl = (gcnew System::Windows::Forms::Label());
			this->cost_lbl = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(26, 38);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(16, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Id";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(26, 68);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(28, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Cost";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(26, 100);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(37, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Status";
			// 
			// status_cmbx
			// 
			this->status_cmbx->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->status_cmbx->FormattingEnabled = true;
			this->status_cmbx->Items->AddRange(gcnew cli::array< System::Object^  >(5) {
				L"EMPTY", L"PENDING", L"SENDING", L"COMPLETE",
					L"CANCELED"
			});
			this->status_cmbx->Location = System::Drawing::Point(138, 100);
			this->status_cmbx->Name = L"status_cmbx";
			this->status_cmbx->Size = System::Drawing::Size(100, 21);
			this->status_cmbx->TabIndex = 5;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(29, 184);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 6;
			this->button1->Text = L"Change";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &OrderStatus::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(163, 184);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 7;
			this->button2->Text = L"Back";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &OrderStatus::button2_Click);
			// 
			// id_lbl
			// 
			this->id_lbl->AutoSize = true;
			this->id_lbl->Location = System::Drawing::Point(138, 37);
			this->id_lbl->Name = L"id_lbl";
			this->id_lbl->Size = System::Drawing::Size(40, 13);
			this->id_lbl->TabIndex = 8;
			this->id_lbl->Text = L"id label";
			// 
			// cost_lbl
			// 
			this->cost_lbl->AutoSize = true;
			this->cost_lbl->Location = System::Drawing::Point(138, 67);
			this->cost_lbl->Name = L"cost_lbl";
			this->cost_lbl->Size = System::Drawing::Size(55, 13);
			this->cost_lbl->TabIndex = 9;
			this->cost_lbl->Text = L"cost_label";
			// 
			// OrderStatus
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->cost_lbl);
			this->Controls->Add(this->id_lbl);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->status_cmbx);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"OrderStatus";
			this->Text = L"OrderStatus";
			this->Load += gcnew System::EventHandler(this, &OrderStatus::OrderStatus_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void OrderStatus_Load(System::Object^  sender, System::EventArgs^  e) {
		String^ temp = item;
		int index;
		Order order = Order(marshal_as<std::string>(temp));

		id_lbl->Text = Convert::ToString(order.getId());
		cost_lbl->Text = Convert::ToString(order.getCost());
		index = status_cmbx->FindString(marshal_as<String^>(order.getStatus()));
		status_cmbx->SelectedIndex = index;
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	std::ifstream infile;
	std::ofstream outfile;
	std::string str, help;
	String^ name = username;
	String^ temp = item;
	Order order = Order(marshal_as<std::string>(temp));

	infile.open(marshal_as<std::string>(name) + "_" + "Order.dat");
	outfile.open("temp.dat");

	while (!infile.eof()) {
		getline(infile, str);
		if (str == marshal_as<std::string>(temp)) {
			order.setStatus(marshal_as<std::string>(status_cmbx->SelectedItem->ToString()));
			outfile << order.toString() << std::endl;
		}
		else
			outfile << str << std::endl;
	}

	infile.close();
	outfile.close();

	help = marshal_as<std::string>(name) + "_" + "Order.dat";

	remove(help.c_str());
	rename("temp.dat", help.c_str());
}
};
}
