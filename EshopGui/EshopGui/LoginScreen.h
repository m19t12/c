#pragma once
#include "User.h"
#include "Admin.h"
#include "Physical.h"
#include "Company.h"
#include "AdminScreen.h"
#include "UserScreen.h"
#include "ForgotUser.h"
#include <fstream>
#include <vector>
#include <msclr\marshal_cppstd.h>

namespace EshopGui {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	/// <summary>
	/// Summary for LoginScreen
	/// </summary>
	public ref class LoginScreen : public System::Windows::Forms::Form
	{
	public:
		LoginScreen(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~LoginScreen()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  login;
	private: System::Windows::Forms::Button^  exit;
	private: System::Windows::Forms::Button^  forgot_btn;

	private: System::Windows::Forms::TextBox^  username_txt;

	protected:




	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  password_txt;

	private: System::Windows::Forms::Label^  label2;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->login = (gcnew System::Windows::Forms::Button());
			this->exit = (gcnew System::Windows::Forms::Button());
			this->forgot_btn = (gcnew System::Windows::Forms::Button());
			this->username_txt = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->password_txt = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// login
			// 
			this->login->Location = System::Drawing::Point(12, 170);
			this->login->Name = L"login";
			this->login->Size = System::Drawing::Size(75, 23);
			this->login->TabIndex = 0;
			this->login->Text = L"Login";
			this->login->UseVisualStyleBackColor = true;
			this->login->Click += gcnew System::EventHandler(this, &LoginScreen::login_Click);
			// 
			// exit
			// 
			this->exit->Location = System::Drawing::Point(197, 170);
			this->exit->Name = L"exit";
			this->exit->Size = System::Drawing::Size(75, 23);
			this->exit->TabIndex = 1;
			this->exit->Text = L"Exit";
			this->exit->UseVisualStyleBackColor = true;
			this->exit->Click += gcnew System::EventHandler(this, &LoginScreen::exit_Click);
			// 
			// forgot_btn
			// 
			this->forgot_btn->Location = System::Drawing::Point(12, 226);
			this->forgot_btn->Name = L"forgot_btn";
			this->forgot_btn->Size = System::Drawing::Size(260, 23);
			this->forgot_btn->TabIndex = 2;
			this->forgot_btn->Text = L"Forgot Password";
			this->forgot_btn->UseVisualStyleBackColor = true;
			this->forgot_btn->Click += gcnew System::EventHandler(this, &LoginScreen::forgot_btn_Click);
			// 
			// username_txt
			// 
			this->username_txt->Location = System::Drawing::Point(124, 34);
			this->username_txt->Name = L"username_txt";
			this->username_txt->Size = System::Drawing::Size(148, 20);
			this->username_txt->TabIndex = 3;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(34, 41);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(61, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Username :";
			// 
			// password_txt
			// 
			this->password_txt->Location = System::Drawing::Point(124, 74);
			this->password_txt->Name = L"password_txt";
			this->password_txt->PasswordChar = '*';
			this->password_txt->Size = System::Drawing::Size(148, 20);
			this->password_txt->TabIndex = 5;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(34, 81);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(59, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"Password :";
			// 
			// LoginScreen
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->password_txt);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->username_txt);
			this->Controls->Add(this->forgot_btn);
			this->Controls->Add(this->exit);
			this->Controls->Add(this->login);
			this->Name = L"LoginScreen";
			this->Text = L"LoginScreen";
			this->Load += gcnew System::EventHandler(this, &LoginScreen::LoginScreen_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void login_Click(System::Object^  sender, System::EventArgs^  e) {
		std::ifstream infile;
		std::string str, type = "";
		std::vector<User> users;
		std::vector<Admin> adminUsers;
		std::vector<Physical> physicalUsers;
		std::vector<Company> companyUsers;
		Admin admin;
		Company company;
		Physical physical;
		String ^ username;
		String ^ password;
		int code = 1;

		infile.open("user.dat");

		while (infile.good()) {
			getline(infile, str);
			if (str.length() > 1) {
				users.push_back(User(str));
			}
		}

		infile.close();

		do {
			username = username_txt->Text;
			password = password_txt->Text;

			for (int i = 0; i < users.size(); i++) {
				if (username == marshal_as<String^>(users[i].getUsername()) && password == marshal_as<String^>(users[i].getPassword())) {
					type = users[i].getType();
					code = 0;
				}
			}

		} while (code == 1);
		if (type == "physical") {

			infile.open(type + ".dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					physicalUsers.push_back(Physical(str));
				}
			}

			infile.close();

			for (int i = 0; i < physicalUsers.size(); i++) {
				if (username == marshal_as<String^>(physicalUsers[i].getUsername())) {
					physical = physicalUsers[i];
				}
			}
			UserScreen^ userForm = gcnew UserScreen(&physical);
			this->Hide();
			userForm->ShowDialog();
			this->Show();
		}
		else if (type == "company") {

			infile.open(type + ".dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					companyUsers.push_back(Company(str));
				}
			}

			infile.close();

			for (int i = 0; i < companyUsers.size(); i++) {
				if (username == marshal_as<String^>(companyUsers[i].getUsername())) {
					company = companyUsers[i];
				}
			}

			UserScreen^ userForm = gcnew UserScreen(&company);
			this->Hide();
			userForm->ShowDialog();
			this->Show();
		}
		else {
			infile.open(type + ".dat");

			while (infile.good()) {
				getline(infile, str);
				if (str.length() > 1) {
					adminUsers.push_back(Admin(str));
				}
			}

			infile.close();

			for (int i = 0; i < adminUsers.size(); i++) {
				if (username == marshal_as<String^>(adminUsers[i].getUsername())) {
					admin = adminUsers[i];
				}
			}
			AdminScreen^ adminForm = gcnew AdminScreen(&admin);
			this->Hide();
			adminForm->ShowDialog();
			this->Show();
			username_txt->Text = "";
			password_txt->Text = "";
		}	
	}
	private: System::Void exit_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
	private: System::Void forgot_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		ForgotUser^ forgotForm = gcnew ForgotUser();
		this->Hide();
		forgotForm->ShowDialog();
		this->Show();
	}
private: System::Void LoginScreen_Load(System::Object^  sender, System::EventArgs^  e) {
}
};
}
