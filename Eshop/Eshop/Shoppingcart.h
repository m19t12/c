#ifndef SHOPPINGCART_H
#define SHOPPINGCART_H
#include <vector>
#include "Compiuter.h"
#include "Smartphone.h"
#include "Television.h"

class ShoppingCart {

public:
	ShoppingCart();

	void addCompiuter(Compiuter c);

	vector<Compiuter> getCompiuter();

	void addSmartphone(SmartPhone s);

	vector<SmartPhone> getSmartphone();

	void addTelevision(Television t);

	vector<Television> getTelevision();

	void setCompiuterVector(vector<Compiuter> compiuter);

	void setSmartPhoneVector(vector<SmartPhone> smartphone);

	void setTelevisionVector(vector<Television> television);

private:
	vector<Compiuter> compiuter;
	vector<SmartPhone> smartphone;
	vector<Television> television;
};

#endif // !SHOPPINGCART_H