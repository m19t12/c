#include "Order.h"

Order::Order() {
	cost = 0;
}


Order::Order(int id, double cost, int status) {
	this->id = id;
	this->cost = cost;
	this->status = status;
}

Order::Order(string strOr) {
	istringstream issin(strOr);
	string split[3];
	int i = 0;

	while (getline(issin, split[i], '|')) {
		i++;
	}
	this->id = stoi(split[0]);
	this->cost = stod(split[1]);
	this->status = stoi(split[2]);
}

void Order::Calculation(double cost) {
		this->cost += cost;
}

int Order::getId() {
	return this->id;
}

void Order::setId(int id) {
	this->id = id;
}

double Order::getCost() {
	return this->cost;
}

void Order::setCost(double cost) {
	this->cost = cost;
}

void Order::setStatus(int status) {
	this->status = status;
}

int Order::getStatus() {
	return this->status;
}

void Order::save(ofstream &outfile) {
	outfile << this->toString() << endl;
}

string Order::toString() {
	string str = to_string(id) + "|" + to_string(cost) + "|" + to_string(status);
	return str;
}